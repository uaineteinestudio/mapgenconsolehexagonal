#include "blockTypes.h"

bool blockTypes::isWalkable(int type)
{
	return walkableBase[type];
}

float blockTypes::getZScale(int type)
{
	return blockScales[type];
}

float blockTypes::getSpawnOffset(int type)
{
	/*switch (type)
	{
	case 6:
		offset.Z = 5;
		break;
	case 4:
		offset.Z = 40;
		break;
	}*/
	return spawnOffset[type];
}

bool blockTypes::isGrass(int type)
{
	switch (type)
	{
	case grass:
		return true;
	case lightgrass:
		return true;
	case heavygrass:
		return true;
	default:
		return false;
	}
}

bool blockTypes::notCave(int type)
{
	switch (type)
	{
	case cave:
		return false;
	case caveInterior:
		return false;
	default:
		return true;
	}
}

bool blockTypes::notWater(int type)
{
	if (type != water)
		return true;
	return false; 	//else
}

bool blockTypes::foliageCanGrow(int type)
{
	switch (type)
	{
	case grass:
		return true;
	case heavygrass:
		return true;
	default:
		return false;
	}
}

bool surfaceTypes::isWalkable(int type)
{
	return walkableSurface[type];
}
