#pragma once

#include "geometry/rectangle.h"
#include "maplen.h"
#include "selClass.h"
//#include "coord.h"
#include "waterfall.h"
#include "arrays/dynInitaliser.h"
#include <vector>
//#include "chunk.h"
#include "chunks/chunkHandler.h"
#include "blockTypes.h"
#include "items/randomchest.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

class mapdata : public rectangle
{
public:
	mapdata(int lx, int ly, bool sqhex) : rectangle(lx, ly)
	{
		//ADD HERE
		square4hex6 = sqhex;
		lenx = lx;
		leny = ly;
		initalise();
	}
	~mapdata();
	//CLASS OBJECTS
	selClass sel = selClass();

	//initalise data
	void initalise();	//do all call
	void destroy();		//deconstruct map to make way for another

	//GET BACK AND SET THE DATA
	//GET TILE TYPES
	int type(int x, int y);
	int stype(int x, int y);

	//get and set walkability
	bool isWalkable(int x, int y);
	void setWalkable(int x, int y, bool walk);
	
	//GET AND SET THE TEMP
	float getTemp(int x, int y);
	void setTemp(int x, int y, float tmp);

	//HEIGHTS
	int height(int x, int y);
	void setHeight(int x, int y, int h);

	//ROTATION
	int getRotation(int x, int y);
	void setRotation(int x, int y, int r);

	//returns a random block and whether to ensure it is walkable
	coord getRandomBlock(bool walkableb);
	coord getRandomCornerBlock(int corner, bool walkableb);

	//get if hex or not
	bool gethexorsquare();

	//chests
	chest* getChest(int i) { return &chests[i]; }
	int getNoChests() { return chests.size(); }
protected:
	//HEXORSQUARE
	bool square4hex6 = false; //false or 0 = square 4

	//DYNAMIC MAP ARRAYS FOR TILE DAT
	virtual void initialisearrs();			//initalise arrays
	virtual void deletearrs();				//delete my arrays :'(
	void InitaliseRotation();
	void InitialiseTypes();
	bool initisedArrs = false;//initalised arrays?
	//
	int** blockTypes = 0;
	int** surfaceTypes = 0;
	int** blockHeights = 0;
	bool** water = false;
	float** temp = 0;
	bool** footpaths = false;
	bool** walkable = false;
	bool** walkableTranspose = false;
	bool** Caves = false;
	vector<waterfall> waterfalls;	
	vector<chest> chests;
	int** rot = 0;				//  rotator of tile-has 6 edges so ranges 0 to 6	
	int maxRot = rotLen;			
	int minRot = 0;			

	//CHUNKS
	ChunkHandler cH; //don't forget to initalise
	int activechunksradius = 2;	
};

