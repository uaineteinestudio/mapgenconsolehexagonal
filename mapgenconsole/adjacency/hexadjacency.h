#pragma once

#include "../coord.h"
#include <vector>

using namespace std;

//GLOBAL FIELDS
static const int HexAdjLen = 6;
static const int colMat[2][HexAdjLen] = 
{
	{ -1,  1, 0, 0, -1,  1 },
	{ -1,  0, 1, 1, 0,  -1}
};
static const int rowMat[2][HexAdjLen] =
{
	{ 0,  0,  -1, 1, 1, 1},
	{ 0, 1, 0, -1, -1, -1}
};


static class hexAdjaceny
{
	friend class squareadjacency;
public:
	static void WriteColRow(int* colarr, int* rowarr, int xi, int yi);
	static void getRadiusCircle(int x, int y, int rad, vector<coord> &circle, int lenx, int leny); //only intending to work with 2 max
protected:
	static void pushBackBoundCheck(coord p, vector<coord> & circle, int lenx, int leny);
};