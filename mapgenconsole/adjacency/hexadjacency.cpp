#include "hexadjacency.h"

void hexAdjaceny::WriteColRow(int * colarr, int * rowarr, int xi, int yi)
{
	int sel = (xi) % 2; //based on col
	for (int x = 0; x < HexAdjLen; x++)
	{
		colarr[x] = colMat[sel][x];
		rowarr[x] = rowMat[sel][x];
	}
}

void hexAdjaceny::getRadiusCircle(int x, int y, int rad, vector<coord>& circle, int lenx, int leny)
{
	circle.clear();
	
	//middle line
	for (int k = -rad; k < rad + 1; k++)
	{
		if (k!= y)
			pushBackBoundCheck(coord(x, y + k), circle, lenx, leny);
	}
	if (x % 2 == 0)		//even
	{
		//extrema
		for (int k = -rad / 2; k < ((rad + 1)/2) + 1; k++)
		{
			pushBackBoundCheck(coord(x - rad, y + k), circle, lenx, leny);
			pushBackBoundCheck(coord(x + rad, y + k), circle, lenx, leny);
		}
		//extra lines
		for (int k = 1; k < rad; k++)
		{
			for (int j = -rad +((k - 1) / 2) + 1; j < rad - (k/2) + 1; j++)
			{
				pushBackBoundCheck(coord(x + k, y + j), circle, lenx, leny);
				pushBackBoundCheck(coord(x - k, y + j), circle, lenx, leny);
			}
		}
	}
	else				//odd
	{
		//extrema
		for (int k = -(rad + 1) / 2; k < rad / 2 + 1; k++)
		{
			pushBackBoundCheck(coord(x - rad, y + k), circle, lenx, leny);
			pushBackBoundCheck(coord(x + rad, y + k), circle, lenx, leny);
		}
		//extra lines
		for (int k = 1; k < rad; k++)
		{
			for (int j = -rad + (k / 2); j < rad - ((k - 1) / 2); j++)
			{
				pushBackBoundCheck(coord(x + k, y + j), circle, lenx, leny);
				pushBackBoundCheck(coord(x - k, y + j), circle, lenx, leny);
			}
		}
	}
}

void hexAdjaceny::pushBackBoundCheck(coord p, vector<coord>& circle, int lenx, int leny)
{
	//error checking
	if (boundchecker::inBounds(p.x, p.y, lenx, leny))
	{
		//within bounds so push back
		circle.push_back(p);
	}
}
