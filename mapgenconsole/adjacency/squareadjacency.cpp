#include "squareadjacency.h"

/*    GRID
0 0 0 0 0 0
0 0 1 0 0 0
0 1 x 1 0 0
0 0 1 0 0 0
0 0 0 0 0 0

0 0 1 0 0 0
0 1 1 1 0 0
1 1 x 1 1 0
0 1 1 1 0 0
0 0 1 0 0 0
*/

void squareadjacency::getRadiusCircle(int x, int y, int rad, vector<coord>& circle, int lenx, int leny)
{
	circle.clear();
	//extrema down and up
	hexAdjaceny::pushBackBoundCheck(coord(x, y + rad), circle, lenx, leny);
	hexAdjaceny::pushBackBoundCheck(coord(x, y - rad), circle, lenx, leny);
	//middle line
	for (int i = -rad; i < rad + 1; i++)
	{
		if (i != 0)
			hexAdjaceny::pushBackBoundCheck(coord(x + i, y), circle, lenx, leny);
	}
	//fillers
	for (int line = 1; line < rad; line++) //if rad = 2, only 1 line to fill
	{
		for (int k = -rad + line; k < rad - line + 1; k++)
		{
			hexAdjaceny::pushBackBoundCheck(coord(x + k, y + line), circle, lenx, leny);
			hexAdjaceny::pushBackBoundCheck(coord(x + k, y - line), circle, lenx, leny);
		}
	}
}
