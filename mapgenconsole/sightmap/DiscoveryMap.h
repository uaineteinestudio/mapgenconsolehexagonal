#pragma once

#include "../geometry/rectangle.h"
#include "../arrays/dynInitaliser.h"
#include "../coord.h"
#include <vector>

using namespace std;

class DiscoveryMap : public rectangle
{
	friend class mapvisibility;
	friend class levelmapIO;
	friend class levelmapUI;
	friend class playerparser;
public: 
	DiscoveryMap() : rectangle(MaxLen, MaxLen)
	{
		iamplayerNo = 0;
		resetVisDis();
	}
	DiscoveryMap(int playerIndex, int lx, int ly) : rectangle(lx, ly)
	{
		iamplayerNo = playerIndex;
		resetVisDis();
	}
	~DiscoveryMap();
	
	int IamPlayer() { return iamplayerNo; }

	//reset
	void resetVisDis();
	//delete
	void destroy();
	//discovery and visibility
	void discover(vector<coord> discv, int num);//discovered these points
	bool getDiscovered(int x, int y);
	void discover(int x, int y);
	void discover(coord point);//discovered this point
	bool getVisible(int x, int y) { return visible[x][y]; }
	void setVisible(int x, int y);
	void setVisible(coord point) { setVisible(point.x, point.y); }
	void setNotVisible(coord point);
	void setNotVisible(int x, int y) { setNotVisible(coord(x, y)); }
	void resetVisible();
protected:
	bool** discovered = false;	//fog of war
	bool** visible = false;		//fog of war
	//VISIBLITY FOR WHICH PLAYER
	int iamplayerNo = 0;
};

