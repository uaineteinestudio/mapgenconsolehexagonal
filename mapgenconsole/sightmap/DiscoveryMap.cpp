#include "DiscoveryMap.h"

DiscoveryMap::~DiscoveryMap()
{
}

void DiscoveryMap::resetVisDis()
{
	discovered = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	visible = dynInitaliser::makeBool(MaxLen, MaxLen, false);
}

void DiscoveryMap::destroy()
{
	dynInitaliser::del(discovered, MaxLen);
	dynInitaliser::del(visible, MaxLen);
}

void DiscoveryMap::discover(vector<coord> discv, int num)
{
	for (int i = 0; i < num; i++)
	{
		discovered[discv[i].x][discv[i].y] = true;
	}
}

bool DiscoveryMap::getDiscovered(int x, int y)
{
	return discovered[x][y];
}

void DiscoveryMap::discover(int x, int y)
{
	discovered[x][y] = true;
}

void DiscoveryMap::discover(coord point)
{
	discover(point.x, point.y);
}

void DiscoveryMap::setVisible(int x, int y)
{
	visible[x][y] = true;
	if (getDiscovered(x, y) == false)
		discover(x, y);
}

void DiscoveryMap::setNotVisible(coord point)
{
	//failsafe check before set
	if (boundchecker::inBounds(point.x, point.y, lenx, leny))
	{
		visible[point.x][point.y] = false;
	}
}

void DiscoveryMap::resetVisible()
{
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			visible[x][y] = false;
		}
	}
}
