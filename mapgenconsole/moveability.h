#pragma once

//THIS CLASS IS DATA FOR DISTANCE TO RETURN BACK TO THE THE SEL CIRCLE 
//THE SELECTION CIRCLE IS CURRENTLY USED LIKE SO (as of Beta 1.1):
/*
vector<coord> circle = vector<coord>();
vector<int> dists;
selCircle::getSelCircle(sel.selectedpos.x, sel.selectedpos.y, circle, dists, lenx, leny);
*/

class moveability
{
public:
	moveability();
	moveability(int dist, bool wlkble);
	~moveability();
	//DEFAULT FIELDS
	int distance = 0;
	bool walkable = true;
	bool piece = false;
};

