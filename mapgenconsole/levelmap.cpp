#include "levelmap.h"

using namespace std;

levelmap::~levelmap()
{
}

void levelmap::update()
{
	cH.UpdateActiveChunks(sel.selectedpos, sel.oldpos);
}

bool levelmap::isbusy()
{
	if (generating | loading)
		return true;
	else return false;
}

float levelmap::getVariation(int x, int y)
{
	return temp[x][y] + ((x + y) / 1000);
}

void levelmap::addToSurface(int x, int y, int newSurfType)
{
	surfaceTypes[x][y] = newSurfType;
}

void levelmap::changeType(int x, int y, int newType)
{
	blockTypes[x][y] = newType;
}

float levelmap::getZScale(int type)
{
	return blockTypes::getZScale(type);
}

float levelmap::getZScale(int x, int y)
{
	return getZScale(type(x, y));
}

bool levelmap::openTreasure(int xi, int yi)
{
	//TODO open and remove item
	if (surfaceTypes[xi][yi] == surfaceTypes::chest)
	{
		surfaceTypes[xi][yi] = surfaceTypes::collectedChest;
		return true;
	}
	return false;
}

bool levelmap::isValidMove(int x, int y)
{
	return blockTypes::isWalkable(blockTypes[x][y]);
}

void levelmap::setoffset(float off)
{
	offset = off;
}

float levelmap::getoffset()
{
	return offset;
}

float levelmap::getSpawnOffset(int type)
{
	return blockTypes::getSpawnOffset(type);
}

int levelmap::getMoveDist(coord a, coord b)
{
	if (square4hex6 == true) //hex
		return hexdist::calcdist(walkable, a, b, lenx, leny);
	else
	{
		int size = 0;
		quickgrid::mainPath(lenx, leny, walkable, a.x, a.y, b.x, b.y, size);
		return size + 1; //and the start point
	}
}