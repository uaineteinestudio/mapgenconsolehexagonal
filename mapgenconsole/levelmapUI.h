#pragma once

#include "ui/ui.h"
#include "levelmap.h"
#include "chunks/ChunkHandlerUI.h"
#include "moveability.h"
#include <chrono>

class levelmapUI : public levelmap
{
public:
	levelmapUI() : levelmap()
	{
		//add anything here
	}
	levelmapUI(string fn, int lx, int ly, bool squareorhex, bool print) : levelmap(fn, lx, ly, squareorhex)
	{
		printinfo = print;
		//add anything here
	}
	levelmapUI(string fn, int lx, int ly, bool squareorhex, bool print, bool setStaticHeightBorder) : levelmap(fn, lx, ly, squareorhex, setStaticHeightBorder)
	{
		printinfo = print;
		//add anything here
	}
	~levelmapUI();

	//METHODS AND FUNCTIONS
	void printMaps();	//UI feature
	void printFootPathCaveOverlap(); //bug detect method
protected:
	//print info/ui
	bool printinfo = false;

	//UI PRINTING TO CONSOLE
	void printWaterfalls(int size);
	void printScaleMap();
	void printChests();
};

