//	Uaine Teine 2020
//	Daniel Stamer-Squair
//	See licence info
#pragma once

static const int MaxLen = 140;	//Be sure to be under 1000 as this can cause stack overflow
static const int rotLen = 4; 	//4 square
static const int rotLenHex = 6; //hexagonal map