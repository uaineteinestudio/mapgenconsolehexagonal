#pragma once

#include <string>

using namespace std;

class Named
{
public:
	Named();
	Named(string n);
	~Named();
	string getName()
	{
		return name;
	}

	//implicit conversion
	operator string() const { return name; }
protected:
	string name;
};

