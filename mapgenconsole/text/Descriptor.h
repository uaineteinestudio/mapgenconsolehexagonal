#pragma once

#include <string>

using namespace std;

class Descriptor
{
public:
	Descriptor();
	Descriptor(string desc);
	~Descriptor();
	string getDescription()
	{
		return description;
	}

	//implicit conversion
	operator string() const { return description; }
protected:
	string description;
};

