#pragma once

#include "mapdata.h"
#include "sightmap/DiscoveryMap.h"

class mapvisibility : public mapdata 
{
public:
	mapvisibility() : mapdata(MaxLen, MaxLen, false)
	{
		DiscoveryMap newmap = DiscoveryMap(0, MaxLen, MaxLen);
		sightMap = &newmap;
	}
	mapvisibility(int lx, int ly, bool squareorhex) : mapdata(lx, ly, squareorhex) //0 is square, 1 is hex
	{
		DiscoveryMap newmap = DiscoveryMap(0, lx, ly);
		sightMap = &newmap;
	};
	~mapvisibility();

	//discoverymap
	void newSightMap(DiscoveryMap* newmapref) { sightMap = newmapref; }
	DiscoveryMap* sightMap;
	int currentPlayerSightMap() { return sightMap->IamPlayer(); }
protected:
	//OVERRIDES
	//DYNAMIC MAP ARRAYS FOR TILE DAT
	void initialisearrs() override;			//initalise arrays
	void deletearrs() override;				//delete my arrays :'(
};

