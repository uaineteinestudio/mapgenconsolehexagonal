#pragma once

#include "genclass.h"
#include "adjacency/hexadjacency.h"
//#include "dynInitaliser.h"
#include "boundaries/BoundaryMaps.h"
#include "cellularautomata/cellautoma.h"
//#include "coord.h"
#include <random>

class stonegenerator : public genclass
{
public:
	stonegenerator(int w, int h) : genclass(w, h)
	{
		//add anything more here
		initalise();
	}
	stonegenerator(float fracgrain, int w, int h) : genclass(w, h)
	{
		//add anything more here
		fracgraingrass = fracgrain;
		initalise();
	}
	~stonegenerator();
	void generateStoneGrain(int** bt, int ** st); //not next to each other
	void makestoneTiles(int** bt, int ** st);
	void smoothOutStone(int** bt, int** st);
private:
	//stone to exist around already existing caves on intercept with CA
	float fracgraingrass = 0.05; //fraction for grain in general, doesn't have to be grass just not water and such
	float fracBigRock = 0.1; //one 10th bigger rocks
	float stonedirtchance = 0.77;
	float stonechance = 0.1;
	int numGrainGrass;
	void initalise();
	bool validStoneGrainSpot(int xi, int yi, int ** bt, int ** st);
	void StoneGrassType(int** bt, int** st, int type); //make stone grass the boundaries of this type
};