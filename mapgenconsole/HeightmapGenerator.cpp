#include "heightmapGenerator.h"

heightmapGenerator::~heightmapGenerator()
{
}

int ** heightmapGenerator::genNewMap()
{
	int** map = makeRandomHeightMap();
	smoothFac(map);
	return map;
}

int ** heightmapGenerator::makeRandomHeightMap()
{
	/* initialize random seed: */
	srand(time(NULL));

	int** hei = 0;							//give 0 value for now
	hei = new int*[MaxLen];					//x vals
	for (int x = 0; x < MaxLen; x++)
	{
		hei[x] = new int[MaxLen];			//y vals
		for (int y = 0; y < MaxLen; y++)
		{
			hei[x][y] = rand() % MaxHeight;	//0 to max height
		}
	}
	return hei;
}
