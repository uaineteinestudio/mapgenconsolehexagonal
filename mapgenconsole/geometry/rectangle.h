//CAN HANDLE NEGATIVE NUMBERS TOO

#pragma once

#include <cstdlib>

class rectangle
{
public:
	rectangle(int lx, int ly);
	~rectangle();

	//get dimensions
	int getlenx();
	int getleny();
	void getlen(int &x, int &y);//get both values
protected:
	int lenx;	//width
	int leny;	//height

	int area(); //always positive
};

