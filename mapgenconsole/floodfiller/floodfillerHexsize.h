//	Uaine Teine 2020
//	Daniel Stamer-Squair
//	See licence info
#pragma once

#include "floodfillerHex.h"
#include <algorithm>
//#include <iostream>

using namespace std;

class floodfillerHexsize : public floodfillerHex
{
public:
	floodfillerHexsize(int w, int h) : floodfillerHex(w, h) { };
	~floodfillerHexsize();
	//get these values only without painting the lists
	int getSize(int** curMap, int xi, int yi);
	int getSize(bool** curMap, int xi, int yi);
	void getSizeArray(int** curMap, int** sizeArr, int lenx, int leny); //return size array
	void getSizeArray(bool** curMap, int** sizeArr, int lenx, int leny); //return size array
};

