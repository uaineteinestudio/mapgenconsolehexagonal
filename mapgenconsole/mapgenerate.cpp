#include "mapgenerate.h"

mapgenerate::~mapgenerate()
{
}

void mapgenerate::FillBorder()
{
	//fill border
	for (int i = 0; i < lenx; i++)
	{
		blockTypes[0][i] = blockTypes::boundary;
		blockTypes[lenx - 1][i] = blockTypes::boundary;
		if (setStaticHeightBorders)
		{
			blockHeights[0][i] = DefBoundaryH;
			blockHeights[lenx - 1][i] = DefBoundaryH;
		}
	}
	for (int i = 0; i < leny; i++)
	{
		blockTypes[i][0] = blockTypes::boundary;
		blockTypes[i][leny - 1] = blockTypes::boundary;
		if (setStaticHeightBorders)
		{
			blockHeights[i][0] = DefBoundaryH;
			blockHeights[i][leny - 1] = DefBoundaryH;
		}
	}
}

void mapgenerate::finaliseTemp()
{
	int sf = 2;
	tempCalc tc = tempCalc(MaxLen, sf);
	tc.calctemp(temp, blockTypes, blockHeights);
}

void mapgenerate::findWalkable()
{
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			if (blockTypes::isWalkable(blockTypes[x][y])) //base is walkable
			{
				if (surfaceTypes::isWalkable(surfaceTypes[x][y]))//top is walkable
					walkable[x][y] = true;
				else
					walkable[x][y] = false;
			}
			else
				walkable[x][y] = false;
		}
	}
	walkableTranspose = dynInitaliser::transpose(walkable, MaxLen, MaxLen);
}

//Steps for map gen
//heightmap first
//types
//water
void mapgenerate::GenerateMap()
{
	generating = true;
	makeHeightMap();
	int lG = 0;
	int hG = 0;
	makegrassPatches(lG, hG);//returns the sizes of grasses
	makeCaves();
	makeWater();//makeSand();-called from makeWater
	makeWaterfalls();
	finaliseTemp();
	makeFoliage();
	makeStone(); //stone smoothing handled in here
	findWalkable();
	surfaceTileCleanup();
	findWalkable();
	establishFootpaths();
	FillBorder();
	findWalkable();
	generating = false;
}

void mapgenerate::makeHeightMap()
{
	dynInitaliser::del(blockHeights, MaxLen);
	heightmapGenerator hG = heightmapGenerator(MaxLen, MaxLen, heightmapsmoothfac);
	blockHeights = hG.genNewMap();
}

void mapgenerate::makeCaves()
{
	//make my caves here
	caveGenerator cg = caveGenerator(lenx, leny, blockTypes::caveInterior);//make CAchest constructor
	dynInitaliser::del(Caves, MaxLen);
	Caves = cg.makeCaves();
	cg.purgeCaves(Caves, square4hex6);
	//if a cave make types change
	cg.setCaves(Caves, blockTypes);
	//now fill interior
	cg.setInteriorCellsHex(Caves, blockTypes, blockHeights);
	cg.caveCleanup(Caves, blockTypes, blockTypes::caveInterior, square4hex6);
	//place chests
	vector<coord> chestLocations;
	int nChests = 0;
	cg.PlaceChests(Caves, blockTypes, surfaceTypes, chestLocations, nChests);
	chests.clear();
	for (int i = 0; i < nChests; i++)
	{
		randomchest newChest = randomchest(chestLocations[i]);
		//TODO put treasure in chest
		chests.push_back(newChest);
	}
}

void mapgenerate::makegrassPatches(int &amountLGrass, int &amountHGrass)
{
	//todo make heavy and light grass a thing
	//Could use CA?
	//Need to make that clustering yeah
	CA lg = CA(0.6, 5, 4, 5);
	bool** lightgrass = lg.cellautomata(lenx, leny);
	int type = 0;
	amountLGrass = 0;
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			type = blockTypes[x][y];
			if (lightgrass[x][y]) //is grass
			{
				amountLGrass += 1;
				if (blockTypes::isGrass(type))
				{
					blockTypes[x][y] = blockTypes::lightgrass;
				}
			}
		}
	}

	bool** heavygrass = lg.cellautomata(lenx, leny);
	amountHGrass = 0;
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			type = blockTypes[x][y];
			if (heavygrass[x][y]) //is heavy grass
			{
				if (!lightgrass[x][y]) //is not light grass
				{
					amountHGrass += 1;
					if (blockTypes::isGrass(type))
					{
						blockTypes[x][y] = blockTypes::heavygrass;
					}
				}
			}
		}
	}
	//cleanup
	dynInitaliser::del(lightgrass, lenx);
	dynInitaliser::del(heavygrass, lenx);
}

void mapgenerate::makeWater()
{
	waterGenerator wg = waterGenerator(190);
	wg.getWater(water, blockHeights);//returns the water arr
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			if (water[x][y] && Caves[x][y] == false) //no caves allowed
				blockTypes[x][y] = blockTypes::water;
		}
	}
	wg.makeSand(blockTypes, water);
}

void mapgenerate::makeWaterfalls()
{
	waterfallGenerator wg = waterfallGenerator(lenx, leny);
	int size = wg.findWaterfalls(waterfalls, blockTypes, water);
	//printWaterfalls(size);
	int x = 0;
	int y = 0;
	for (int i = 0; i < size; i++)
	{
		x = waterfalls[i].getX();
		y = waterfalls[i].getY();
		surfaceTypes[x][y] = surfaceTypes::waterfall;
	}
}

void mapgenerate::makeFoliage()
{
	foliagegen fg = foliagegen(lenx, leny);
	fg.generateFoliage(blockTypes, surfaceTypes, temp);
}

void mapgenerate::makeStone()
{
	stonegenerator sg = stonegenerator(lenx, leny);
	sg.makestoneTiles(blockTypes, surfaceTypes);
	sg.generateStoneGrain(blockTypes, surfaceTypes);
	sg.smoothOutStone(blockTypes, surfaceTypes);
}

void mapgenerate::establishFootpaths()
{
	footpathgen fg = footpathgen(lenx, leny);
	fg.establishFootpaths(footpaths, walkableTranspose, blockTypes, rot, square4hex6);
	fg.smoothFootpaths(footpaths, blockHeights, blockTypes, square4hex6);
}

void mapgenerate::surfaceTileCleanup()
{
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			if (blockTypes[x][y] == blockTypes::footpath)
				surfaceTypes[x][y] = surfaceTypes::air; //nothing where there are footpaths
		}
	}
}