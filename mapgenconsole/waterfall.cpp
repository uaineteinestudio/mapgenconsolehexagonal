#include "waterfall.h"

waterfall::waterfall(int xi, int yi, bool falloffDirs[rotLen])
{
	pos = coord(xi, yi);
	for (int i = 0; i < rotLen; i++)
	{
		dirs[i] = falloffDirs[i];
	}
}

waterfall::waterfall(coord p, bool falloffDirs[rotLen])
{
	pos = p;
	for (int i = 0; i < rotLen; i++)
	{
		dirs[i] = falloffDirs[i];
	}
}

waterfall::~waterfall()
{
}

int waterfall::getX()
{
	return pos.x;
}

int waterfall::getY()
{
	return pos.y;
}

coord waterfall::getpos()
{
	return pos;
}
