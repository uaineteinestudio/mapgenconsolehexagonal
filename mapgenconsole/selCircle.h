#pragma once

#include "adjacency/squareadjacency.h"
#include "arrays/dynInitaliser.h"
#include "pathfinding/hexdist.h"
#include "boundaries/boundchecker.h"
#include "coord.h"
#include "moveability.h"
#include "player\boardobjs\piece.h"
#include <vector>
#include <algorithm>    // std::min
//#include "parallelfor.h"

using namespace std;

static const int SelCirLen = 14;	//15 if we include the start point and then there's the original hex ones to be added in later
static const int SelTotalLen = 20;
static const int colSelMat[2][SelCirLen] =
{
	{-2,-1, 0,+1,+2,+2,-2,-2,-2, 0,-1,-1,+2,+1},
	{ -2,-1, 0,+1,+2,+2,-2,-2,-2, 0,-1,-1,+2,+1 }
};
static const int rowSelMat[2][SelCirLen] =
{
	{ -1,-1,-2,-1,0, -1,-1,+1,0, +2,+2,-1,+1,+2 },
	{ -1,-2,-2,-2,0,-1,-1,+1,0,+2,+1,-2,+1,+1 }
};

static class selCircle
{
public:
	static void getSelCircleM(coord p, vector<coord> &selcircle, vector<moveability> &move, int lenx, int leny, int rad, bool sqHex);		//return selection circle as vector-used for moveability class instead of int distances
	static void getSelCircleM(int x, int y, vector<coord> &selcircle, vector<moveability> &move, int lenx, int leny, int rad, bool sqHex);	//return selection circle as vector-used for moveability class instead of int distances
	static void updateSelCirclePiece(int x, int y, vector<coord> & selcircle, vector<moveability> & move, piece* thePiece);					//return the new size after removing the piece walkability
	/*
	static void getSelCircle(coord p, vector<coord> &selcircle, vector<int> &dists, int lenx, int leny);						//return selection circle as vector
	static void getSelCircle(int x, int y, vector<coord> &selcircle, vector<int> &dists, int lenx, int leny);					//return selection circle as vector
	*/
};

