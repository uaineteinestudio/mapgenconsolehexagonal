#include "footpathgen.h"

footpathgen::~footpathgen()
{
}

void footpathgen::finaliseRot(int** rotmap, bool** footpathmap)
{
	//make 0 for footpath
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			if (footpathmap[x][y])
			{
				//if a footpath
				rotmap[x][y] = 0; //no rotation
			}
		}
	}
}

void footpathgen::establishFootpaths(bool ** footpaths, bool ** walkable, int ** blockTypes, int** rotmap, bool squareorhex) //0 for square, 1 for hex
{
	//get 2 points at other ends of the map
	/* initialize random seed: */
	srand(time(NULL));
	quickgrid q = quickgrid();
	vector<coord> path;

	int sx = 0;
	int sy = 0;
	int ex = 0;
	int ey = 0;

	//for the first half
	int i = 0;//index at 0
	int size = 0;
	while (i < attemptFootpathLim)
	{
		getRandomNode(0, sx, sy, 3);
		getRandomNode(2, ex, ey, 3);
		if (squareorhex == false) //square
			path = q.mainPath(lenx, leny, walkable, sx, sy, ex, ey, size);
		else
			path = q.mainPathHex(lenx, leny, walkable, sx, sy, ex, ey, size);
		ConnectPoints(footpaths, walkable, blockTypes, path);
		if (size > 2) //consider it a success
			break;
		i += 1;//increase index
	}

	vector<coord> path2;
	int mainsize = size;
	size = 0;
	i = 0;//index at 0 reset
	while (i < attemptFootpathLim)
	{
		//now get random point from a path
		int lowerlim = (mainsize / 2) - 2;
		if (lowerlim < 1)
			lowerlim = 1;
		int nodeind = getrandInt(lowerlim, 6);
		ex = path[nodeind].x;
		ey = path[nodeind].y;
		int select = getrandInt(0, 2);
		int corn = 1;
		if (select == 0)
			corn = 3;
		//now that I have that, get me random node
		getRandomNode(corn, sx, sy, 3);
		if (squareorhex == false) //square
			path2 = q.mainPath(lenx, leny, walkable, sx, sy, ex, ey, size);
		else
			path2 = q.mainPathHex(lenx, leny, walkable, sx, sy, ex, ey, size);
		ConnectPoints(footpaths, walkable, blockTypes, path2);
		if (size > 2) //consider it a success
			break;
		i += 1;//increase index
	}

	//4th path that could be programmed in by 50% chance
	if (rand() % 2 == 0)
	{
		vector<coord> path3;
		i = 0;
		int mainsize = size;
		size = 0;
		//make a 4th path here now
		while (i < attemptFootpathLim)
		{
			getRandomNode(0, sx, sy, 3);
			sy = sy + (leny / 2) + (rand() % 3 - 1);
			if (squareorhex == false) //square
				path3 = q.mainPath(lenx, leny, walkable, sx, sy, ex, ey, size);
			else
				path3 = q.mainPathHex(lenx, leny, walkable, sx, sy, ex, ey, size);
			ConnectPoints(footpaths, walkable, blockTypes, path3);
			if (size > 2) //consider it a success
				break;
			i += 1;//increase index
		}
	}

	//finalse rotation
	finaliseRot(rotmap, footpaths);
}

void footpathgen::smoothFootpaths(bool ** footpaths, int ** heights, int ** bt, bool sqhex)
{
	//first get the path itself from the map
	vector<coord> footpathcoords = getFootpathFromMap(footpaths, bt, sqhex);
	//smooth point
	for (int k = 0; k < footpathsmoothN; k++)
	{
		for (int i = 0; i < footpathcoords.size(); i++)
		{
			smoothPoint(footpathcoords[i].x, footpathcoords[i].y, heights, sqhex);
		}
	}
}

void footpathgen::ConnectPoints(bool ** footpaths, bool ** walkable, int ** blockTypes, vector<coord> path)
{
	int x = 0;
	int y = 0;
	for (int i = 0; i < path.size(); i++)
	{
		x = path[i].x;
		y = path[i].y;
		footpaths[x][y] = true;
		walkable[x][y] = true;
		blockTypes[x][y] = blockTypes::footpath;
	}
}

void footpathgen::getRandomNode(int corner, int & x, int & y, int limitFrom)
{
	switch (corner)
	{
	case 1:
		x = lenx - 1 - (rand() % limitFrom);
		y = (rand() % limitFrom);
		break;
	case 2:
		x = lenx - 1 - (rand() % limitFrom);
		y = leny - 1 - (rand() % limitFrom);
		break;
	case 3:
		x = (rand() % limitFrom);
		y = leny - 1 - (rand() % limitFrom);
		break;
	default://same as 0
		x = (rand() % limitFrom);
		y = (rand() % limitFrom);
	}
}

int footpathgen::getrandInt(int min, int range)
{
	return min + (rand() % range);
}

vector<coord> footpathgen::getFootpathFromMap(bool ** footpathmap, int** bt, bool sqhex)
{
	int col[HexAdjLen];
	int row[HexAdjLen];

	int sqcol[4] = { -1, 0, 0, 1 };
	int sqrow[4] = { 0, -1, 1, 0 };

	vector<coord> path = vector<coord>();
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			if (footpathmap[x][y])
			{
				//if footpath
				path.push_back(coord(x, y));
				if (sqhex == true)			//is hex
				{
					hexAdjaceny::WriteColRow(col, row, x, y);
					for (int i = 0; i < HexAdjLen; i++)
					{
						int xi = x + col[i];
						int yi = y + row[i];
						if (boundchecker::inBounds(xi, yi, lenx, leny))
						{
							if (blockTypes::notWater(bt[xi][yi]))
								path.push_back(coord(xi, yi));
						}
					}
				}
				else						//is square
				{
					for (int i = 0; i < 4; i++)
					{
						int xi = x + sqcol[i];
						int yi = y + sqrow[i];
						if (boundchecker::inBounds(xi, yi, lenx, leny))
						{
							if (blockTypes::notWater(bt[xi][yi]))
								path.push_back(coord(xi, yi));
						}
					}
				}
			}
		}
	}
	return path;
}

void footpathgen::smoothPoint(int x, int y, int ** heights, bool sqhex)
{
	float sum = 0;

	int sqcol[4] = { -1, 0, 0, 1 };
	int sqrow[4] = { 0, -1, 1, 0 };

	int col[HexAdjLen];
	int row[HexAdjLen];
	hexAdjaceny::WriteColRow(col, row, x, y);

	int xi = 0;
	int yi = 0;
	int n = 0;
	if (sqhex == true) //hex
	{
		for (int i = 0; i < HexAdjLen; i++)
		{
			xi = x + col[i];
			yi = y + row[i];
			if (boundchecker::inBounds(xi, yi, lenx, leny))
			{
				sum += heights[xi][yi];
				n += 1;
			}
		}
	}
	else //square
	{
		for (int i = 0; i < 4; i++)
		{
			xi = x + sqcol[i];
			yi = y + sqrow[i];
			if (boundchecker::inBounds(xi, yi, lenx, leny))
			{
				sum += heights[xi][yi];
				n += 1;
			}
		}
	}
	//now take average across all of them you know
	heights[x][y] = (int)(sum / n);
}
