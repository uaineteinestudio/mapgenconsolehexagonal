#include "version.h"

version::version(int ndigits, int vNumbers[])
{
	if (ndigits == maxDigits) //4
	{
		_Major = vNumbers[0];
		_Minor = vNumbers[1];
		_Fix = vNumbers[2];
		_Build = vNumbers[3];
	}
	else if (ndigits == 3)
	{
		_Major = vNumbers[0];
		_Minor = vNumbers[1];
		_Fix = vNumbers[2];
		_Build = 0;
	}
	else if (ndigits == 2)
	{
		_Major = vNumbers[0];
		_Minor = vNumbers[1];
		_Fix = 0;
		_Build = 0;
	}
	else if (ndigits == 1)
	{
		_Major = vNumbers[0];
		_Minor = 0;
		_Fix = 0;
		_Build = 0;
	}
	else
	{
		_Major = 0;
		_Minor = 0;
		_Fix = 0;
		_Build = 0;
	}
	setString();
}

version::version(int o, int t, int th, int f)
{
	_Major = o;
	_Minor = t;
	_Fix = th;
	_Build = f;
	setString();
}

version::version()
{
	_Major = 0;
	_Minor = 0;
	_Fix = 0;
	_Build = 0;
	setString();
}

version::~version()
{
}

void version::NewVersion(int o, int t, int th, int f)
{
	_Major = o;
	_Minor = t;
	_Fix = th;
	_Build = f;
	setString();
}

void version::setString()
{
	stringstream ss;
	ss << _Major << "." << _Minor << "." << _Fix << "." << _Build;
	string str; // a variable of str data type
	ss >> str;
	VersionString = str;
}
