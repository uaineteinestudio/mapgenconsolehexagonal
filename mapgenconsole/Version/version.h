#pragma once

#include <string>
#include <sstream>

using namespace std;

class version
{
public:
	version(int ndigits, int vNumbers[]);
	version(int o, int t, int th, int f);
	version();
	~version();
	const int maxDigits = 4;
	int GetMajor() { return _Major; }
	int GetMinor() { return _Minor; }
	int GetFix() { return _Fix; }
	int GetBuild() { return _Build; }
	string VersionString;
	void NewVersion(int o, int t, int th, int f);
protected:
	int _Major;
	int _Minor;
	int _Fix;
	int _Build;
private:
	void setString();
};

