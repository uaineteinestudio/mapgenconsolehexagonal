#pragma once

#include "version.h"

class VersionControl
{
public:
	VersionControl();
	~VersionControl();
	version MapVersion;
	version PlayerVersion;
	version ItemsVersion;
	version CompleteVersion;
};

