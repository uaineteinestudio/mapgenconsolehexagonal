#pragma once

#include "levelmapIO.h"
#include "arrays/dynInitaliser.h"
#include "chunks/chunk.h"
#include "pathfinding/hexdist.h"
#include "pathfinding/quickgrid.h"
#include "coord.h"
#include "chunks/ChunkHandler.h"
#include <vector>
#include "selCircle.h"
#include "selClass.h"
// NON UE FRIENDLY

class levelmap : public levelmapIO
{
public:
	levelmap() : levelmapIO()
	{
		//anything to add
	};
	levelmap(string fn, int lx, int ly, bool squareorhex) : levelmapIO(fn, lx, ly, squareorhex) //0 is square, 1 is hex
	{
		//anything to add here
	}; 
	levelmap(string fn, int lx, int ly, bool squareorhex, bool setStaticHeightBorder) : levelmapIO(fn, lx, ly, squareorhex, setStaticHeightBorder)
	{
		//anything to add here
	};
	~levelmap();

	//tick like update
	void update();

	//AM I BUSY?
	bool isbusy();

	//VARIATION FLOAT BASED ON LOCATION AND TEMP
	float getVariation(int x, int y); //based on location

	//CHANGE MY TYPES
	void addToSurface(int x, int y, int newSurfType);//add this to the surface layer at pos (x,y)
	void changeType(int x, int y, int newType);

	//GET SCALE OF BLOCK
	float getZScale(int type);
	float getZScale(int x, int y);

	//spawn offset
	void setoffset(float off);
	float getoffset();
	//modification offset
	float getSpawnOffset(int type);

	//movement distance
	int getMoveDist(coord a, coord b);
private:
	//spawn offset
	float offset = 271;

	bool openTreasure(int xi, int yi);
	bool isValidMove(int x, int y); //returns moveability of position
};
