#include "Temp.h"

tempCalc::~tempCalc()
{
}

void tempCalc::calctemp(float** temp, int** type, int** height)
{
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			int t = type[x][y];
			int he = height[x][y];
			temp[x][y] = tmpvalue(t, he);
		}
	}
	//now smooth
	for (int i = 0; i < smoothfac; i++)
	{
		smoothit(temp);
	}
	Normalise(temp);
}

float tempCalc::tmpvalue(int type, int height)
{
	float val = 0;
	switch (type)
	{
	case blockTypes::stone:
		val = 1;
		break;
	case blockTypes::grass:
		val = 0.3;
		break;
	case blockTypes::water://I hope this is water
		val = 0.0001;
		break;
	case blockTypes::cave:
		val = 0.5;
		break;
	case blockTypes::caveInterior:
		val = 0.4;
	default:
		val = 1;
		break;
	}
	val -= height * 0.001;
	return val;
}

void tempCalc::smoothit(float** cur)
{
	/* initialize random seed: */
	srand(time(NULL));

	int col[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
	int row[8] = { 0, -1, 1, -1, 1, -1, 0, 1 };

	//make old list
	float old[MaxLen][MaxLen];
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			old[x][y] = cur[x][y];
		}
	}

	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			float smthed = cur[x][y];
			int count = 1;
			for (int k = 0; k < 8; k++)
			{
				smthed += getvecval(old, x + col[k], y + row[k], count);
			}
			smthed = smthed / count;
			float rndm = static_cast <float> (rand()) / static_cast <float> (RAND_MAX); //between 0 and 1
			float variation = (rndm * 2) - 1; //between -1 and 1
			smthed += variation * varamount; //between -0.2 and 0.2
			//now overwrite that value?
			cur[x][y] = smthed;
			if (isneg(cur[x][y]))
				cur[x][y] = 0; //check zero
		}
	}
}

bool tempCalc::isneg(float val)
{
	if (val < 0)
		return true;
	else
		return false;
}

float tempCalc::getvecval(float** cur, int x, int y, int & num)
{
	float out = 0;
	if (boundchecker::inBounds(x, y, MaxLen, MaxLen))
	{
		out = cur[x][y];
		num += 1;
	}
	return out;
}

float tempCalc::getvecval(float cur[MaxLen][MaxLen], int x, int y, int & num)
{
	float out = 0;
	if (boundchecker::inBounds(x, y, MaxLen, MaxLen))
	{
		out = cur[x][y];
		num += 1;
	}
	return out;
}

void tempCalc::Normalise(float** cur)
{
	float greatest = largest(cur);
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			cur[x][y] = cur[x][y]/greatest;//normalise for the set to be between 0 and 1.
			if (isneg(cur[x][y]))
				cur[x][y] = 0;
		}
	}
}

float tempCalc::largest(float** cur)
{
	float greatest = -2;
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			if (cur[x][y] > greatest)
				greatest = cur[x][y];//overwrite it
		}
	}
	return greatest;
}
