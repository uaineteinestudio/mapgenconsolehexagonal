#pragma once

#include "piece.h"
#include "../../coord.h"
#include <vector>

using namespace std;

static const int numTypes = 5;

static class defpieces
{
public:
	static vector<piece> getDefaultPieces(int belongsTo, int lenx, int leny);
	static vector<piece> getDefaultPieces(int belongsTo, int startIndex, int lenx, int leny);
	static piece getFromType(int type, coord pos, int belongs, int newind, int lenx, int leny);
};

class wizard : public piece
{
public:
	wizard(coord pos, int belongsTo, int ind, int lenx, int leny) : piece(pos, belongsTo, 0, ind, "Wizard", lenx, leny)
	{
		//extra
		pieceStats = piecestats(2, 2, 100, 100, 1, 1, true, true, false, 2);
	}
};

class squire : public piece
{
public:
	squire(coord pos, int belongsTo, int ind, int lenx, int leny) : piece(pos, belongsTo, 1, ind, "Squire", lenx, leny)
	{
		//extra
		pieceStats = piecestats(2, 2, 100, 100, 1, 1, true, false, false, 1);
	}
};

class hero : public piece
{
public:
	hero(coord pos, int belongsTo, int ind, int lenx, int leny) : piece(pos, belongsTo, 2, ind, "Hero", lenx, leny)
	{
		//extra
		pieceStats = piecestats(3, 2, 100, 100, 1, 1, true, false, false, 1);
	}
};

class healer : public piece
{
public:
	healer(coord pos, int belongsTo, int ind, int lenx, int leny) : piece(pos, belongsTo, 3, ind, "Healer", lenx, leny)
	{
		//extra
		pieceStats = piecestats(2, 2, 100, 100, 1, 1, true, false, true, 1);
	}
};

class rouge : public piece
{
public:
	rouge(coord pos, int belongsTo, int ind, int lenx, int leny) : piece(pos, belongsTo, 4, ind, "Rouge", lenx, leny)
	{
		//extra
		pieceStats = piecestats(3, 3, 100, 100, 1, 1, true, false, false, 2);
	}
};
