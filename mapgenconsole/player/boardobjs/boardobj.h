#pragma once

#include "../../coord.h"
#include "../../text/Named.h"

class boardobj : public Named //abstract class
{
public:
	boardobj(coord p, string n) : Named(n)
	{
		pos = p;
		name = n;
	}
	boardobj(int xi, int yi, string n) : Named(n)
	{
		pos = coord(xi, yi);
		name = n;
	}
	~boardobj();
	coord getPos();
	bool inPos(coord p); //return if in this position
	void PosOverride(coord newp);
protected:
	coord pos;
};

