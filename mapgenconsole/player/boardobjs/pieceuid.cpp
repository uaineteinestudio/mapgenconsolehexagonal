#include "pieceuid.h"

string pieceuid::getuid(int plind, int pieceind)
{
	stringstream ss;
	ss << to2String(plind) << to2String(pieceind);
	return ss.str();
}

int pieceuid::getPieceIndex(string uid)
{
	string indexstring = uid.substr(2, 2);
	return atoi(indexstring.c_str());
}

bool pieceuid::matches(string uid1, string uid2)
{
	if (getPieceIndex(uid1) == getPieceIndex(uid2))
		if (getPlayerIndex(uid1) == getPlayerIndex(uid2))
			return true;
	return false; //else
}

int pieceuid::getPlayerIndex(string uid)
{
	string ownerstring = uid.substr(0, 2);
	return atoi(ownerstring.c_str());
}

string pieceuid::convertToString(int value)
{
	return to_string(value);
}

string pieceuid::to2String(int twodigitmax)
{
	string sv = convertToString(twodigitmax);
	stringstream ss;
	if (sv.length() == 1)
	{
		ss << "0" << sv;
	}
	else
	{
		ss << sv;
	}
	return ss.str();
}
