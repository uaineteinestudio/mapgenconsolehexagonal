#include "piecestats.h"

piecestats::~piecestats()
{
}

bool piecestats::isMelee()
{
	if (range.Default == 1)
		return true;
	else
		return false;
}

string piecestats::viewStats(string name)
{
	stringstream ss;
	ss << name << endl;
	ss << "Health: " << Health << "/" << HealthMax << endl;
	ss << "Attack: " << attack << endl;
	ss << "Defence: " << defence << endl;
	if (isMelee())
		ss << "Melee (1)" << endl;
	else
		ss << "Ranged (" << range << ")" << endl;
	if (canMove)
		ss << "Can Move" << endl;
	else
		ss << "Cannot Move" << endl;
	ss << "Visibility: " << visibility << endl;
	return ss.str();
}

int piecestats::getMoveDist()
{
	return movedist;
}
