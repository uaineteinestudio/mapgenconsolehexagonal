#pragma once

#include "..\..\items\stats.h"
#include <string>
#include <sstream>

using namespace std;

class piecestats : public stats
{
	//def piece friends
	friend class piece;
	friend class wizard;
	friend class rouge;
	friend class hero;
	friend class healer;
	//io friends
	friend class statparser;
	friend class pieceparser;
public:
	piecestats() : stats()
	{
		HealthMax = Health;
		alive = true;
		canMove = true;
		canPhase = false;
		canHeal = false;
		range = 1;//melee
		visibility = 2;
		movedist = 2;
	}
	piecestats(int movdis, int vis, float maxH, float curH, float atck, float def, bool canmove, bool phase, bool cHeal, float rang) : stats(curH, atck, def, rang)
	{
		HealthMax = maxH;
		alive = true;
		canMove = canmove;
		canPhase = phase;
		canHeal = cHeal;
		visibility = vis;
		movedist = movdis;
	}
	piecestats(iStatValue movdis, iStatValue vis, fStatValue maxH, bool canmove, bool phase, bool cHeal, stats st) : stats(st.Health, st.attack, st.defence, st.range)
	{
		HealthMax = maxH;
		alive = true;
		canMove = canmove;
		canPhase = phase;
		canHeal = cHeal;
		visibility = vis;
		movedist = movdis;
	}
	~piecestats();
	bool isMelee();		//defined as having a range of attack as 1
	string viewStats(string name); //view stats as a string with the linebreaks too
	int getMoveDist();
protected:
	fStatValue HealthMax = 100;
	iStatValue visibility = 2;
	iStatValue movedist = 2;
	bool alive = true;
	bool canMove = true;
	bool canPhase = false;
	bool canHeal = false;
};

