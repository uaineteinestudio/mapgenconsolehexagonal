#include "viewCircle.h"

/*
void viewCircle::getd1Circle(coord p, vector<coord> &viewcircle, int lenx, int leny)
{
	viewcircle.clear();
	vector<int> dists;
	int col[6];
	int row[6];
	hexAdjaceny::WriteColRow(col, row, p.x, p.y);
	for (int i = 0; i < 6; i++)
	{
		if (boundchecker::inBounds(col[i], row[i], lenx, leny))
		{
			viewcircle.push_back(coord(col[i], row[i]));
		}
	}
}

void viewCircle::getd2Circle(coord p, vector<coord>& viewcircle, int lenx, int leny)
{
	viewcircle.clear();
	//unused distance vector
	vector<int> dists;
	selCircle::getSelCircle(p, viewcircle, dists, lenx, leny);
	//el fino
	dists.clear(); //cleanup
}

void viewCircle::getd2Circle(int x, int y, vector<coord>& viewcircle, int lenx, int leny)
{
	getd2Circle(coord(x, y), viewcircle, lenx, leny);
}

void viewCircle::getd3Circle(coord p, vector<coord>& viewcircle, int lenx, int leny)
{
	getd2Circle(p, viewcircle, lenx, leny);
	//add in extra parts
	int sel = (p.x) % 2; //based on col
	coord point = coord(0, 0);
	for (int k = 0; k < Viewd3CirLen; k++)
	{
		point = coord(colViewMatd3[sel][k] + p.x, rowViewMatd3[sel][k] + p.y);
		if (boundchecker::inBounds(point.x, point.y, lenx, leny))
		{
			viewcircle.push_back(point);
		}
	}
}

void viewCircle::getd3Circle(int x, int y, vector<coord>& viewcircle, int lenx, int leny)
{
	getd3Circle(coord(x, y), viewcircle, lenx, leny);
}
*/

void viewCircle::getViewCircle(coord p, vector<coord>& viewcircle, int lenx, int leny, int rad, bool sqHex)
{
	if (sqHex)
	{
		hexAdjaceny::getRadiusCircle(p.x, p.y, rad, viewcircle, lenx, leny);
	}
	else
	{
		squareadjacency::getRadiusCircle(p.x, p.y, rad, viewcircle, lenx, leny);
	}

	//add centre
	viewcircle.push_back(p);
}

void viewCircle::getViewCircle(int x, int y, vector<coord>& viewcircle, int lenx, int leny, int rad, bool sqHex)
{
	getViewCircle(coord(x, y), viewcircle, lenx, leny, rad, sqHex);
}
