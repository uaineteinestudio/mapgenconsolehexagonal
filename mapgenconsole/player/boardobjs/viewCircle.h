#pragma once

#include "../../selCircle.h"

//d2 lengths
static const int ViewTotalLen = 20;
static const int ViewCirLen = 14;	//15 if we include the start point and then there's the original hex ones to be added in later
//d3
//lets just add extra parts from d2
static const int Viewd3CirLen = 18;	//16 if we include the start point and then there's the original hex ones to be added in later
static const int Viewd3TotalLen = ViewTotalLen + Viewd3CirLen;
static const int colViewMatd3[2][Viewd3CirLen] =
{
	{3,3,2,1,0,-1,-2,-3,-3,-3,-3,-2,-1,0,1,2,3,3},
	{3,3,2,1,0,-1,-2,-3,-3,-3,-3,-2,-1,0,1,2,3,3}
};
static const int rowViewMatd3[2][Viewd3CirLen] =
{
	{-1,0,-2,-2,-3,-2,-2,-1,0,1,2,2,3,3,3,2,2,1},
	{-1,0,-2,-2,-3,-2,-2,-1,0,1,2,2,3,3,3,2,2,1}
};

static class viewCircle
{
public:
	/*
	static void getd1Circle(coord p, vector<coord> &viewcircle, int lenx, int leny);				//return selection circle as vector
	static void getd1Circle(int x, int y, vector<coord> &viewcircle, int lenx, int leny);			//return selection circle as vector

	static void getd2Circle(coord p, vector<coord> &viewcircle, int lenx, int leny);				//return selection circle as vector
	static void getd2Circle(int x, int y, vector<coord> &viewcircle, int lenx, int leny);			//return selection circle as vector

	static void getd3Circle(coord p, vector<coord> &viewcircle, int lenx, int leny);				//return selection circle as vector
	static void getd3Circle(int x, int y, vector<coord> &viewcircle, int lenx, int leny);			//return selection circle as vector
	*/

	static void getViewCircle(coord p, vector<coord> &viewcircle, int lenx, int leny, int rad, bool sqHex);		//return selection circle as vector-arbitrary radius
	static void getViewCircle(int x, int y, vector<coord> &viewcircle, int lenx, int leny, int rad, bool sqHex);//return selection circle as vector-arbitrary radius
};

