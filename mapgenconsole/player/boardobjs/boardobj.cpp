#include "boardobj.h"

boardobj::~boardobj()
{
}

coord boardobj::getPos()
{
	return pos;
}

bool boardobj::inPos(coord p)
{
	return pos == p;
}

void boardobj::PosOverride(coord newp)
{
	pos = newp;
}
