#include "playerMovement.h"

playerMovement::playerMovement()
{
}

playerMovement::~playerMovement()
{
}

vector<coord>* playerMovement::getCirc()
{
	return &movecircle;
}

vector<moveability>* playerMovement::getMoveAbility()
{
	return &moveinfo;
}

void playerMovement::store(vector<coord> newCirc, vector<moveability> newMoveability, int newSize)
{
	movecircle = newCirc;
	moveinfo = newMoveability;
	size = newSize;
	initalised = true;
}

int playerMovement::getSize()
{
	return size;
}

bool playerMovement::isInit()
{
	return initalised;
}
