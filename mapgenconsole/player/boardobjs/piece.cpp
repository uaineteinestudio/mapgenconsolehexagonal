#include "piece.h"

piece::~piece()
{
}

string piece::viewStats()
{
	return pieceStats.viewStats(name);
}

int piece::getOwner()
{
	return owner;
}

int piece::getIndex()
{
	return index;
}

int piece::getIDType()
{
	return ID;
}

bool piece::killCheck()
{
	if (pieceStats.Health <= 0)
		kill();
	return pieceStats.alive;
}

void piece::kill()
{
	pieceStats.alive = false;
}

bool piece::isalive()
{
	return pieceStats.alive;
}

vector<coord> piece::getMovementCircle(vector<moveability>& move, int & size, int lenx, int leny, int rad, bool sqHex)
{
	vector<coord> output = vector<coord>();
	selCircle::getSelCircleM(pos, output, move, lenx, leny, rad, sqHex);
	size = output.size();
	return output;
}

void piece::movePiece(coord newP, int lenx, int leny, bool sqHex)
{
	PosOverride(newP);
	move.initalised = false;
	//now update move circle
	initaliseMovement(lenx, leny, sqHex);
}

vector<coord> piece::getViewCircle(int lenx, int leny, bool sqHex)
{
	//by distance. Need a d3 view circle programmed in first
	vector<coord> viewcirc = vector<coord>();
	switch (pieceStats.visibility)
	{
	case 0:
		//length 0
		viewcirc.push_back(pos);
	default:
		//length 2
		viewCircle::getViewCircle(pos, viewcirc, lenx, leny, pieceStats.range, sqHex);
	}
	return viewcirc;
}

void piece::initaliseMovement(int lenx, int leny, bool sqHex)
{
	vector<moveability> moveab;
	int size;
	vector<coord> circ = getMovementCircle(moveab, size, lenx, leny, pieceStats.movedist, sqHex);
	move.store(circ, moveab, size);
}
