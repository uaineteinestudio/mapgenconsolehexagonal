#pragma once

#include "../../coord.h"
#include "../../moveability.h"
#include <vector>

using namespace std;

class playerMovement
{
	friend class piece;
	friend class player;
public:
	playerMovement();
	~playerMovement();
	vector<coord>* getCirc();
	vector<moveability>* getMoveAbility();
	void store(vector<coord> newCirc, vector<moveability> newMoveability, int newSize);
	int getSize();
	bool isInit(); //check
protected:
	vector<coord> movecircle;
	vector<moveability> moveinfo;
	int size;
	bool initalised = false;
};

