#pragma once

#include "boardobj.h"
#include "piecestats.h"
//#include "../../moveability.h"
#include "playerMovement.h"
#include "../../selCircle.h"
#include "viewCircle.h"
#include <string>
#include <vector>
#include "pieceuid.h"

using namespace std;

class piece : public boardobj
{
	friend class playerData;
	friend class player;
	friend class playerUI;
	friend class playerHandler;
public:
	piece(coord p, int belongsTo, int id, int ind, piecestats s, string n, int lenx, int leny) : boardobj(p, n)
	{
		ID = id;
		owner = belongsTo;
		index = ind;
		uid = pieceuid::getuid(belongsTo, ind);
		pieceStats = s;
		initaliseMovement(lenx, leny, true);
	}
	piece(coord p, int belongsTo, int id, int ind, string n, int lenx, int leny) : boardobj(p, n)//DEFAULT STATS
	{
		ID = id;
		owner = belongsTo;
		index = ind;
		uid = pieceuid::getuid(belongsTo, ind);
		pieceStats = piecestats();
		initaliseMovement(lenx, leny, true);
	}
	piece(int xi, int yi, int belongsTo, int id, int ind, piecestats s, string n, int lenx, int leny) : boardobj(xi, yi, n)
	{
		ID = id;
		owner = belongsTo;
		index = ind;
		uid = pieceuid::getuid(belongsTo, ind);
		pieceStats = s;
		initaliseMovement(lenx, leny, true);
	}
	~piece();

	piecestats pieceStats;
	string viewStats();
	int getOwner();
	int getIndex();
	int getIDType();
	bool killCheck();	//kill if below 0, return if alive or not
	void kill();
	bool isalive();		//return if alive
	//GET MOVEMENT CIRCLE AROUND
	playerMovement move;
	vector<coord> getMovementCircle(vector<moveability> &move, int &size, int lenx, int leny, int rad, bool sqHex);	//removal of non walkable spots needs to take place, otherwise just a distance circle all set to true
protected:
	int ID;
	int index; //ID is 'type' not the index in the list it is stored
	string uid; //index in the pointer list-unique index
	int owner;
	void initaliseMovement(int lenx, int leny, bool sqHex); //all will be set to true here
	void movePiece(coord newP, int lenx, int leny, bool sqHex);
	//get view circle
	vector<coord> getViewCircle(int lenx, int leny, bool sqHex);
};

