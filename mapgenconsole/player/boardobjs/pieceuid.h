#pragma once

#include <string>
#include <sstream>

using namespace std;

//SEE pieceUID for info on this

static class pieceuid
{
public:
	static const int plPrefix = 1000;//SEE pieceUID for info on this
	static const int plPrefixLen = 3;
	static const int maxPiecesPerPlayer = plPrefix - 1;

	static string getuid(int plind, int pieceind);
	static int getPlayerIndex(string uid);
	static int getPieceIndex(string uid);

	static bool matches(string uid1, string uid2);
private:
	static string convertToString(int value);
	static string to2String(int twodigitmax);
};

