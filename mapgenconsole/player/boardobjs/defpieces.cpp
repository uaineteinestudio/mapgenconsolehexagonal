#include "defpieces.h"

using namespace std;

vector<piece> defpieces::getDefaultPieces(int belongsTo, int startIndex,int lenx, int leny)
{
	vector<piece> thepieces;
	for (int i = 0; i < 5; i++)
	{
		int index = startIndex + i;
		coord pos = coord(i, 0);
		thepieces.push_back(getFromType(i, pos, belongsTo, index, lenx, leny));
	}
	/* ABOVE CODE REPLACES THIS
	thepieces.push_back(hero(coord(0, 0), belongsTo, startIndex));
	thepieces.push_back(wizard(coord(1, 0), belongsTo, startIndex + 1));
	thepieces.push_back(rouge(coord(0, 1), belongsTo, startIndex + 2));
	thepieces.push_back(healer(coord(3, 3), belongsTo, startIndex + 3));
	thepieces.push_back(squire(coord(5, 4), belongsTo, startIndex + 4));
	*/
	return thepieces;
}

piece defpieces::getFromType(int type, coord pos, int belongs, int newind, int lenx, int leny)
{
	switch (type)
	{
	case 0:
		return wizard(pos, belongs, newind, lenx, leny);
		break;
	case 1:
		return squire(pos, belongs, newind, lenx, leny);
		break;
	case 2:
		return hero(pos, belongs, newind, lenx, leny);
		break;
	case 3:
		return healer(pos, belongs, newind, lenx, leny);
		break;
	case 4:
		return rouge(pos, belongs, newind, lenx, leny);
		break;
	default: //default should we pick something else
		return squire(pos, belongs, newind, lenx, leny);
	}
}

vector<piece> defpieces::getDefaultPieces(int belongsTo, int lenx, int leny)
{
	return getDefaultPieces(belongsTo, 0, lenx, leny);
}