#pragma once

//#include "../boardobjs/boardobj.h"
//#include "../boardobjs/piece.h"
#include "../../levelmap.h"	//discovery map included
#include "../boardobjs/defpieces.h"
#include "../../text/Named.h"
#include "../../items/inventory.h"

class playerData : public Named
{
public:
	playerData(levelmap* initmap, int ind, string n) : Named(n)
	{
		index = ind;
		mappointer = initmap;
		InventoryStash = inventory();
		sightMap = DiscoveryMap(ind, initmap->getlenx(), initmap->getleny());
	}
	//load file constructor
	playerData(int ind, string n, inventory inv) : Named(n)
	{
		index = ind;
		InventoryStash = inv;
	}
	~playerData();

	void destroy();		//deconstruct to make way for another

	inventory InventoryStash;
	//MEMBERS
	int getIndex();
	int newpieceindex();
	int getNoPieces();
	piece* getPieceByIndex(int ind);
	bool doesPieceExist(int index);

	//map pointer
	void setNewMapPointer(levelmap* newpointer);

	//sightmap
	DiscoveryMap sightMap;
protected:
	int index = 0;
	vector<piece> playerPieces;
	int NoPieces = 0;

	void addPiece(piece addthis);

	//discover a place
	void discover(int x, int y);							//discover things based on what the players can see-requires levelmap pointer to be set to an initalisedmap
	void discover(coord point);								//discover things based on what the players can see-requires levelmap pointer to be set to an initalisedmap
	void setVisibility(piece* p, int lenx, int leny);		//set visibility based on what this piece can see
	void setVisibilityAllPieces(int lenx, int leny);

	//MAP POINTER
	levelmap * mappointer;
};

