#pragma once

#include "playerColours.h"
#include "../../io/playerparser.h"

class playerIO : public playerColours
{
public:
	playerIO() : playerColours(0, 0, 0, "not given", inventory(), false, playerColours::defMovement)
	{

	}
	playerIO(levelmap* initmap, int colr, int sty) : playerColours(initmap, colr, sty)
	{
		//extra things here
	}
	playerIO(levelmap* initmap, int colr, int sty, int ind, string n) : playerColours(initmap, colr, sty, ind, n)
	{
		//extra things here
	}
	//load file constructor
	playerIO(int colr, int sty, int ind, string n, inventory inv, bool ai, iStatValue movementCash) : playerColours(colr, sty, ind, n, inv, ai, movementCash)
	{
		//extra things here
	}

	//void savePlayer();
	//void loadPlayer();
};

