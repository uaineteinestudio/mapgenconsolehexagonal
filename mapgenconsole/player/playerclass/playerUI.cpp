#include "playerUI.h"
//MAYBE NOT UE FRIENDLY
//#include <algorithm>
#include "../../ui/ui.h"

playerUI::~playerUI()
{
}

void playerUI::printPlayerInfo(levelmap * initialisedmap, int w, int h)
{
	printPlayerMap(initialisedmap, w, h);
	std::cout << "" << endl;
	std::cout << "Remaining movement cash: " << movementCash << endl;
}

void playerUI::printPlayerMap(levelmap * initialisedmap, int w, int h)
{
	//NOT UE FRIENDLY
	int** map = dynInitaliser::makeInt(w, h, -9); //intialise the map
	cout << "PLAYER " << index << " MAP" << endl;
	//if walkable I want to see it
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			if (initialisedmap->isWalkable(x, y))//is walkable
			{
				map[x][y] = -1;
			}
		}
	}
	for (int i = 0; i < NoPieces; i++)
	{
		coord pos = playerPieces[i].getPos();
		map[pos.x][pos.y] = playerPieces[i].getIndex();
	}
	ui::print_matrix2digit(map, w, h);
	//cleanup
	dynInitaliser::del(map, w);
}
