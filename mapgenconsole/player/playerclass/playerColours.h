#pragma once

#include "player.h"

using namespace std;

class playerColours : public player 
{
public:
	playerColours(levelmap* initmap, int colr, int sty) : player(initmap)
	{
		//extra things here
		colour = colr;
		style = sty;
	}
	playerColours(levelmap* initmap, int colr, int sty, int ind, string n) : player(initmap, ind, n)
	{
		//extra things here
		colour = colr;
		style = sty;
	}
	//load file constructor
	playerColours(int colr, int sty, int ind, string n, inventory inv, bool ai, iStatValue movementCash) : player(ind, n, inv, ai, movementCash)
	{
		//extra things here
		colour = colr;
		style = sty;
	}
	int colour;
	int style;
};

