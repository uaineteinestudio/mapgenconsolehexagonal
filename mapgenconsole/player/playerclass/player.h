#pragma once

#include "playerData.h"
#include "../../arrays/dynInitaliser.h"
#include <vector>

class player : public playerData
{
	friend class playerIO;
	friend class playerUI;
	friend class playerHandler;
	friend class playerHandlerIO;
public:
	player(levelmap* initmap) : playerData(initmap,-1, "NameUndefined")
	{
		//ADD EXTRA HERE
		initaliseArrs();
	}
	player(levelmap* initmap, int ind, string n) : playerData(initmap, ind, n)
	{
		//ADD HERE
		initaliseArrs();
	}
	//load file constructor
	player(int ind, string n, inventory inv, bool ai, iStatValue moveCas) : playerData(ind, n, inv)
	{
		movementCash = moveCas;
		AIControlled = ai;
		initaliseArrs();
	}
	~player();

	//FIELDS
	static const int defMovement = 20;			//default movement cash, max movement of all pieces
	iStatValue movementCash = defMovement;

	//MEMBERS
	void cleanup(); //purge any data
	//MOVEMENT
	int getRemainingCash();				//return current movementcash
	void deductMovementCash(int movementamnt);
	bool canMoveThisFar(int movementamnt);
	int BoostMovementCash(int amount);	//boost the movement cash and return the new amount
	//Turn end
	void EndTurn();
	//initialising
	void initialisepieces(int lenx, int leny);
	void randomisePositions(bool walkablespots, int corner);
	//POSITIONS
	bool isInPosition(coord pos);//returns if a piece is in this position
	vector<coord> getPositions();
	//SELECTION
	void selectpiece(int piInd);
	void deselectpiece();
	bool hasSelectedPiece(int& index);
	bool isPlayerControlled() { return !AIControlled; }	//return opposite
	bool isAI() { return AIControlled; }				
protected:
	void initaliseArrs();
	void delArrs();
	bool movePiece(int i, coord newCoord);
	//selection
	int selectedPiece = 0;
	bool ispieceSelected = false;
	bool AIControlled = false;
private:
	bool initalised = false;
	int NormMovementcash = defMovement;
};
