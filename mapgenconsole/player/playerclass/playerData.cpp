#include "playerData.h"

playerData::~playerData()
{
}

void playerData::destroy()
{
	InventoryStash.destroy();
	playerPieces.clear();
	NoPieces = 0;
}

void playerData::addPiece(piece addthis)
{
	NoPieces += 1;
	playerPieces.push_back(addthis);
	setVisibility(getPieceByIndex(NoPieces - 1), mappointer->getlenx(), mappointer->getleny());
}

int playerData::newpieceindex()
{
	return NoPieces;
}

int playerData::getNoPieces()
{
	return NoPieces;
}

bool playerData::doesPieceExist(int index)
{
	if (index <= NoPieces)
		return true;
	else
		return false;
}

void playerData::setNewMapPointer(levelmap * newpointer)
{
	mappointer = newpointer;
}

piece* playerData::getPieceByIndex(int ind)
{
	if (doesPieceExist(ind))
	{
		return &playerPieces[ind];
	}
	else
	{
		piece* p = new squire(coord(0, 0), 0, 0, MaxLen, MaxLen);//null piece right there as failsafe
		return p;
	}
}

int playerData::getIndex()
{
	return index;
}

void playerData::discover(int x, int y)
{
	//set as true, faster than just checking if false and then setting anyway
	sightMap.discover(x, y);
}

void playerData::discover(coord point)
{
	discover(point.x, point.y);
}

void playerData::setVisibility(piece* p, int lenx, int leny)
{
	vector<coord> view = p->getViewCircle(lenx, leny, mappointer->gethexorsquare());
	for (int i = 0; i < view.size(); i++)
	{
		sightMap.setVisible(view[i]);
		discover(view[i]);
	}
	view.clear(); //cleanup please
}

void playerData::setVisibilityAllPieces(int lenx, int leny)
{
	for (int i = 0; i < NoPieces; i++)
	{
		setVisibility(getPieceByIndex(i), lenx, leny);
	}
}