//TO BE A CLASS FOR HANDLING PAWNS NOT PLAYER CONTROLLED

#pragma once

#include "playerIO.h"

class playerNPC : public playerIO
{
	playerNPC(levelmap* initmap, int colr, int sty, int ind, string n) : playerIO(initmap, colr, sty, ind, n)
	{
		AIControlled = true;
	}
	playerNPC(levelmap* initmap, int colr, int sty) : playerIO(initmap, colr, sty)
	{
		AIControlled = true;
	}
};

