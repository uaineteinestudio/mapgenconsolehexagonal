#pragma once

#include "playerIO.h"

class playerUI : public playerIO
{
public:
	playerUI(levelmap* initmap) : playerIO(initmap, 0, 0)
	{
		//add anything here
	}
	playerUI(levelmap* initmap, int sty, int pcol, int ind, string n) : playerIO(initmap, pcol, sty, ind, n)
	{
		//add anything here
	}
	playerUI(int sty, int pcol, player p) : playerIO(p.mappointer, pcol, sty, p.index, p.name)
	{
		playerPieces = p.playerPieces;
		NoPieces = p.NoPieces;
		movementCash = p.movementCash;
		selectedPiece = p.selectedPiece;
		initalised = p.initalised;
		NormMovementcash = p.NormMovementcash;
	}
	playerUI(playerColours p) : playerIO(p.mappointer, p.colour, p.style, p.index, p.name)
	{
		playerPieces = p.playerPieces;
		NoPieces = p.NoPieces;
		movementCash = p.movementCash;
		selectedPiece = p.selectedPiece;
		initalised = p.initalised;
		NormMovementcash = p.NormMovementcash;
	}
	~playerUI();

	//MEMBERS
	void printPlayerInfo(levelmap* initialisedmap, int w, int h);
	void printPlayerMap(levelmap* initialisedmap, int w, int h);
};

