#include "player.h"


player::~player()
{
}

void player::cleanup()
{
	delArrs();			//remove arrays
	deselectpiece();	//deselect

}

void player::initaliseArrs()
{
	initalised = true;
}

int player::getRemainingCash()
{
	return movementCash;
}

bool player::movePiece(int i, coord newCoord)
{
	//need to know movement length with piece
	//so get that movement length
	int len = mappointer->getMoveDist(playerPieces[i].getPos(), newCoord);
	if (canMoveThisFar(len))
	{
		deductMovementCash(len);
		//remove view
		mappointer->sightMap->resetVisible();
		//move piece
		playerPieces[i].movePiece(newCoord, mappointer->getlenx(), mappointer->getleny(), mappointer->gethexorsquare());
		//then discover/set visible
		setVisibilityAllPieces(mappointer->getlenx(), mappointer->getleny());
		return true;
	}
	else
		return false; //didn't work
}

void player::deductMovementCash(int movementamnt)
{
	movementCash.Value -= movementamnt;
}

bool player::canMoveThisFar(int movementamnt)
{
	if (movementCash.Value < movementamnt)
		return false;
	else
		return true;
}

int player::BoostMovementCash(int amount)
{
	movementCash.Value += amount;
	return movementCash;
}

void player::EndTurn()
{
	movementCash.reset();
}

void player::initialisepieces(int lenx, int leny)
{
	playerPieces = defpieces::getDefaultPieces(index, lenx, leny);
	NoPieces = numTypes;
	setVisibilityAllPieces(lenx, leny);
	//find them spawn positions would be good
}

void player::randomisePositions(bool walkablespots, int corner)
{
	const int attemptLim = 100;
	vector<coord> curPos; //list of all current positions
	for (int i = 0; i < NoPieces; i++)
	{
		int k = 0; //index for while loop
		while (k < attemptLim)
		{
			coord potentialspot = mappointer->getRandomCornerBlock(corner, walkablespots);
			if (i == 0) //lucky first up no checks to be done
			{
				curPos.push_back(potentialspot);
			}
			else
			{
				//need to make sure there is not one there already
				bool conflict = false;
				for (int j = 0; j < i; j++)
				{
					//loop through and check
					if (curPos[j] == potentialspot)
					{
						conflict = true;
					}
				}
				if (conflict == false) //no conflict we can use
				{
					curPos.push_back(potentialspot);
				}
				//else do nothing and try again
			}
			k += 1;//increment
		}
	}
	//set from the list
	for (int i = 0; i < NoPieces; i++)
	{
		playerPieces[i].PosOverride(curPos[i]); //set new spot
	}
	//el fino
}

void player::delArrs()
{
	playerPieces = vector<piece>();
	NoPieces = 0;
	initalised = false;
}

bool player::isInPosition(coord pos)
{
	for (int i = 0; i < NoPieces; i++)
	{
		if (playerPieces[i].inPos(pos))
			return true;
	}
	return false;
}

vector<coord> player::getPositions()
{
	vector<coord> posi = vector<coord>();
	for (int i = 0; i < NoPieces; i++)
	{
		posi.push_back(playerPieces[i].getPos());
	}
	return posi;
}

void player::selectpiece(int piInd)
{
	selectedPiece = piInd;
	ispieceSelected = true;
}

void player::deselectpiece()
{
	//then deselect
	ispieceSelected = false;
	selectedPiece = 0;
}

bool player::hasSelectedPiece(int & index)
{
	index = selectedPiece;
	return ispieceSelected;
}
