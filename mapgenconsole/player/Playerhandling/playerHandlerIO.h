#pragma once

#include "playerHandler.h"
#include "../../io/playerparser.h"
#include "../../io/pieceparser.h"
#include <sstream>      // std::stringstream

class playerHandlerIO : public playerHandler
{
public:
	playerHandlerIO(levelmap* initmap) : playerHandler(initmap)
	{

	}
	~playerHandlerIO();
	void SaveAll();
	void LoadAll();
	int getVersionNumber() { return VersionNumber; }
protected:
	int VersionNumber = 2;
private:
	string getFilenameForPieces();
	string getFilenameForplayers();
};

