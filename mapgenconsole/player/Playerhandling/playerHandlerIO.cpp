#include "playerHandlerIO.h"


playerHandlerIO::~playerHandlerIO()
{

}

void playerHandlerIO::SaveAll()
{
	//pieces go first, then players
	//get filename of pieces by levelmap
	string piecesFileName = getFilenameForPieces();
	string playerFileName = getFilenameForplayers();

	pieceparser::writePieces(piecesFileName, VersionNumber, &allpieces, allpieces.size());//write
	playerparser::writePlayers(playerFileName, &players, NoPlayers, TurnNumber, curPlayerTurn);
}

void playerHandlerIO::LoadAll()
{
	//todo
	string piecesFileName = getFilenameForPieces();
	string playerFileName = getFilenameForplayers();

	int npieces = 0;
	vector<piece> piecevec = pieceparser::loadPieces(piecesFileName, VersionNumber, npieces, initialisedmap->getlenx(), initialisedmap->getleny());
	players = playerparser::loadPlayers(playerFileName, NoPlayers, TurnNumber, curPlayerTurn);

	//pointer update
	for (int i = 0; i < NoPlayers; i++)
	{
		players[i].mappointer = initialisedmap;
	}

	//now to add pieces
	for (int i = 0; i < npieces; i++)
	{
		int o = piecevec[i].getOwner();	//owner
		players[o].addPiece(piecevec[i]);
	}
	for (int i = 0; i < NoPlayers; i++)
	{
		for (int k = 0; k < players[i].NoPieces; k++)
		{
			piece* piecepointer = &players[i].playerPieces[k];
			allpieces.push_back(piecepointer);
		}
	}
	//now cleanup
	piecevec.clear();
	//now to set vis based on player
	updateSightMapByCurrentPlayer();
}

string playerHandlerIO::getFilenameForPieces()
{
	stringstream ss;
	ss << initialisedmap->filename.getName() << "_pieces.txt";
	return ss.str();
}

string playerHandlerIO::getFilenameForplayers()
{
	stringstream ss;
	string prefix = initialisedmap->filename.getName() + "_Players";
	ss << prefix << ".txt";
	return ss.str();
}
