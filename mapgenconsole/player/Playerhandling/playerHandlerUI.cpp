#include "playerHandlerUI.h"

playerHandlerUI::~playerHandlerUI()
{
}

void playerHandlerUI::printPlayerInfo(levelmap* initalisedmap, int index, int w, int h)
{
	printPlayerInfo(initalisedmap, &players[index], w, h);
}

void playerHandlerUI::printPlayerInfo(levelmap* initalisedmap, player * p, int w, int h)
{
	playerUI p1 = playerUI(0,0,*p);//0 out the colours and such ye ken
	p1.printPlayerInfo(initalisedmap, w, h);
}

void playerHandlerUI::printPlayerInfo(levelmap * initalisedmap, playerColours * p, int w, int h)
{
	playerUI p1 = playerUI(*p);//0 out the colours and such ye ken
	p1.printPlayerInfo(initalisedmap, w, h);
}

void playerHandlerUI::printPlayerInfo(levelmap* initalisedmap, playerUI * p, int w, int h)
{
	p->printPlayerInfo(initalisedmap, w, h);
}
