#pragma once

#include "../playerclass/playerIO.h"
//#include "../../levelmap.h"
//#include <vector>

class playerHandler
{
public:
	playerHandler(levelmap* initmap);
	~playerHandler();
	void initialise(levelmap* initmap);
	void destroy();		//deconstruct map to make way for another

	//add a player
	void addPlayer(playerIO newP);
	int addPieceFromType(int playerindex, coord spawnpos, int type, int lenx, int leny); //return new index
	int addPieceFromType(int playerindex, int type, int lenx, int leny); //return new index-same as above but 0,0 pos
	//remove players
	void delPlayers();
	//Does player exist
	bool doesPlayerExist(int ind); //valid index?
	//return a player
	playerIO* returnPlayer(int ind); //return by index
	piece* getPieceByUID(string uid);
	int newIndex(); //top level index
	//delete em
	void cleanup();
	int getNumPlayers() { return NoPlayers; }

	//SPAWN PLAYERS
	void spawnPlayers(bool walkcheck);	//spawn each players pieces in random positions with walkability checking on or off

	//TURNS
	int getTurnNo();
	int getCurPlayerTurn();
	playerIO* getCurPlayer();				//return the current player as a pointer
	//end turn here
	void endCurrentPlayerTurn();
	void endTurn();

	//move a player
	bool movePiece(player* p, int piecei, coord newC);	//return if can be made with movement cash
	bool movePiece(int i, int piecei, coord newC);		//return if can be made with movement cash

	//CHECK IF PLACE IS OCCUPIED BY A PLAYER PIECE
	bool pieceInPos(coord pos);
protected:
	//levelmap reference
	levelmap* initialisedmap;
	//set new level sightmap
	void updateSightMapByCurrentPlayer();
	//PLAYER LIST
	int NoPlayers = 0;
	vector<playerIO> players;
	//piece list
	vector<piece*> allpieces;

	//Turn counter
	int TurnNumber = 0;
	int curPlayerTurn = 0;
	//start
	bool firstTurn();					//is it the first turn
	//end turn
	void endPlayerTurn(int index);
	void endPlayerTurn(player* p);		//same thing as above but using a pointer
	void setPlTurnIndex(int newInd);	//set new index of the current player
	bool AllPlayersFinished();			//all players finished their current turn
	//reset counters
	void resetAllTurns();				//bring it all back to 0
	//filter walkability
	vector<coord> getPieceMovementCircle(int plyrind, int ind, vector<moveability> &move, int &size, int lenx, int leny); //note this calculates it
	vector<coord> getPieceMovementCircleWalkability(int plyrind, int ind, vector<moveability> &move, int &size, int lenx, int leny); //walkability included- note this calculates it
	void updatePieceMovement(player* plyr, piece* p);
	void updateAllPiecesMovement();
	void filterWalkable(vector<coord> &circl, vector<moveability>& move, int size); //update walkability
private:
	void playerConfictErrorCheck(bool walkablespots); //ran to check the spawning has not duplicated players
};

