#include "playerHandler.h"

using namespace std;

playerHandler::playerHandler(levelmap* initmap)
{
	initialise(initmap);
}

playerHandler::~playerHandler()
{
}

void playerHandler::initialise(levelmap* initmap)
{
	players = vector<playerIO>();
	NoPlayers = 0;
	initialisedmap = initmap;
	allpieces = vector<piece*>();
}

void playerHandler::destroy()
{
	//Turn counter
	int TurnNumber = 0;
	int curPlayerTurn = 0;
	for (int i = 0; i < NoPlayers; i++)
	{
		players[i].destroy();
	}
	int NoPlayers = 0;
	//clear
	players.clear();
	allpieces.clear();
}

void playerHandler::addPlayer(playerIO newP)
{
	players.push_back(newP);
	//care about pices
	for (int i =0; i < newP.NoPieces; i++)
	{
		piece* piecepointer = &newP.playerPieces[i];
		allpieces.push_back(piecepointer);
	}
	NoPlayers += 1;
}

int playerHandler::addPieceFromType(int playerindex, coord spawnpos, int type, int lenx, int leny)
{
	if (doesPlayerExist(playerindex))
	{
		int newi = players[playerindex].newpieceindex();
		piece newp = defpieces::getFromType(type, spawnpos, playerindex, newi, lenx, leny);
		players[playerindex].addPiece(newp);
		allpieces.push_back(players[playerindex].getPieceByIndex(newi));
		return newi; //return new index
	}
	else
	{
		//failed
		return -1;
	}
}

int playerHandler::addPieceFromType(int playerindex, int type, int lenx, int leny)
{
	return addPieceFromType(playerindex, coord(0, 0), type, lenx, leny); //using 0,0
}

void playerHandler::delPlayers()
{
	//JUST NEED TO CALL INITIALISE TO RESET COUNT AND LIST
	initialise(initialisedmap);
}

bool playerHandler::doesPlayerExist(int ind)
{
	if (ind < NoPlayers)
	{
		return true;
	}
	else
	{
		return false;
	}
}

playerIO * playerHandler::returnPlayer(int ind)
{
	if (doesPlayerExist(ind))
	{
		//if the player exists, then return it.
		return &players[ind];
	}
	else
	{
		//otherwise make a new null player
		playerIO* newP = new playerIO(initialisedmap, 0,0,0, "null player");
		return newP;
	}
}

piece * playerHandler::getPieceByUID(string uid)
{
	/* not needed for now
	int playerIndex = pieceuid::getPlayerIndex(uid);
	int pieceIndex = pieceuid::getPieceIndex(uid);
	*/

	for (int i = 0; i < allpieces.size(); i++)
	{
		if (pieceuid::matches(allpieces[i]->uid, uid))
		{
			//return
			return allpieces[i];
		}
	}
	//nothing found failsafe
	piece* newp = new piece(coord(0, 0), -1, 0, -1, "failed", MaxLen, MaxLen);
	return newp;
}

int playerHandler::newIndex()
{
	return NoPlayers;
}

void playerHandler::cleanup()
{
	for (int i = 0; i < NoPlayers; i++)
	{
		players[i].cleanup();
	}
	NoPlayers = 0;
	initialise(initialisedmap);
}

void playerHandler::spawnPlayers(bool walkcheck)
{
	//remove discovery
	initialisedmap->sightMap->resetVisDis();
	for (int i = 0; i < NoPlayers; i++)
	{
		players[i].initialisepieces(initialisedmap->getlenx(), initialisedmap->getleny());
		players[i].randomisePositions(walkcheck, i);
		//add
		for (int k = 0; k < players[i].NoPieces; k++)
		{
			allpieces.push_back(&players[i].playerPieces[k]);
		}
	}
	playerConfictErrorCheck(walkcheck);
	//then get moveability
	updateAllPiecesMovement();
	updateSightMapByCurrentPlayer();
}

int playerHandler::getTurnNo()
{
	return TurnNumber;
}

int playerHandler::getCurPlayerTurn()
{
	return curPlayerTurn;
}

playerIO * playerHandler::getCurPlayer()
{
	//failsafe return null player contained in this called method
	return returnPlayer(curPlayerTurn);
}

void playerHandler::endCurrentPlayerTurn()
{
	endPlayerTurn(curPlayerTurn);
	//update sightmap
	updateSightMapByCurrentPlayer();
	//update the map
	initialisedmap->update();
	//then if we have ended all, go to next turn itself
	if (AllPlayersFinished())
	{
		//since all have finished time to reset the whole turn itself
		endTurn();
	}
}

void playerHandler::endTurn()
{
	//reinitialisedturn check
	//call this: void playerHandler::updatePieceMovement(player* plyr, piece * p)
	for (int i = 0; i < NoPlayers; i++)
	{
		player* plyr = returnPlayer(i);
		for (int j = 0; j < players[i].NoPieces; j++)
		{
			piece* p = plyr->getPieceByIndex(j);
			if (p->move.isInit() == true)
			{
				//is initalised
				updatePieceMovement(plyr, p);
			}
		}
	}
	curPlayerTurn = 0;
	TurnNumber += 1;
}

bool playerHandler::movePiece(player * p, int piecei, coord newC)
{
	//reinitalises walkability map for this one, reinitialise for all pieces would be great though
	bool success = p->movePiece(piecei, newC);

	//now to set the moveability circles for all
	updateAllPiecesMovement();
	
	return success;
}

bool playerHandler::movePiece(int pi, int piecei, coord newC)
{
	return movePiece(&players[pi], piecei, newC);
}

vector<coord> playerHandler::getPieceMovementCircle(int plyrind, int ind, vector<moveability>& move, int & size, int lenx, int leny)
{
	piece* p = players[plyrind].getPieceByIndex(ind);
	return p->getMovementCircle(move, size, lenx, leny, p->pieceStats.getMoveDist(), initialisedmap->gethexorsquare());
}

vector<coord> playerHandler::getPieceMovementCircleWalkability(int plyrind, int ind, vector<moveability>& move, int & size, int lenx, int leny)
{
	vector<coord> circl = getPieceMovementCircle(plyrind, ind, move, size, lenx, leny);
	//filter out tiles for starters
	//removal code
	/*
	int removed = 0;
	for (int i = 0; i < size; i++)
	{
		int index = i - removed;
		if (initialisedmap->isWalkable(circl[index].x, circl[index].y) == false)
		{
			//is not walkable remove it
			circl.erase(circl.begin() + index);
			move.erase(move.begin() + index);
			removed += 1;
		}
	}
	size -= removed;
	*/
	//set to false walk instead
	
	filterWalkable(circl, move, size);

	return circl;
}

void playerHandler::updatePieceMovement(player* plyr, piece * p)
{
	int size = 0;

	//to get pointers
	//vector<coord>* circ = pi->move.getCirc();
	//vector<moveability>* moveinfo = pi->move.getMoveAbility();

	//calculating from scratch
	vector<moveability> moveinfo;
	vector<coord> circ = getPieceMovementCircleWalkability(plyr->getIndex(), p->getIndex(), moveinfo, size, initialisedmap->getlenx(), initialisedmap->getleny());
	p->move.store(circ, moveinfo, size);
}

void playerHandler::updateAllPiecesMovement()
{
	for (int i = 0; i < NoPlayers; i++)
	{
		player* plyr = returnPlayer(i);
		for (int j = 0; j < players[i].NoPieces; j++)
		{
			piece* pi = plyr->getPieceByIndex(j);
			updatePieceMovement(plyr, pi);
		}
		//update view too
		plyr->setVisibilityAllPieces(initialisedmap->getlenx(), initialisedmap->getleny());
	}
}

bool playerHandler::pieceInPos(coord pos)
{
	for (int i = 0; i < NoPlayers; i++)
	{
		if (players[i].isInPosition(pos))
			return true;
	}
	//else
	return false;
}

void playerHandler::updateSightMapByCurrentPlayer()
{
	playerIO* newPlayerWhoseTurnItIs = getCurPlayer();
	initialisedmap->newSightMap(&newPlayerWhoseTurnItIs->sightMap);
}

bool playerHandler::firstTurn()
{
	if (TurnNumber == 0)
		return true;
	else
		return false;
}

void playerHandler::endPlayerTurn(int index)
{
	endPlayerTurn(&players[index]);
}

void playerHandler::endPlayerTurn(player * p)
{
	p->EndTurn();
	setPlTurnIndex(p->getIndex() + 1);
}

void playerHandler::setPlTurnIndex(int newInd)
{
	curPlayerTurn = newInd;
}

bool playerHandler::AllPlayersFinished()
{
	if (curPlayerTurn == NoPlayers) //then we are finished
		return true;
	else 
		return false;				//we are not
}

void playerHandler::resetAllTurns()
{
	TurnNumber = 0;
	curPlayerTurn = 0;
}

void playerHandler::filterWalkable(vector<coord> &circl, vector<moveability>& move, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (initialisedmap->isWalkable(circl[i].x, circl[i].y) == false)
		{
			move[i].walkable = false;
		}
	}

	//piece updates
	for (int i = 0; i < NoPlayers; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (players[i].isInPosition(circl[j]))
			{
				//if in this position show me
				move[j].piece = true;
			}
		}
	}
}

void playerHandler::playerConfictErrorCheck(bool walkablespots)
{
	int counter = 0;
	int lim = 25;

	while (counter < lim)
	{
		vector<coord> piecePos = vector<coord>();
		vector<coord> PlayerNPieceID = vector<coord>();
		int i = 0;
		for (int l = 0; l < NoPlayers; l++)
		{
			vector<coord> piecePos2 = players[l].getPositions();
			for (int k = 0; k < players[l].getNoPieces(); k++)
			{
				piecePos.push_back(piecePos2[k]);
				PlayerNPieceID.push_back(coord(l, k));
				i += 1;
			}
		}
		//now to check all positions for matches
		vector<int> conflicts = vector<int>();
		int nconf = 0;
		for (int j = 0; j < i; j++)
		{
			for (int k = j + 1; k < i; k++)
			{
				//if conflict is in here
				if (piecePos[j] == piecePos[k])
				{
					nconf += 1;
					conflicts.push_back(j);
				}
			}
		}
		if (nconf > 0)
		{
			//now that I have the list of conflicts respawn them
			for (int k = 0; k < nconf; k++)
			{
				coord pid = PlayerNPieceID[conflicts[k]];
				int plyr = pid.x;
				int piec = pid.y;
				coord potentialspot = initialisedmap->getRandomCornerBlock(plyr, walkablespots);
				piece* p = players[plyr].getPieceByIndex(piec);
				p->PosOverride(potentialspot);
			}
			//now try again if in conflict
			counter += 1;
		}
		else
		{
			break;
		}
	}
}
