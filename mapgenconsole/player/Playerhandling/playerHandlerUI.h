#pragma once

#include "playerHandlerIO.h"
#include "../playerclass/playerUI.h"
//NOT UE FRIENDLY
#include "../../ui/ui.h"

class playerHandlerUI : public playerHandlerIO
{
public:
	playerHandlerUI(levelmap* initmap) : playerHandlerIO(initmap)
	{
		//ADD HERE
	}
	~playerHandlerUI();

	void printPlayerInfo(levelmap* initalisedmap, int index, int w, int h);
	void printPlayerInfo(levelmap* initalisedmap, player* p, int w, int h);
	void printPlayerInfo(levelmap* initalisedmap, playerColours* p, int w, int h);
	void printPlayerInfo(levelmap* initalisedmap, playerUI* p, int w, int h);
};

