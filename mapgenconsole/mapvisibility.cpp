#include "mapvisibility.h"

mapvisibility::~mapvisibility()
{
}

void mapvisibility::initialisearrs()
{
	//call parent
	mapdata::initialisearrs();

	//--ADDITIONAL--
	sightMap->resetVisDis();
}

void mapvisibility::deletearrs()
{
	//call parent
	mapdata::deletearrs();

	//--ADDITIONAL--
	sightMap->destroy();
}