#pragma once

#include "io/mapparser.h"
#include "mapgenerate.h"
#include "text/Named.h"

class levelmapIO : public mapgenerate
{
public:
	levelmapIO() : mapgenerate(MaxLen, MaxLen, false)
	{
		filename = Named("testout.txt");
	};
	levelmapIO(string fn) : mapgenerate(MaxLen, MaxLen, false)
	{
		filename = Named(fn);
	};
	levelmapIO(string fn, int lx, int ly, bool squareorhex) : mapgenerate(lx, ly, squareorhex) //0 is square, 1 is hex
	{
		filename = Named(fn);
	};
	levelmapIO(string fn, int lx, int ly, bool squareorhex, bool setStaticHeightBorder) : mapgenerate(lx, ly, squareorhex, setStaticHeightBorder)
	{
		filename = Named(fn);
	};
	~levelmapIO();

	//save and load
	void savemap();
	void loadmap();
	Named filename;
protected:
	//levelmap version
	int version = 10;

	//busy bools
	bool loading = false;
};

