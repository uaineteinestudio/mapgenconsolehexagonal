#pragma once

#include "maplen.h"
#include "cellularautomata/cellautomachest.h"
#include "coord.h"
#include "artificialCaves.h"
#include "adjacency/hexadjacency.h"
#include "blockTypes.h"
#include <iostream>
#include "floodfiller/floodfillerHexsize.h"
//#include "ui.h"

class caveGenerator : public CAChest //inherit
{
public:
	caveGenerator(int lenx, int leny);//default
	caveGenerator(int lenx, int leny, float ch, int dl, int bl, int smoofac, int tL, int nC, int interior) :
		CAChest(ch, dl, bl, smoofac, tL, nC)
	{
		w = lenx;
		h = leny;
		//interior block type
		Interior = interior;
	}
	caveGenerator(int lenx, int leny, int interior, int tL, int nC) : CAChest(tL, nC)
	{
		w = lenx;
		h = leny;
		Interior = interior;
	}
	caveGenerator(int lenx, int leny, int interior) : CAChest()
	{
		w = lenx;
		h = leny;
		//interior block type
		Interior = interior;
	}
	~caveGenerator();
	bool** makeCaves();
	void purgeCaves(bool** caves, bool sqHex);
	void setCaves(bool** caves, int** bt);
	void setInteriorCells(bool** cavemap, int** blocktypes);
	void setInteriorCellsHex(bool** cavemap, int** blocktypes, int** blockheights);
	vector<coord> interiorCells(bool** caves, int &size);//return size too
	vector<coord> interiorCellsHex(bool** caves, int &size);//return size too
	void PlaceChests(bool** cavemap, int ** blocktypes, int** surfacetypes, vector<coord> &chestLocations, int &numChests);
	void caveCleanup(bool** caves, int** bt, int keepthistype, bool sqhex); //0 is square
private:
	int w;
	int h;
	bool isInterior(bool ** map, int xi, int yi);
	bool isInteriorHex(bool ** map, int xi, int yi);
	int Interior;
	//purge caves smaller than this:
	int purgeSmallerThan = 11;
	int purgeLargerThan = 44;
	bool HasCave(bool** cavemapAfterPurge); //has at least 1 cave on the map
	bool oppval(bool** caves, int x, int y);
	float calcPercComplete(int num, int max);
	void DebugPrintStats(float p);
	void makeArtificalCave(bool** cavesmap);
};

