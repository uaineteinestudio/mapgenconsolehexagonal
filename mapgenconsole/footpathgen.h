#pragma once

#include "genclass.h"
#include <vector>
#include "maplen.h"
#include "blockTypes.h"
#include "pathfinding/quickgrid.h"
#include "coord.h"
#include "arrays/dynInitaliser.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

class footpathgen : genclass
{
public:
	footpathgen(int width, int height) : genclass(width, height)
	{
		//nothing more to add
	}
	~footpathgen();
	void establishFootpaths(bool** footpaths, bool** walkable, int** blockTypes, int** rotmap, bool squareorhex); //0 for square, 1 for hex
	void smoothFootpaths(bool** footpaths, int** heights, int** bt, bool sqhex); //0 is square
private:
	void ConnectPoints(bool ** footpaths, bool ** walkable, int ** blockTypes, vector<coord> path);
	void finaliseRot(int** rotmap, bool** footpathmap);
	void getRandomNode(int corner, int &x, int &y, int limitFrom); //get from corner and limit away from that corner
	int getrandInt(int min, int range);
	static const int attemptFootpathLim = 35; //if this is breached go with a footpath that ignores blockades
	vector<coord> getFootpathFromMap(bool** footpathmap, int** bt, bool sqhex);
	void smoothPoint(int x, int y, int** heights, bool sqhex); //block heights passed in
	static const int footpathsmoothN = 3;
};