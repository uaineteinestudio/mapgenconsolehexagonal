#include "stonegenerator.h"

stonegenerator::~stonegenerator()
{
}

void stonegenerator::generateStoneGrain(int ** bt, int ** st)
{
	int x = 0;
	int y = 0;
	int k = 0;
	int i = 0;
	int maxAttempts = 100;	//max index
	while (k < maxAttempts)
	{
		x = rand() % lenx;
		y = rand() % leny;
		if (validStoneGrainSpot(x, y, bt, st))
		{
			//works
			float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			if (r < fracBigRock) //low chance to be a big rock
				st[x][y] = surfaceTypes::rocklarge; //is a large rock
			else
				st[x][y] = surfaceTypes::rockgrain; //is not
			i += 1;
		}
		if (i == numGrainGrass)
		{
			k = maxAttempts;
			break;//complete
		}
		k += 1; //attempt count
	}
}

void stonegenerator::makestoneTiles(int ** bt, int ** st)
{
	CA ca = CA(0.4, 4, 4, 3);
	bool ** stonemap = ca.cellautomata(lenx, leny);
	int col[HexAdjLen];
	int row[HexAdjLen];
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			if (stonemap[x][y])
			{
				if (blockTypes::notCave(bt[x][y]))
				{
					hexAdjaceny::WriteColRow(col, row, x, y);
					bool cavenei = false; //looking for cave neighbour
					for (int i = 0; i < HexAdjLen; i++)
					{
						int xi = col[i] + x;
						int yi = row[i] + y;
						if (boundchecker::inBounds(xi, yi, lenx, leny))
						{
							if (bt[xi][yi] == blockTypes::cave)
								cavenei = true; //has a cave neighbour
						}
					}
					if (cavenei) //if there was one
					{
						float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
						if (r < stonechance)
						{
							bt[x][y] = blockTypes::stone;
						}
						else
						{
							//heavy stone instead
							bt[x][y] = blockTypes::heavystonegrass;
						}
					}
				}
			}
		}
	}
	//cleanup
	dynInitaliser::del(stonemap, lenx);
}

void stonegenerator::smoothOutStone(int ** bt, int ** st)
{
	//2 types to handle here
	StoneGrassType(bt, st, blockTypes::stone);
	StoneGrassType(bt, st, blockTypes::heavystonegrass);
}

void stonegenerator::initalise()
{
	numGrainGrass = (int)(fracgraingrass * area());
}

bool stonegenerator::validStoneGrainSpot(int xi, int yi, int ** bt, int ** st)
{
	if (boundchecker::inBounds(xi, yi, lenx, leny))
	{
		//in bounds
		if (st[xi][yi] == surfaceTypes::air)
		{
			//is air
			if (blockTypes::notCave(bt[xi][yi]))
			{
				//not cave blocks
				if (blockTypes::notWater(bt[xi][yi]))
				{
					//not water
					return true;//conditions met
				}
			}
		}
	}
	return false;
}

void stonegenerator::StoneGrassType(int ** bt, int ** st, int type)
{
	int size = 0;
	bool** stonemap = BoundaryMaps::getMapOfType(bt, type, MaxLen, MaxLen);
	//need to get boundaries
	vector<coord> boundaries = BoundaryMaps::getBoundariesChance(stonemap, size, stonedirtchance);
	for (int i = 0; i < size; i++)
	{
		int xi = boundaries[i].x;
		int yi = boundaries[i].y;
		if (bt[xi][yi] == blockTypes::grass)
			bt[xi][yi] = blockTypes::stonegrass;
		if (bt[xi][yi] == blockTypes::sand)
			bt[xi][yi] = blockTypes::stonesand;
	}
	//cleanup please
	boundaries.clear();

	//cleanup
	dynInitaliser::del(stonemap, lenx);
}
