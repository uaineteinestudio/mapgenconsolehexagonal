#include "selClass.h"

selClass::selClass()
{
}

selClass::~selClass()
{
}

void selClass::updateSelectedBlock(int x, int y)
{
	oldpos = selectedpos;
	selectedpos.x = x;
	selectedpos.y = y;
	//cH.UpdateActiveChunks(selectedpos, oldpos);-now called from update method in levelmap as a tick like replacement
}

void selClass::getSelectedBlock(int & x, int & y)
{
	x = selectedpos.x;
	y = selectedpos.y;
}

void selClass::updatemousepos(int x, int y)
{
	mouseposOld = mousepos;
	mousepos.x = x;
	mousepos.y = y;
}

void selClass::getmousepos(int & x, int & y)
{
	x = mousepos.x;
	y = mousepos.y;
}

void selClass::getmouseOldpos(int & x, int & y)
{
	x = mouseposOld.x;
	y = mouseposOld.y;
}
