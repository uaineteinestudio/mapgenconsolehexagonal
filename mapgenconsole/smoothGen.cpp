#include "smoothGen.h"

smoothGen::smoothGen()
{
}

int smoothGen::getAvg(int ** map, int xi, int yi, int & no)
{
	int col[5] = { -1, 0, 0, 0, 1 };
	int row[5] = { 0, -1, 0, 1, 0 };

	no = 0;
	int sum = 0;
	//iterate over the coords for the neighbours
	for (int i = 0; i < 5; i++)
	{
		int x = xi + col[i];
		int y = yi + row[i];
		if (inMaxBounds(x, y))
		{
			no += 1;
			sum += map[x][y];//add value
		}
	}
	if (no > 0)
		return sum / no;//get sum over number as average;
	else return map[xi][yi];
}

bool smoothGen::inMaxBounds(int xi, int yi)
{
	return boundchecker::inBounds(xi, yi, MaxLen, MaxLen);
}

void smoothGen::smoothOnce(int ** map)
{
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			int no = 0;
			int avg = getAvg(map, x, y, no);
			//now to add random var
			int var = (rand() % 3) - 1;	//variation between -1 and 1;
			map[x][y] = avg + var;
		}
	}
}

void smoothGen::smoothFac(int ** map)
{
	for (int i = 0; i < smoothfac; i++)
	{
		smoothOnce(map);
	}
}
