#include "ui.h"

using namespace std;

void ui::print_matrix(bool ** mat, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			if (mat[i][j] == false)
			{
				cout << 0 << " ";
			}
			else
			{
				cout << 1 << " ";
			}
		}
		cout << endl;
	}
}

void ui::print_matrixSel(bool ** mat, int width, int height, int x, int y, int col)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			if (i == x && y == j)
			{
				setConsoleColour(col);
			}
			cout << mat[i][j] << " ";
			if (i == x && y == j)
			{
				defColour();
			}
		}
		cout << endl;
	}
}

void ui::print_matrix(float ** mat, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			cout << mat[i][j] << " ";
		}
		cout << endl;
	}
}

void ui::print_matrix(int ** mat, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			cout << mat[i][j] << " ";
		}
		cout << endl;
	}
}

void ui::print_matrix2digit(int ** mat, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			if (mat[i][j] < 10 && mat[i][j] > -1)
			{
				cout << "0" << mat[i][j] << " ";
			}
			else
			{
				cout << mat[i][j] << " ";
			}
		}
		cout << endl;
	}
}

void ui::print_matrixSel(int ** mat, int width, int height, int x, int y, int col)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			if (i == x && y == j)
			{
				setConsoleColour(col);
			}
			cout << mat[i][j] << " ";
			if (i == x && y == j)
			{
				defColour();
			}
		}
		cout << endl;
	}
}

void ui::print_matrixSelM(vector<coord> posi, vector<moveability> move, int size, int width, int height, int x, int y, int col)
{
	int** selmat = dynInitaliser::makeInt(width, height, 0);

	for (int k = 0; k < size; k++)
	{
		selmat[posi[k].x][posi[k].y] = move[k].distance;
	}

	ui::print_matrixSel(selmat, width, height, x, y, col);

	dynInitaliser::del(selmat, width);
}

void ui::hexprint_matrix(bool ** mat, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		if (i % 2 == 0)
		{
			//try and align the the new line
			cout << " ";
		}
		for (int j = 0; j < height; j++)
		{
			if (mat[i][j] == false)
			{
				cout << 0 << " ";
			}
			else
			{
				cout << 1 << " ";
			}
		}
		cout << endl;
	}
}

void ui::hexprint_matrix(int ** mat, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		if (i % 2 == 0)
		{
			setConsoleColour(Green);
			//try and align the the new line
			cout << " ";
		}
		else
		{
			defColour();
		}
		for (int j = 0; j < height; j++)
		{
			if (mat[i][j] < 10)
			{
				cout << "0" << mat[i][j] << " ";;
			}
			else
			{
				cout << mat[i][j] << " ";
			}
		}
		cout << endl;
	}
}

void ui::hexprintSingleDigit(int ** mat, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		if (i % 2 == 0)
		{
			setConsoleColour(Green);
			//try and align the the new line
			cout << " ";
		}
		else
		{
			defColour();
		}
		for (int j = 0; j < height; j++)
		{
			cout << mat[i][j] << " ";
		}
		cout << endl;
	}
}

void ui::hexprintSingleDigit(int ** mat, int width, int height, int col, int coldigit)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		if (i % 2 == 0)
		{
			setConsoleColour(Green);
			//try and align the the new line
			cout << " ";
		}
		else
		{
			defColour();
		}
		for (int j = 0; j < height; j++)
		{
			if (mat[i][j] == coldigit)
			{
				setConsoleColour(col);
			}
			else if(i % 2 == 0)
			{
				setConsoleColour(Green);
			}
			else
			{
				defColour();
			}
			cout << mat[i][j] << " ";
		}
		cout << endl;
	}
}

void ui::printCoord(coord c)
{
	cout << "(" << c.x << "," << c.y << ")";
}

void ui::findColours()
{
	for (int i = 0; i < 255; i++)
	{
		setConsoleColour(i);
		writeLine(i);
	}

	setConsoleColour(BlacknWhite);
}

void ui::setConsoleColour(int colri)
{
	HANDLE  hConsole;
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	FlushConsoleInputBuffer(hConsole);
	SetConsoleTextAttribute(hConsole, colri);
}

void ui::defColour()
{
	setConsoleColour(BlacknWhite);
}

void ui::writeLine(int out)
{
	cout << out << endl;
}
