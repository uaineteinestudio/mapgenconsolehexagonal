#pragma once

#include <iostream>
#include <windows.h>
#include "../coord.h"
#include "../arrays/dynInitaliser.h"
#include "../moveability.h"
#include <vector>

using namespace std;

static class ui
{
public:
	static void print_matrix(bool** mat, int width, int height);
	static void print_matrixSel(bool** mat, int width, int height, int x, int y, int col);
	static void print_matrix(float** mat, int width, int height);
	static void print_matrix(int** mat, int width, int height);
	static void print_matrix2digit(int** mat, int width, int height);
	static void print_matrixSel(int** mat, int width, int height, int x, int y, int col);
	static void print_matrixSelM(vector<coord> posi, vector<moveability> move, int size, int width, int height, int x, int y, int col); //with moveability
	static void hexprint_matrix(bool ** mat, int width, int height);
	static void hexprint_matrix(int ** mat, int width, int height);
	static void hexprintSingleDigit(int ** mat, int width, int height);
	static void hexprintSingleDigit(int ** mat, int width, int height, int col, int coldigit);
	static void printCoord(coord c);
	static void findColours();
	static void setConsoleColour(int colri);
	static void defColour();
	static const int BlacknWhite = 15;
	static const int Green = 10;
	static const int Blue = 11;
	static const int Orange = 14;
	static const int Red = 12;
	static const int Purple = 13;
private:
	static void writeLine(int out);
};

