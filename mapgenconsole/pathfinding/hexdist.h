#pragma once

#include "quickgrid.h"
#include "../coord.h"
#include "../maplen.h"

static class hexdist
{
public:
	static int calcdist(bool** walkable, coord a, coord b, int lenx, int leny);
};