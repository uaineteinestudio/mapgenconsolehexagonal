#pragma once

#ifndef __CANVAS_H__
#define __CANVAS_H__

#include "Node.h"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <utility>
#include <math.h>
#include "../maplen.h"
#include "../coord.h"

#endif

#define DEFAULT_INTERVAL 15000

using namespace std;

class Canvas {
private:
	std::vector<std::vector<Node*>> grid;
	 
	int rows;
	int cols;
	
	std::pair<int, int> start;
	std::pair<int, int> end;

	void readGrid(int m, int n, bool** arr, int sx, int sy, int ex, int ey);
	void assignNeighbors();

public:
	Canvas(int m, int n, bool** arr, int sx, int sy, int ex, int ey);
	~Canvas();

	Node* getStart();
	Node* getEnd();
	Node* get(int i, int j);
	Node* get(std::pair<int, int> coord);

	vector<coord> getPath(int sx, int sy, int ex, int ey, int &size);

	float getDist(Node* a, Node* b);

	void draw();
	void draw(int nanosec);
};