#include "CanvasHex.h"
#include <chrono>
#include <thread>

using namespace std;

CanvasHex::CanvasHex(int m, int n, bool** arr, int sx, int sy, int ex, int ey) 
{
	readGrid(m, n, arr, sx, sy, ex, ey);
	
	rows = grid.size();
	cols = (!grid.empty() ? grid[0].size() : 0);
	
	assignNeighbors();
}

CanvasHex::~CanvasHex()
{
	if (!grid.empty())
	{
		for (int i = 0; i < rows; i++)
		{
			if (!grid[i].empty()) 
			{
				for (int j = 0; j < cols; j++) 
				{
					delete grid[i][j];
				}
			}
		}
	}
}

void CanvasHex::readGrid(int m, int n, bool** arr, int sx, int sy, int ex, int ey) 
{
	//std::ifstream fin(filename);
	
	int i = 0;//row
	int j = 0;//column
	std::vector<Node*> line;
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
			char c = '.';//default
			if (i == sy && j == sx)
			{
				c = 'S';
				start = std::make_pair(i, j);
			}
			else if (i == ey && j == ex)
			{
				c = 'E';
				end = std::make_pair(i, j);
			}
			else if (arr[i][j] == false)
				c = 'X';
			Node* sq = new Node(i, j, c);
			line.push_back(sq);
		}
		grid.push_back(line);
		line.clear();
	}
}

void CanvasHex::assignNeighbors() 
{
	int col[HexAdjLen];
	int row[HexAdjLen];

	int xi = 0;
	int yi = 0; //fastest initaliser
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			Node* sq = grid[i][j];

			//get new x and y from adjacency class
			hexAdjaceny::WriteColRow(col, row, j, i);
			for (int k = 0; k < HexAdjLen; k++)
			{
				xi = col[k] + j;
				yi = row[k] + i;
				//then make a bounds check
				if (boundchecker::inBounds(xi, yi, cols, rows))
				{
					//then make sure not an obstacle and if not
					if (!(grid[yi][xi])->isObstacle())
					{
						//make to a pair
						//like so:  sq->addNeighbor(std::make_pair(somex, somey))
						sq->addNeighbor(std::make_pair(yi, xi));
					}
				}
			}
		}
	}
}


Node* CanvasHex::get(int i, int j) 
{
	if (i >= 0 && i < rows && j >= 0 && j < cols) {
		return grid[i][j];
	}
	else
	{
		return nullptr;
	}
}

Node* CanvasHex::get(std::pair<int, int> coord)
{
	return CanvasHex::get(coord.first, coord.second);
}

vector<coord> CanvasHex::getPath(int sx, int sy, int ex, int ey, int &size)
{
	size = 0;
	vector<coord> pat = vector<coord>();
	pat.push_back(coord(sx, sy));
	int yi = 0;
	for (auto& v : grid)
	{
		int xi = 0;
		for (Node* sq : v)
		{
			if (sq->getSymbol() == '*')
			{
				pat.push_back(coord(xi,yi));
				size += 1;
			}
			xi += 1;
		}
		yi += 1;
	}
	pat.push_back(coord(ex, ey));
	return pat;
}


Node* CanvasHex::getStart()
{
	return CanvasHex::get(start); 
}

Node* CanvasHex::getEnd()
{
	return CanvasHex::get(end);
}


float CanvasHex::getDist(Node* a, Node* b)
{
	float distX = (float) (a->getCol() - b->getCol());
	float distY = (float) (a->getRow() - b->getRow());
	return (float) (sqrt((distX*distX) + (distY*distY)));
}


void CanvasHex::draw()
{
	CanvasHex::draw(DEFAULT_INTERVAL);
}

void CanvasHex::draw(int nanosec)
{
	system("cls");
	for (auto& v : grid) 
	{
		for (Node* sq : v)
		{
			std::cout << sq->getSymbol();
		}
		std::cout << std::endl;
	}
	std::cout << std::flush;	
	std::this_thread::sleep_for(std::chrono::microseconds(nanosec));
}

