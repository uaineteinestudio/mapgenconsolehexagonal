#pragma once

#include "Node.h"

class CompareG 
{
public:
	bool operator() (const Node* a, const Node* b) const;
};