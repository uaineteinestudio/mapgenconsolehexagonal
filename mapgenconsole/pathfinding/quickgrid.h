#pragma once

#include "Node.h"
#include "CompareG.h"
#include "CompareF.h"
#include "Canvas.h"
#include "CanvasHex.h"
#include <vector>
#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>
#include <utility>
#include <limits>
#include <chrono>
#include <thread>
#include "../coord.h"

using namespace std;

static class quickgrid
{
public:
	static bool main(int m, int n, bool** arr, int sx, int sy, int ex, int ey);
	static vector<coord> mainPath(int m, int n, bool** arr, int sx, int sy, int ex, int ey, int&size);
	static bool mainHex(int m, int n, bool** arr, int sx, int sy, int ex, int ey);
	static vector<coord> mainPathHex(int m, int n, bool** arr, int sx, int sy, int ex, int ey, int&size);
};