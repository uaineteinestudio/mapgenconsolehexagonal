#include "hexdist.h"

int hexdist::calcdist(bool ** walkable, coord a, coord b, int lenx, int leny)
{
	int size = 0;
	//would be nice to work with the active chunks only
	quickgrid::mainPathHex(lenx, leny, walkable, a.x, a.y, b.x, b.y, size);//calced size
	return size + 1;//plus the start point
}
