#include "fileparser.h"

void fileparser::write2DMap(ofstream * myfile, int w, int h, int ** map)
{
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			if (under10(map[x][y]))
				*myfile << "00" << map[x][y];
			else if (under100(map[x][y]))
				*myfile << "0" << map[x][y];
			else
				*myfile << map[x][y];
		}
	}
	*myfile << "\n";
}

void fileparser::write2DMap(ofstream * myfile, int w, int h, bool ** map)
{
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			if (map[x][y])
				*myfile << 1;
			else
				*myfile << 0;
		}
	}
	*myfile << "\n";
}

void fileparser::write2DMap(ofstream * myfile, int w, int h, float ** map)
{
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			*myfile << map[x][y];
			*myfile << "\n";
		}
	}
	*myfile << "\n";
}

void fileparser::writeBool(ofstream * myfile, bool toWrite)
{
	if (toWrite)
	{
		//write 1
		*myfile << 1;
	}
	else
	{
		//write 0
		*myfile << 0;
	}
	*myfile << "\n";
}

string fileparser::readLine(ifstream * myfile)
{
	string line;
	if (getline(*myfile, line))
		return line;
	else
		return "";
}

bool fileparser::under10(int val)
{
	if (val < 10)
		return true;
	else
		return false;
}

bool fileparser::under100(int val)
{
	if (val < 100)
		return true;
	else
		return false;
}

int fileparser::parseInt(string val)
{
	return stoi(val);
}

float fileparser::parseFloat(string val)
{
	return stof(val);
}

bool fileparser::parseBool(string val)
{
	if (val == "0")
		return false;
	return true;
}

void fileparser::parseIntMap(string line, int ** map, int w, int h)
{
	int i = 0;
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			string sub = line.substr(i, 3);
			map[x][y] = parseInt(sub);
			i += 3;
		}
	}
}

void fileparser::parseBoolMap(string line, bool ** map, int w, int h)
{
	int i = 0;
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			string sub = line.substr(i, 1);
			map[x][y] = parseBool(sub);
			i += 1;
		}
	}
}

coord fileparser::parseCoord(ifstream * myfile)
{
	int x = parseInt(readLine(myfile));
	int y = parseInt(readLine(myfile));
	return coord(x, y);
}

