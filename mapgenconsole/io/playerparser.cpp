#include "playerparser.h"

void playerparser::writePlayers(string fn, vector<playerIO>* playerlistpointer, int noplayers, int turnNum, int playerturn)
{
	ofstream myfile;
	myfile.open(fn);
	myfile << noplayers << "\n";
	myfile << turnNum << "\n";
	myfile << playerturn << "\n";
	vector<playerIO> sameADDRESSvec = *playerlistpointer;
	for (int i = 0; i < noplayers; i++)
	{
		writePlayer(&myfile, &sameADDRESSvec[i]);
	}
	myfile.close();
}

void playerparser::writePlayer(ofstream * myfile, playerIO * playerpointer)
{
	//PLAYERDATA.h
	*myfile << playerpointer->getName() << "\n";			//player name
	*myfile << playerpointer->getIndex() << "\n";			//index
	*myfile << playerpointer->getNoPieces() << "\n";		//numpieces
	writeInventory(myfile, &playerpointer->InventoryStash);	//inventory
	//PLAYER.h
	writeBool(myfile, !playerpointer->isAI());				//aicontrolled
	writeiStat(myfile, playerpointer->movementCash);		//movementcash-istat
	//PLAYERCOLOURS.h
	*myfile << playerpointer->colour << "\n";				//colour
	*myfile << playerpointer->style << "\n";				//style
	//visibility + discovery
	int w = playerpointer->sightMap.getlenx();
	int h = playerpointer->sightMap.getleny();
	*myfile << w << "\n";
	*myfile << h << "\n";
	write2DMap(myfile, w, h, playerpointer->sightMap.visible);
	write2DMap(myfile, w, h, playerpointer->sightMap.discovered);
}

playerIO playerparser::loadPlayer(ifstream * myfile)
{
	string n = readLine(myfile);
	int index = parseInt(readLine(myfile));
	int noPices = parseInt(readLine(myfile));
	inventory inv = loadInventory(myfile);
	bool ai = parseBool(readLine(myfile));
	iStatValue mc = parseiStat(myfile);
	int c = parseInt(readLine(myfile));
	int s = parseInt(readLine(myfile));
	playerIO newp = playerIO(c, s, index, n, inv, ai, mc);
	//vis
	//int w = MaxLen;//playerpointer->sightMap.getlenx();
	//int h = MaxLen;//playerpointer->sightMap.getleny();
	int w = parseInt(readLine(myfile));
	int h = parseInt(readLine(myfile));
	newp.sightMap = DiscoveryMap(index, w, h);
	//newp.sightMap.discovered = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	//newp.sightMap.visible = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	parseBoolMap(readLine(myfile), newp.sightMap.visible, w, h);//visible
	parseBoolMap(readLine(myfile), newp.sightMap.discovered, w, h);//discovered
	return newp;
}

vector<playerIO> playerparser::loadPlayers(string fn, int &noplayers, int &turnNum, int &playerturn)
{
	ifstream myfile = ifstream(fn);
	vector<playerIO> newVector;
	if (myfile.is_open())
	{
		string line;
		noplayers = parseInt(readLine(&myfile));
		turnNum = parseInt(readLine(&myfile));
		playerturn = parseInt(readLine(&myfile));
		for (int i = 0; i < noplayers; i++)
		{
			playerIO loaded = loadPlayer(&myfile);
			newVector.push_back(loaded);
		}
		string blank = readLine(&myfile);
		myfile.close();
	}
	else
	{
		noplayers = 0;
		turnNum = 0;
		playerturn = 0;
	}
	return newVector;
}

