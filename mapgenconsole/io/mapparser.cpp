#include "mapparser.h"

void mapparser::writeFile(string fn, int ver, int w, int h, int** types, int** stypes, int** heights, int** rot, float** tmp, bool** water, bool** footpaths, bool** caves, bool** visible, bool** discovered, vector<chest> &chestspointer, bool sqHex)
{
	ofstream myfile;
	myfile.open(fn);
	myfile << ver << "\n";
	myfile << w << "\n";
	myfile << h << "\n";
	write2DMap(&myfile, w, h, types);
	write2DMap(&myfile, w, h, stypes);
	write2DMap(&myfile, w, h, heights);
	write2DMap(&myfile, w, h, rot);
	write2DMap(&myfile, w, h, tmp);
	write2DMap(&myfile, w, h, water);
	write2DMap(&myfile, w, h, footpaths);
	write2DMap(&myfile, w, h, caves);
	write2DMap(&myfile, w, h, visible);
	write2DMap(&myfile, w, h, discovered);
	writeChests(&myfile, w, h, &chestspointer);
	writeBool(&myfile, sqHex);
	myfile.close();
}

void mapparser::loadFile(string fn, int &ver, int &w, int &h, int ** types, int ** stypes, int ** heights, int** rot, float ** tmp, bool** water, bool** footpaths, bool** caves, bool** visible, bool** discovered, vector<chest> &chestslist, bool &sqHex)
{
	ifstream myfile = ifstream(fn);
	if (myfile.is_open())
	{
		string line;
		ver = parseInt(readLine(&myfile));
		w = parseInt(readLine(&myfile));
		h = parseInt(readLine(&myfile));
		//now that I have the size get me the types
	    parseIntMap(readLine(&myfile), types, w, h);  //types
		parseIntMap(readLine(&myfile), stypes, w, h); //stypes
		parseIntMap(readLine(&myfile), heights, w, h);//heightmap
		parseIntMap(readLine(&myfile), rot, w, h);//rotation
		for (int x = 0; x < w; x++)
		{
			for (int y = 0; y < h; y++)
			{
				tmp[x][y] = parseFloat(readLine(&myfile));
			}
		}
		string blank = readLine(&myfile);
		parseBoolMap(readLine(&myfile), water, w, h);//water
		parseBoolMap(readLine(&myfile), footpaths, w, h);//footpaths
		parseBoolMap(readLine(&myfile), caves, w, h);//caves
		parseBoolMap(readLine(&myfile), visible, w, h);//visible
		parseBoolMap(readLine(&myfile), discovered, w, h);//discovered
		chestslist = loadChests(&myfile, w, h);						//chests
		sqHex = parseBool(readLine(&myfile));
		myfile.close();
	}
	else
	{
		w = 0;
		h = 0;
	}
}

void mapparser::writeChests(ofstream * myfile, int w, int h, vector<chest>* chestspointer)
{
	int siz = chestspointer->size();
	*myfile << siz << "\n";
	vector<chest> sameADDRESS = *chestspointer;
	for (int i =0; i < siz; i++)
	{
		chest* chpointer = &sameADDRESS[i];
		writeChest(myfile, w, h, chpointer);
	}
}

void mapparser::writeChest(ofstream * myfile, int w, int h, chest * chestpointer)
{
	*myfile << chestpointer->x << "\n";
	*myfile << chestpointer->y << "\n";
	writeInventory(myfile, &chestpointer->chestInv);
}

vector<chest> mapparser::loadChests(ifstream * myfile, int w, int h)
{
	vector<chest> listc = vector<chest>();
	int size = parseInt(readLine(myfile));
	for (int i = 0; i < size; i++)
	{
		chest newc = loadChest(myfile, w, h);
		listc.push_back(newc);
	}
	return listc;
}

chest mapparser::loadChest(ifstream * myfile, int w, int h)
{
	coord p = parseCoord(myfile);
	chest ch = chest(p);
	ch.chestInv = loadInventory(myfile);
	return ch;
}

