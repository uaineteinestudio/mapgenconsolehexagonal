#pragma once

#include "statparser.h"
//#include "../items/item.h"
#include "../items/inventory.h"

static class itemparser : public statparser
{
public:
	static void writeInventory(ofstream* myfile, inventory* invvector);
	static void writeItem(ofstream* myfile, item* itempointer);
	static void writeItemStats(ofstream* myfile, stats* itemStats);
	static inventory loadInventory(ifstream* myfile);
	static item loadItem(ifstream* myfile);
	static stats loadItemStats(ifstream* myfile);
};

