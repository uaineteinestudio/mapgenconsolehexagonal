#pragma once

#include "fileparser.h"
#include "../items/iStatValue.h"
#include "../items/fStatValue.h"

class statparser : public fileparser
{
public:
	static void writefStat(ofstream* myfile, fStatValue toWrite);
	static fStatValue parsefStat(ifstream* myfile);
	static void writeiStat(ofstream* myfile, iStatValue toWrite);
	static iStatValue parseiStat(ifstream* myfile);
};

