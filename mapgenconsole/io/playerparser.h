#pragma once

#include "itemparser.h"
#include "../player/playerclass/playerIO.h"
#include <vector>

using namespace std;

static class playerparser : public itemparser
{
public:
	static void writePlayers(string fn, vector<playerIO>* playerlistpointer, int noplayers, int turnNum, int playerturn);
	static void writePlayer(ofstream* myfile, playerIO* playerpointer);
	static vector<playerIO> loadPlayers(string fn, int &noplayers, int &turnNum, int &playerturn); //read into all this please
	static playerIO loadPlayer(ifstream* myfile);
};

