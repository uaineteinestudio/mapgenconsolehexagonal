#pragma once

#include "itemparser.h"
#include "../player/boardobjs/piece.h"

static class pieceparser : public itemparser
{
public:
	static void writePieces(string fn, int ver, vector<piece*>* piecesvector, int numPieces);
	static void writePiece(ofstream* myfile, int ver, piece* piecepointer);
	static void writeStats(ofstream* myfile, piecestats* pieceStats);
	static vector<piece> loadPieces(string fn, int& ver, int &numPieces, int lenx, int leny);
	static piece loadPiece(ifstream* myfile, int lenx, int leny);
	static piecestats loadStats(ifstream* myfile);
};

