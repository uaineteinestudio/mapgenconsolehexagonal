#pragma once

#include "itemparser.h"
#include "../items/chest.h"

static class mapparser : public itemparser
{
public:
	static void writeFile(string fn, int ver, int w, int h, int** types, int** stypes, int** heights, int** rot, float** tmp, bool** water, bool** footpaths, bool** caves, bool** visible, bool** discovered, vector<chest> &chestspointer, bool sqHex);
	static void loadFile(string fn, int &ver, int &w, int &h, int **types, int** stypes, int** heights, int** rot, float** tmp, bool** water, bool** footpaths, bool** caves, bool** visible, bool** discovered, vector<chest> &chestslist, bool &sqHex); //read into all this please
protected:
	static void writeChests(ofstream* myfile, int w, int h, vector<chest>* chestspointer);
	static void writeChest(ofstream* myfile, int w, int h, chest* chestpointer);
	static vector<chest> loadChests(ifstream* myfile, int w, int h);
	static chest loadChest(ifstream* myfile, int w, int h);
};

