#include "statparser.h"

void statparser::writefStat(ofstream * myfile, fStatValue toWrite)
{
	*myfile << toWrite.Default << "\n"; //default
	*myfile << toWrite.Value << "\n";	//value
}

fStatValue statparser::parsefStat(ifstream* myfile)
{
	float def = parseFloat(readLine(myfile));
	float val = parseFloat(readLine(myfile));
	fStatValue fv = fStatValue(def);
	fv.Value = val;
	return fv;
}

void statparser::writeiStat(ofstream * myfile, iStatValue toWrite)
{
	*myfile << toWrite.Default << "\n"; //default
	*myfile << toWrite.Value << "\n";	//valu
}

iStatValue statparser::parseiStat(ifstream * myfile)
{
	int def = parseFloat(readLine(myfile));
	int val = parseFloat(readLine(myfile));
	iStatValue iv = iStatValue(def);
	iv.Value = val;
	return iv;
}
