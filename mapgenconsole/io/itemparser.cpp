#include "itemparser.h"

void itemparser::writeInventory(ofstream * myfile, inventory * invvector)
{
	int n = invvector->getN();
	//write how many
	*myfile << n << "\n";
	inventory sameADDRESSinv = *invvector;
	for (int i = 0; i < n; i++)
	{
		item* itempointer = &sameADDRESSinv.items[i];
		writeItem(myfile, itempointer);
	}
}

void itemparser::writeItem(ofstream * myfile, item * itempointer)
{
	*myfile << itempointer->name << "\n";
	*myfile << itempointer->description << "\n";
	*myfile << itempointer->ItemLevel.GetLevel() << "\n";
	*myfile << itempointer->NumUses << "\n";
	writeiStat(myfile, itempointer->Cooldown);
	//write item stats
	item sameADDRESSasItem = *itempointer;
	writeItemStats(myfile, &sameADDRESSasItem.theStats);
}

void itemparser::writeItemStats(ofstream * myfile, stats * itemStats)
{
	writefStat(myfile, itemStats->Health);	//health
	writefStat(myfile, itemStats->attack);	//attack
	writefStat(myfile, itemStats->defence);
	writeiStat(myfile, itemStats->range);
}

inventory itemparser::loadInventory(ifstream * myfile)
{
	int n = parseInt(readLine(myfile));
	inventory newInv = inventory();
	for (int i = 0; i < n; i++)
	{
		item newItem = loadItem(myfile);
		newInv.addItem(newItem);
	}
	return newInv;
}

item itemparser::loadItem(ifstream * myfile)
{
	string name = readLine(myfile);
	string d = readLine(myfile);
	int lvl = parseInt(readLine(myfile));
	int numUses = parseInt(readLine(myfile));
	cooldown cool = parseiStat(myfile);
	stats itstats = loadItemStats(myfile);
	return item(name, itstats, d, lvl, numUses, cool);
}

stats itemparser::loadItemStats(ifstream * myfile)
{
	fStatValue health = parsefStat(myfile);
	fStatValue attack = parsefStat(myfile);
	fStatValue defence = parsefStat(myfile);
	iStatValue range = parseiStat(myfile);
	stats base = stats(health, attack, defence, range);
	return base;
}
