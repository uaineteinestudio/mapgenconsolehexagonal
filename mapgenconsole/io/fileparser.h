#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "../coord.h"

using namespace std;

class fileparser
{
public:
	static void write2DMap(ofstream* myfile, int w, int h, int**map);
	static void write2DMap(ofstream* myfile, int w, int h, bool**map);
	static void write2DMap(ofstream* myfile, int w, int h, float**map);
	static void writeBool(ofstream* myfile, bool toWrite);
	static string readLine(ifstream* myfile);
	static bool under10(int val);
	static bool under100(int val);
	static int parseInt(string val);
	static float parseFloat(string val);
	static bool parseBool(string val);
	static void parseIntMap(string line, int **map, int w, int h);
	static void parseBoolMap(string line, bool **map, int w, int h);
	static coord parseCoord(ifstream* myfile);
};

