#include "pieceparser.h"

void pieceparser::writePieces(string fn, int ver, vector<piece*>* piecesvector, int numPieces)
{
	ofstream myfile;
	myfile.open(fn);
	//write num pieces and version
	myfile << ver << "\n";
	myfile << numPieces << "\n";
	//write my pieces
	vector<piece*> vr = *piecesvector; //Create a reference to same place
	for (int i = 0; i < numPieces; i++)
	{
		writePiece(&myfile, ver, vr[i]);
	}
	myfile.close();
}

vector<piece> pieceparser::loadPieces(string fn, int& ver, int &numPieces, int lenx, int leny)
{
	//todo
	vector<piece> piecesvec;
	ifstream myfile = ifstream(fn);
	if (myfile.is_open())
	{
		string line;
		ver = parseInt(readLine(&myfile));
		string piecesline = readLine(&myfile);
		numPieces = parseInt(piecesline);
		for (int i = 0; i < numPieces; i++)
		{
			piecesvec.push_back(loadPiece(&myfile, lenx, leny));
		}
		//el fino-finished
		string blank = readLine(&myfile);
		myfile.close();
	}
	else
	{
		//default
		//todo
	}
	return piecesvec;
}

void pieceparser::writePiece(ofstream * myfile, int ver, piece * piecepointer)
{
	string uid = pieceuid::getuid(piecepointer->getOwner(), piecepointer->getIndex());
	*myfile << uid << "\n";	//uid
	*myfile << piecepointer->getIDType() << "\n";	//type
	*myfile << piecepointer->getName() << "\n";		//name
	coord p = piecepointer->getPos();
	*myfile << p.x << "\n" << p.y << "\n";			//pos
	writeStats(myfile, &piecepointer->pieceStats);	//stats
}

void pieceparser::writeStats(ofstream * myfile, piecestats * pieceStats)
{
	piecestats samePieceADDRESS = *pieceStats;
	stats* statbase = &samePieceADDRESS;
	writeItemStats(myfile, statbase);		//base stats
	//piece stats
	writefStat(myfile, pieceStats->HealthMax);
	writeiStat(myfile, pieceStats->visibility);
	writeiStat(myfile, pieceStats->movedist);
	writeBool(myfile, pieceStats->alive);
	writeBool(myfile, pieceStats->canMove);
	writeBool(myfile, pieceStats->canPhase);
	writeBool(myfile, pieceStats->canHeal);
}

piece pieceparser::loadPiece(ifstream * myfile, int lenx, int leny)
{
	string uid = readLine(myfile);
	int type = parseInt(readLine(myfile));
	string name = readLine(myfile);
	coord p = parseCoord(myfile);
	piecestats st = loadStats(myfile);
	return piece(p, pieceuid::getPlayerIndex(uid), type, pieceuid::getPieceIndex(uid), name, lenx, leny);
}

piecestats pieceparser::loadStats(ifstream * myfile)
{
	stats base = loadItemStats(myfile);
	//piecestats
	fStatValue healthMax = parsefStat(myfile);
	iStatValue visibility = parseiStat(myfile);
	iStatValue movedist = parseiStat(myfile);
	bool alive = parseBool(readLine(myfile));
	bool canmove = parseBool(readLine(myfile));
	bool canphase = parseBool(readLine(myfile));
	bool canheal = parseBool(readLine(myfile));

	return piecestats(movedist, visibility, healthMax, canmove, canphase, canheal, base);
}
