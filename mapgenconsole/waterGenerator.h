#pragma once

#include "maplen.h"
#include "coord.h"
#include "boundaries/boundchecker.h"
#include "blockTypes.h"
#include "boundaries/BoundaryMaps.h"
#include "arrays/dynInitaliser.h"
#include <vector>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

using namespace std;

class waterGenerator
{
public:
	waterGenerator(int hL);
	~waterGenerator();
	void getWater(bool** newmap, int** heightmap);
	bool belowLim(int atHeight);
	int getHeightLimit();
	void setnewLim(int newlim);
	int howMuchWater(bool ** watermap);
	void makeSand(int** bt, bool** water);
private:
	int heightLim;
	float sandchance = 0.9;
	int attemptLimit = 5; //limit on modifying the height limit for water
};

