#include "selCircle.h"

using namespace std;

void selCircle::getSelCircleM(coord p, vector<coord>& selcircle, vector<moveability>& move, int lenx, int leny, int rad, bool sqHex)
{
	//clear vector-handled in the getRadiusCircle circle method
	//distance calc map
	bool** map = dynInitaliser::makeBool(lenx, leny, true);//all true

	int radd = rad + 2;
	//getMaxValues
	int maxX = min(p.x + radd, lenx);
	int maxY = min(p.y + radd, leny);
	//getMinValues
	int minX = max(p.x - radd, 0);
	if (minX > 0 && minX % 2 == 0) //we want to translate by even number
		minX -= 1;
	int minY = max(p.y - radd, 0);

	//range
	coord max = coord(maxX, maxY);
	coord min = coord(minX, minY);
	coord range = max - min;
	//swap the range due to the handling of the algorithm
	range.swap();

	//get points
	if (sqHex == true) //if hex
		hexAdjaceny::getRadiusCircle(p.x, p.y, rad, selcircle, lenx, leny);
	else
		squareadjacency::getRadiusCircle(p.x, p.y, rad, selcircle, lenx, leny);

	//now to find distances
	for (int i = 0; i < selcircle.size(); i++)
	{
		int dist = hexdist::calcdist(map, p - min, selcircle[i] - min, range.x, range.y);
		move.push_back(moveability(dist, true));
	}

	//start point
	selcircle.push_back(p);
	move.push_back(moveability(0, true));
	//el fino
	dynInitaliser::del(map, lenx);
}

void selCircle::getSelCircleM(int x, int y, vector<coord>& selcircle, vector<moveability>& move, int lenx, int leny, int rad, bool sqHex)
{
	getSelCircleM(coord(x, y), selcircle, move, lenx, leny, rad, sqHex);
}

void selCircle::updateSelCirclePiece(int x, int y, vector<coord>& selcircle, vector<moveability>& move, piece * thePiece)
{
	int curSize = selcircle.size();
	coord p = thePiece->getPos();
	for (int i = 0; i < curSize; i++)
	{
		if (p == selcircle[i])
		{
			//if so update it
			move[i].piece = true;
		}
	}
}

/*
void selCircle::getSelCircle(coord p, vector<coord>& selcircle, vector<int> &dists, int lenx, int leny)
{
	//clear vector
	selcircle.clear();

	//PARALLEL REMOVED AS IT IS TOOOO SLOW BUT COULD BE USEFUL IN OTHER APPLICATIONS
	//parallel_for(HexAdjLen, [&](int start, int end) 
	//{
	//	for (int k = start; k < end; k++)
	//	{
	//		coord point = coord(colMat[sel][k] + p.x, rowMat[sel][k] + p.y);
	//		if (boundchecker::inBounds(point.x, point.y, lenx, leny))
	//		{
	//			//since push back isn't thread safe, best bet is to initalise first and then change entry
	//			selcircle[k] = point;
	//		}
	//	}
	//
	//});

	//now find the points I want to add in
	int sel = (p.x) % 2; //based on col
	//use original 1 radius away
	coord point = coord(0, 0);
	for (int k = 0; k < HexAdjLen; k++)
	{
		point = coord(colMat[sel][k] + p.x, rowMat[sel][k] + p.y);
		if (boundchecker::inBounds(point.x, point.y, lenx, leny))
		{
			selcircle.push_back(point);
			dists.push_back(1);
		}
	}
	for (int x = 0; x < SelCirLen; x++)
	{
		point = coord(colSelMat[sel][x] + p.x, rowSelMat[sel][x] + p.y);
		if (boundchecker::inBounds(point.x, point.y, lenx, leny))
		{
			selcircle.push_back(point);
			dists.push_back(2);
		}
	}
	//start point
	selcircle.push_back(p);
	dists.push_back(0);
	//el fino
}

void selCircle::getSelCircle(int x, int y, vector<coord>& selcircle, vector<int> &dists, int lenx, int leny)
{
	getSelCircle(coord(x, y), selcircle, dists, lenx, leny);
}
*/