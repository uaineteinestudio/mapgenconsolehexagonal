#include "BoundaryMaps.h"

using namespace std;

vector<coord> BoundaryMaps::getBoundaries(bool ** water, int &size)
{
	size = 0;
	vector<coord> boundaries = vector<coord>();
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			if (boundchecker::isBoundary(water, x, y))
			{
				size += 1;
				boundaries.push_back(coord(x, y));
			}
		}
	}
	return boundaries;
}

vector<coord> BoundaryMaps::getBoundariesChance(bool ** water, int &size, float chance)
{
	float r = 0;
	size = 0;
	vector<coord> boundaries = vector<coord>();
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			if (r < chance)
			{
				if (boundchecker::isBoundary(water, x, y))
				{
					size += 1;
					boundaries.push_back(coord(x, y));
				}
			}
		}
	}
	return boundaries;
}

bool ** BoundaryMaps::getMapOfType(int ** bt, int t, int w, int l)
{
	bool** map = dynInitaliser::makeBool(w, l, false); //intialise
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < l; y++)
		{
			if (bt[x][y] == t)
				map[x][y] = true;
		}
	}
	return map;
}
