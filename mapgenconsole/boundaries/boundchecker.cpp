#include "boundchecker.h"

using namespace std;

bool boundchecker::inBounds(int x, int y, int w, int h)
{
	if (x < w && y < h)//in bounds
		if (x > -1 && y > -1)
			return true;
		else
			return false;
	else 
		return false;
}

bool boundchecker::isBoundary(bool ** map, int x, int y)
{
	int col[4] = { -1, 0, 0, 1 };
	int row[4] = { 0, -1, 1, 0 };
	if (map[x][y] == true)
	{
		return false;
	}
	for (int i = 0; i < 4; i++)
	{
		int xi = x + col[i];
		int yi = y + row[i];
		if (boundchecker::inBounds(xi, yi, MaxLen, MaxLen))
			if (map[xi][yi] == true)
				return true;
	}
	return false; //else
}