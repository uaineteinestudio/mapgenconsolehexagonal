#pragma once

#include "boundchecker.h"
#include "../arrays/dynInitaliser.h"
#include "../coord.h"
#include <vector>

class BoundaryMaps
{
public:
	static std::vector<coord> getBoundaries(bool** boolmap, int &size);
	static std::vector<coord> getBoundariesChance(bool** boolmap, int &size, float chance);
	//get a bool map of anytype
	static bool** getMapOfType(int** bt, int t, int w, int l); //don't forget to delete after please
};

