#pragma once

#include "../maplen.h"

static class boundchecker
{
public:
	static bool inBounds(int x, int y, int w, int h);
	static bool isBoundary(bool ** map, int x, int y);
};

