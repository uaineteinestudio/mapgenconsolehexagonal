// mapgenconsole.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
//#include "levelmap.h"
#include "levelmapUI.h"
#include "selCircle.h"
//#include "coord.h"
//#include "player.h"
#include "player/playerclass/playerUI.h"
//#include "playerHandler.h"
#include "player/Playerhandling/playerHandlerUI.h"
#include <chrono>
#include "Version/VersionControl.h"

int width = 40;
int height = 40;
levelmapUI* map;
playerHandlerUI* pH;

void getXY(string statement, int &x, int &y)//return x and y
{
	std::cout << "give x and y " << statement << endl;
	std::cin >> x;
	std::cin >> y;
}

void newplayerpiece(player & plyr, int x, int y)
{
	//plyr.addPiece(p);
}

void movePiece(levelmap &map, player &plyr, playerHandlerUI& pH)
{
	std::cout << "give piece index:" << endl;
	int index = 0;
	std::cin >> index;

	//one the piece is selected draw the selection circle
	piece* p = plyr.getPieceByIndex(index);
	cout << "SELECTION CIRCLE" << endl;
	int size = 0;
	vector<moveability>* move = p->move.getMoveAbility();
	vector<coord>* selcirl = p->move.getCirc();
	coord pos = p->getPos();
	ui::print_matrixSelM(*selcirl, *move, p->move.getSize(), width, height, pos.x, pos.y, ui::Red);

	int x = 0;
	int y = 0;
	getXY("for new position", x, y);
	//now move it
	pH.movePiece(&plyr, index, coord(x, y));
}

void action(levelmap& map, player& plyr, playerHandlerUI& pH)
{
	int looplim = 100;
	int i = 0;
	while (i < looplim)
	{
		//spawning
		std::cout << "spawning a new piece (0), move a piece(1), select pos on map (2) or end turn(3)" << endl;
		int r = 0;
		std::cin >> r;
		int x = 0;
		int y = 0;
		switch (r)
		{
		case 0:
			getXY("for spawn point", x, y);
			newplayerpiece(plyr, x, y);
			break;
		case 1:
			movePiece(map, plyr, pH);
			break;
		case 2:
			getXY("for new block selection", x, y);
			map.sel.updateSelectedBlock(x, y);
			break;
		case 3:
			i = looplim;
			break;
		}
		i += 1;
	}
}

/*
void testGen()
{
	int i = 0;
	while (i < 200)
	{
		levelmapUI map = levelmapUI("testout.txt", width, height, true, true);
		map.GenerateMap();
		map.printMaps();
		//cout << map.calcDist(coord(0, 0), coord(5, 10)) << endl;  //#include "coord.h"
		map.destroy();
		system("PAUSE");
		i += 1;
	}
}
*/

//NOT UPDATED
/*
void testMoveDistanceCalculation()
{
	player p1 = player();
	int i = 0;
	//while (i < 200)
	//{
	levelmapUI map = levelmapUI(width, height, true, true);
	map.GenerateMap();
	map.sel.updateSelectedBlock(18, 14);
	map.sel.updateSelectedBlock(4, 4);
	map.printMaps();

	//now calculate that move distance
	vector<int> dists;
	vector<coord> cir;
	coord mypoint = coord(4, 4);
	selCircle::getSelCircle(4, 4, cir, dists, width, height);

	//now make further action
	action(map, p1, pH);
	map.printMaps();
	map.destroy();
	system("PAUSE");
	i += 1;
	//}
}
*/

//NOT UPDATED
/*
void testSpawn()
{
	playerUI p1 = playerUI(0,0,0, "Daniel"); //p1 is 0 index
	playerUI p2 = playerUI(1, 0, 1, "Stephen"); //p2 is 1 index
	int i = 0;
	//while (i < 200)
	//{
	levelmapUI map = levelmapUI(width, height, true, true);
	map.GenerateMap();
	map.sel.updateSelectedBlock(18, 14);
	map.sel.updateSelectedBlock(4, 4);
	p1.initialisepieces();
	p2.initialisepieces();
	p1.randomisePositions(&map, true, 0); //find walkable positions
	map.printMaps();
	p1.printPlayerInfo(&map, width, height);

	//now calculate that move distance
	vector<int> dists;
	vector<coord> cir;
	coord mypoint = coord(4, 4);
	selCircle::getSelCircle(4, 4, cir, dists, width, height);

	//now make further action
	action(map, p1);
	map.printMaps();
	//print me the player pos
	p1.printPlayerInfo(&map, width, height);
	map.destroy();
	system("PAUSE");
	i += 1;
	//}
}
*/

void testSpawnV2()
{
	int i = 0;
	//while (i < 200)
	//{
	int width = 30;
	int height = 30;
	levelmapUI map = levelmapUI("testout.txt", width, height, true, true, false);
	map.GenerateMap();
	map.sel.updateSelectedBlock(18, 14);
	map.sel.updateSelectedBlock(4, 4);
	playerHandlerUI pH = playerHandlerUI(&map);
	pH.addPlayer(playerUI(&map, 0, 0, 0, "Daniel"));
	pH.addPlayer(playerUI(&map, 1, 0, 1, "Stephen"));
	pH.spawnPlayers(true);
	map.printMaps();
	pH.printPlayerInfo(&map, 0, width, height);//0th index

	//now make further action
	action(map, *pH.getCurPlayer(), pH);
	map.printMaps();
	//print me the player pos
	pH.printPlayerInfo(&map, 0, width, height);//0th index
	//end that player turn
	pH.endCurrentPlayerTurn();

	map.destroy();
	system("PAUSE");
	i += 1;
	//}
}

void testSpawnCavesFootpathsBug()
{
	int i = 0;
	//while (i < 200)
	//{
	int width = 20;
	int height = 20;
	levelmapUI map = levelmapUI("testout.txt", width, height, true, true);
	map.GenerateMap();
	map.sel.updateSelectedBlock(18, 14);
	map.sel.updateSelectedBlock(4, 4);
	playerHandlerUI pH = playerHandlerUI(&map);
	pH.addPlayer(playerUI(&map, 0, 0, 0, "Daniel"));
	pH.addPlayer(playerUI(&map, 1, 0, 1, "Stephen"));
	pH.spawnPlayers(true);
	map.printMaps();
	map.printFootPathCaveOverlap();
	pH.printPlayerInfo(&map, 0, width, height);//0th index

	//now make further action
	action(map, *pH.getCurPlayer(), pH);
	map.printMaps();
	//print me the player pos
	pH.printPlayerInfo(&map, 0, width, height);//0th index
	//end that player turn
	pH.endCurrentPlayerTurn();

	map.destroy();
	system("PAUSE");
	i += 1;
	//}
}

void testSpawnV2withSave()
{
	int i = 0;
	//while (i < 200)
	//{
	int width = 20;
	int height = 20;
	levelmapUI map = levelmapUI("testout.txt", width, height, true, true);
	map.GenerateMap();
	map.sel.updateSelectedBlock(18, 14);
	map.sel.updateSelectedBlock(4, 4);
	playerHandlerUI pH = playerHandlerUI(&map);
	pH.addPlayer(playerUI(&map, 0, 0, 0, "Daniel"));
	pH.addPlayer(playerUI(&map, 1, 0, 1, "Stephen"));
	pH.spawnPlayers(true);
	map.printMaps();
	pH.printPlayerInfo(&map, 0, width, height);//0th index

	//save it all
	map.savemap();
	pH.SaveAll();
	cout << "  Saved the map !" << endl;

	//now make further action
	action(map, *pH.getCurPlayer(), pH);
	map.printMaps();
	//print me the player pos
	pH.printPlayerInfo(&map, 0, width, height);//0th index
	//end that player turn
	pH.endCurrentPlayerTurn();

	map.destroy();
	system("PAUSE");
	i += 1;
	//}
}

void testSpawnV2withLoad()
{
	//int i = 0;
	//while (i < 200)
	//{
	levelmapUI newMap = levelmapUI("testout.txt", width, height, true, true);
	map = &newMap;
	map->GenerateMap();
	map->sel.updateSelectedBlock(18, 14);
	map->sel.updateSelectedBlock(4, 4);
	playerHandlerUI newpH = playerHandlerUI(map);
	pH = &newpH;
	pH->addPlayer(playerUI(map, 0, 0, 0, "Daniel"));
	pH->addPlayer(playerUI(map, 1, 0, 1, "Stephen"));
	pH->spawnPlayers(true);
	map->printMaps();
	pH->printPlayerInfo(map, 0, width, height);//0th index

	//save it all
	map->savemap();
	pH->SaveAll();
	cout << "  Saved the map !" << endl;

	//destroy
	map->destroy();
	pH->destroy();
	system("PAUSE");
	//i += 1;
	//}

	//load it
	cout << "  Now loading the map !" << endl;
	levelmapUI loadMap = levelmapUI("testout.txt", MaxLen, MaxLen, true, true);
	map = &loadMap;
	//load players
	playerHandlerUI newplayherH = playerHandlerUI(map);
	pH = &newplayherH;
	map->loadmap();
	pH->LoadAll();
	map->printMaps();
	pH->printPlayerInfo(map, 0, width, height);//0th index
	system("PAUSE");
}

void testChunkUpdate()
{
	int i = 0;
	while (i < 200)
	{
		width = 100;
		height = 100;
		levelmapUI map = levelmapUI("testout.txt", width, height, true, true);
		map.GenerateMap();
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		map.sel.updateSelectedBlock(18, 14);
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[micros]" << std::endl;
		map.sel.updateSelectedBlock(4, 4);
		map.printMaps();
		map.destroy();
		system("PAUSE");
		i += 1;
	}
}


//NOT UPDATED
/*
void testselcircle()
{
	player p1 = player();
	int i = 0;
	//while (i < 200)
	//{
	levelmapUI map = levelmapUI(width, height, true, true);
	map.GenerateMap();
	map.sel.updateSelectedBlock(18, 14);
	map.sel.updateSelectedBlock(4, 4);
	map.printMaps();
	action(map, p1);
	map.printMaps();
	map.destroy();
	system("PAUSE");
	i += 1;
	//}
}
*/

void testSave()
{
	int i = 0;
	//while (i < 200)
	//{
		levelmapUI map = levelmapUI("testout.txt", width, height, true, true);
		map.GenerateMap();
		map.sel.updateSelectedBlock(18, 14);
		map.sel.updateSelectedBlock(4, 4);
		map.printMaps();
		map.savemap();
		map.destroy();
		system("PAUSE");
		i += 1;
	//}
}

void testLoad()
{
	int i = 0;
	//while (i < 200)
	//{
		levelmapUI map = levelmapUI("testout.txt", MaxLen, MaxLen, true, true);
		map.loadmap();
		map.printMaps();
		map.destroy();
		system("PAUSE");
		i += 1;
	//}
}

void hexMapTest()
{
	width = 30;
	height = 30;

	std::cout << "get circle at point:" << std::endl << "(give x and y)" << std::endl;
	int x;
	int y;
	std::cin >> x;
	std::cin >> y;
	std::cout << "get radius:" << std::endl;
	int rad = 10;
	std::cin >> rad;

	int marker = 9;

	coord cent = coord(x, y);
	int** matrix = dynInitaliser::makeInt(width, height, 0);
	vector<coord> circle;

	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

	hexAdjaceny::getRadiusCircle(cent.x, cent.y, rad, circle, width, height);

	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[micros]" << std::endl;

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			matrix[i][j] = 0;
		}
	}
	for (int i = 0; i < circle.size(); i++)
	{
		matrix[circle[i].x][circle[i].y] = marker;
	}
	matrix[cent.x][cent.y] = 1;
	ui::hexprintSingleDigit(matrix, width, height, ui::Red, marker);
	//cleanup
	dynInitaliser::del(matrix, width);
}

int main()
{
	VersionControl v = VersionControl();
	std::cout << "Complete engine version " << v.CompleteVersion.VersionString << endl;
	std::cout << "Map engine version " << v.MapVersion.VersionString << endl;
	std::cout << "Player engine version " << v.PlayerVersion.VersionString << endl;
	std::cout << "Items engine version " << v.ItemsVersion.VersionString << endl;
	std::cout << endl;

	//testGen();
	//testChunkUpdate();
	//testselcircle();
	//testMoveDistanceCalculation();
	//testSpawn();
	//testSave();
	//testLoad();
	//hexMapTest();
	//testSpawnV2withSave();
	testSpawnV2withLoad();
	//testSpawnV2();
	//testSpawnCavesFootpathsBug();
}
