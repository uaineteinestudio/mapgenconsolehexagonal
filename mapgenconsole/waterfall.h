#pragma once

#include "maplen.h"
#include "coord.h"

class waterfall	//must be hex friendly too
{
public:
	waterfall(int xi, int yi, bool falloffDirs[rotLen]);
	waterfall(coord pos, bool falloffDirs[rotLen]);
	~waterfall();
	int getX();
	int getY();
	coord getpos();
private:
	coord pos;
	bool dirs[rotLen];
};

