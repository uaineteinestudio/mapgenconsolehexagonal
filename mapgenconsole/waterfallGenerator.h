#pragma once

//#include "waterGenerator.h"
#include "waterfall.h"
#include "boundaries/boundchecker.h"
#include "blockTypes.h"
#include <vector>

using namespace std;

class waterfallGenerator
{
public:
	waterfallGenerator(int width, int height);
	~waterfallGenerator();
	int findWaterfalls(vector<waterfall>& falls,  int** blocktypes, bool ** water);//pass in by ref and return size
private:
	bool isWaterfall(int** bt, bool** water, int x, int y, bool * dirs);
	int w;	//width
	int h;	//height
};

