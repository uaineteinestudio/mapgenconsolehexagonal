#pragma once

#include "genclass.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

class smoothGen : public genclass
{
public:
	smoothGen(int smthfac, int width, int height) : genclass(width, height)
	{
		smoothfac = smthfac;
	}
	smoothGen();
protected:
	int getAvg(int** map, int xi, int yi, int &no);//return number
	bool inMaxBounds(int xi, int yi);
	void smoothOnce(int** map);
	void smoothFac(int** map);
	int smoothfac;
};

