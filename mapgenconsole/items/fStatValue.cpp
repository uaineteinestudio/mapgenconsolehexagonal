#include "fStatValue.h"

fStatValue::fStatValue()
{
	Default = 0;
	Value = 0;
}

fStatValue::fStatValue(float def)
{
	Default = def;
	Value = def;
}

fStatValue::~fStatValue()
{
}

void fStatValue::reset()
{
	Value = Default;
}

iStatValue fStatValue::toIStat()
{
	iStatValue v = iStatValue(Default);
	v.Value = Value;
	return v;
}
