#pragma once

#include "chest.h"

class randomchest : public chest
{
public:
	//default constructor of pos 0,0
	randomchest() : chest(0, 0)
	{
		RandomiseInventory();
	}
	randomchest(int cx, int cy) : chest(cx, cy)
	{
		RandomiseInventory();
	}
	randomchest(coord cpos) : chest(cpos.x, cpos.y)
	{
		RandomiseInventory();
	}
	~randomchest();
protected:
	void RandomiseInventory();
};

