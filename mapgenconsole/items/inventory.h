#pragma once

#include <vector>
#include "item.h"

using namespace std;

class inventory
{
	friend class itemparser;
public:
	inventory();	//DEFAULT
	~inventory();

	void destroy();	//delete

	int MaxSize = 1000;
	void addItem(item addthis);
	void addItem(item* addpointer);
	item popItem(int index);
	int getN()	{ return nItems; }
	bool CanAcceptMore()
	{
		return nItems < MaxSize;
	}
protected:
	vector<item> items;
	int nItems = 0;
};

