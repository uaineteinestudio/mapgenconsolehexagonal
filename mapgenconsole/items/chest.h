#pragma once

#include "../coord.h" //hopefully can make a static list of this
#include "inventory.h"

class chest : public coord
{
public:
	//default constructor of pos 0,0
	chest() : coord(0, 0)
	{
		chestInv = inventory();
	}
	chest(int cx, int cy) : coord(cx, cy)
	{
		chestInv = inventory();
	}
	chest(coord cpos) : coord(cpos.x, cpos.y)
	{
		chestInv = inventory();
	}
	~chest();

	inventory chestInv = inventory();
};

