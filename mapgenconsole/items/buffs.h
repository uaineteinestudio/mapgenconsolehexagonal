#pragma once

#include "stats.h"
#include "../text/Named.h"
#include "../text/Descriptor.h"

class buff : public Named, public Descriptor
{
	//io friends
	friend class itemparser;
public:
	buff(string n, stats nS, string descr) : Named(n), Descriptor(descr)
	{
		theStats = nS;
		description = descr;
	}
	~buff();
protected:
	stats theStats;
};

