#pragma once

#include <string>

using namespace std;

class itemlevel
{
public:
	itemlevel(); //default 0 common
	itemlevel(int level);
	~itemlevel();
	int GetLevel()
	{
		return Level;
	}
	string GetLevelDescription();
private:
	int Level = 0;
};

static const itemlevel common = itemlevel(0);
static const itemlevel uncommon = itemlevel(1);
static const itemlevel rare = itemlevel(2);
static const itemlevel godly = itemlevel(3);
static const itemlevel quest = itemlevel(100);
//cards
static const itemlevel itemcard = itemlevel(200);
static const itemlevel abilitycard = itemlevel(201);
static const itemlevel powercard = itemlevel(202);

