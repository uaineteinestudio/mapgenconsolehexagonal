#pragma once

#include "iStatValue.h"

class fStatValue
{
public:
	fStatValue();	//DEFAULT
	fStatValue(float def);
	~fStatValue();
	float Default;
	float Value;
	void reset();
	iStatValue toIStat();

	//implicit conversion
	operator float() const { return Value; }
};

