#include "iStatValue.h"

iStatValue::iStatValue()
{
	Default = 0;
	Value = 0;
}

iStatValue::iStatValue(int def)
{
	Default = def;
	Value = def;
}

iStatValue::~iStatValue()
{
}

void iStatValue::reset()
{
	Value = Default;
}
