#pragma once

#include "iStatValue.h"

class cooldown : public iStatValue
{
public:
	cooldown(int def) : iStatValue(def)
	{
		Value = 0;
	}
	cooldown(int def, int v) : iStatValue(def)
	{
		Value = v;
	}
	cooldown(iStatValue ivalue) : iStatValue(ivalue.Default)
	{
		Value = ivalue.Value;
	}
	~cooldown();
	int getCoolDownRemaining() { return Value; }
	void decrease();
};

