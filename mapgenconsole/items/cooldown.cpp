#include "cooldown.h"

cooldown::~cooldown()
{
}

void cooldown::decrease()
{
	if (Value == 0)
	{
		Value = Default;
	}
	else
	{
		Value -= 1;
	}
}