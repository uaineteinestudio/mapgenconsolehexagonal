#pragma once

#include "fStatValue.h"
#include "iStatValue.h"

class stats
{
	friend class piecestats;
	//io
	friend class statparser;
	friend class itemparser;
	friend class pieceparser;
public:
	stats();	//DEFAULT
	stats(fStatValue curH, fStatValue atck, fStatValue def, iStatValue ran);
	stats(float curH, float atck, float def, int ran);
	~stats();
protected:
	fStatValue Health = 100;
	fStatValue attack = 1;
	fStatValue defence = 1;
	iStatValue range = 1;
};

