#include "inventory.h"

inventory::inventory()
{
	nItems = 0;
	int MaxSize = 1000;
}

inventory::~inventory()
{
}

void inventory::destroy()
{
	items.clear();
	nItems = 0;
}

void inventory::addItem(item addthis)
{
	items.push_back(addthis);
	nItems += 1;
}

void inventory::addItem(item * addpointer)
{
	item a = *addpointer;
	addItem(a);
}

item inventory::popItem(int index)
{
	item v = items[index];
	items.erase(items.begin() + index);
	nItems -= 1;
	return v;
}
