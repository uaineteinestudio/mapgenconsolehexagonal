#pragma once
class iStatValue
{
public:
	iStatValue();	//DEFAULT
	iStatValue(int def);
	~iStatValue();
	int Default;
	int Value;
	void reset();

	//implicit conversion
	operator int() const { return Value; }
};

