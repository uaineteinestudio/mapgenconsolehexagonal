#pragma once

#include "buffs.h"
#include "itemlevel.h"
#include "cooldown.h"

class item : public buff
{
	friend class itemparser;
public:
	item(string n, stats nS, string descr, itemlevel itmlvl, int numUses, cooldown Cooldwn) : buff(n, nS, descr)
	{
		ItemLevel = itmlvl;
		NumUses = numUses;
		Cooldown = Cooldwn;
	}
	~item();
	itemlevel ItemLevel;
	cooldown Cooldown = 1;
	void use();
	int getNumUsesRemaining()	{ return NumUses; }
	bool canuse()				{ return Cooldown.Value == 0; }
	bool UsesDepleted()			{ return discarded; }
	bool IsInfinite()			{ return NumUses == -1; }
protected:
	int NumUses = -1; //indefinite
	bool discarded = false;
};

