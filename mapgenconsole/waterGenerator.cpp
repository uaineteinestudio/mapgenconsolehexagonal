#include "waterGenerator.h"

waterGenerator::waterGenerator(int hL)
{
	heightLim = hL;
}

waterGenerator::~waterGenerator()
{
}

void waterGenerator::makeSand(int** bt, bool** water)
{
	int size = 0;
	vector<coord> bounds = BoundaryMaps::getBoundariesChance(water, size, sandchance);
	for (int i = 0; i < size; i++)
	{
		int xi = bounds[i].x;
		int yi = bounds[i].y;
		switch (bt[xi][yi])
		{
		case blockTypes::cave:
			//do nothing
			break;
		default:
			bt[xi][yi] = blockTypes::sand;
		}
	}
	//cleanup please
	bounds.clear();
	bounds = vector<coord>();

	//now that I have 'sand' get the boundaries of sand and grass please
	bool** sandmap = BoundaryMaps::getMapOfType(bt, blockTypes::sand, MaxLen, MaxLen);
	size = 0;
	bounds = BoundaryMaps::getBoundariesChance(sandmap, size, sandchance);
	for (int i = 0; i < size; i++)
	{
		if (bt[bounds[i].x][bounds[i].y] == blockTypes::grass) //sand and grass bordering
			bt[bounds[i].x][bounds[i].y] = blockTypes::sandgrass;
	}
	//cleanup please
	bounds.clear();
	bounds = vector<coord>();
	dynInitaliser::del(sandmap, MaxLen);

	bool** sandmap2 = BoundaryMaps::getMapOfType(bt, blockTypes::sandgrass, MaxLen, MaxLen);
	size = 0;
	bounds = BoundaryMaps::getBoundariesChance(sandmap2, size, sandchance);
	for (int i = 0; i < size; i++)
	{
		if (bt[bounds[i].x][bounds[i].y] == blockTypes::grass) //sand and grass bordering
			bt[bounds[i].x][bounds[i].y] = blockTypes::sandgrass;
	}
	//cleanup please
	dynInitaliser::del(sandmap2, MaxLen);
}

void waterGenerator::getWater(bool ** newmap, int ** heightmap)
{
	int i = 0;
	int originalHL = heightLim;
	int acceptableWaterSize = 20;
	while (i < attemptLimit)
	{
		for (int x = 0; x < MaxLen; x++)
		{
			for (int y = 0; y < MaxLen; y++)
			{
				newmap[x][y] = belowLim(heightmap[x][y]);
			}
		}
		//now count
		int size = howMuchWater(newmap);
		if (size < acceptableWaterSize)
			heightLim += 3;
		else
			break;
		i += 1;
	}
	//restore
	heightLim = originalHL;
}

bool waterGenerator::belowLim(int atHeight)
{
	if (atHeight < heightLim)
		return true;
	else
		return false;
}

int waterGenerator::getHeightLimit()
{
	return heightLim;
}

void waterGenerator::setnewLim(int newlim)
{
	heightLim = newlim;
}

int waterGenerator::howMuchWater(bool ** watermap)
{
	int size = 0;
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			if (watermap[x][y])
			{
				size += 1;
			}
		}
	}
	return size;
}