#pragma once

#include "smoothGen.h"
#include "maplen.h"

class heightmapGenerator : public smoothGen
{
public:
	heightmapGenerator(int width, int height, int smthFac) : smoothGen(smthFac, width, height)
	{
		//ADD MORE HERE
	}
	~heightmapGenerator();
	int** genNewMap();
	int** makeRandomHeightMap();
private:
	static const int MaxHeight = 512;//changeable
};

