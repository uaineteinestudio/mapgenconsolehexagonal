#pragma once

#include "smoothGen.h"
#include "maplen.h"
#include "boundaries/boundchecker.h"
#include "blockTypes.h"

using namespace std;

class tempCalc : public smoothGen
{
public:
	tempCalc(int dimLength, int smthcont) : smoothGen(smthcont, dimLength, dimLength)
	{
		//ADD HERE
	}
	tempCalc(int width, int height, int smthcont) : smoothGen(smthcont, width, height)
	{
		//ADD HERE
	}
	~tempCalc();
	void calctemp(float **temp, int** type, int** height);
	//output should be between 0 and 1
	static float tmpvalue(int type, int height);//to get the temp value for a tile type. This is a changeable method
private:
	float varamount = 0.1;	//max random variation +/- per smooth step
	float getvecval(float** cur, int x, int y, int & num);
	float getvecval(float cur[MaxLen][MaxLen], int x, int y, int & num);
	void Normalise(float** cur);
	float largest(float** cur);
	void smoothit(float** cur);
	bool isneg(float val); //under 0 or not
};