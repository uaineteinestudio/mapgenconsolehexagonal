#include "levelmapUI.h"

levelmapUI::~levelmapUI()
{
}

void levelmapUI::printMaps()
{
	if (printinfo)
	{

		//heightmap
		//ui::findColours();
		ui::setConsoleColour(ui::Green);
		cout << "Heights:" << endl;
		ui::print_matrix(blockHeights, lenx, leny);
		//water
		ui::setConsoleColour(ui::Blue);
		cout << "Water:" << endl;
		ui::print_matrix(water, lenx, leny);
		//types
		ui::setConsoleColour(ui::Orange);
		cout << "Types:" << endl;
		ui::print_matrix2digit(blockTypes, lenx, leny);
		//temp
		ui::setConsoleColour(ui::Red);
		cout << "Temperature:" << endl;
		ui::print_matrix(temp, lenx, leny);
		//footpaths
		ui::setConsoleColour(ui::Purple);
		cout << "Footpaths:" << endl;
		ui::print_matrix(footpaths, lenx, leny);
		//walkable
		ui::setConsoleColour(ui::Green);
		cout << "Walkable:" << endl;
		ui::print_matrix(walkable, lenx, leny);
		//rotation
		ui::setConsoleColour(ui::Blue);
		cout << "Rotation:" << endl;
		ui::print_matrix(rot, lenx, leny);
		//surface tiles
		ui::setConsoleColour(ui::Orange);
		cout << "Surface Tiles:" << endl;
		ui::print_matrix2digit(surfaceTypes, lenx, leny);
		//caves
		ui::setConsoleColour(ui::Red);
		cout << "Caves:" << endl;
		ui::print_matrix(Caves, lenx, leny);
		//chests
		printChests();
		//printing the scale factors
		ui::setConsoleColour(ui::Purple);
		printScaleMap();
		ui::defColour();
		//give me the active chunks
		ChunkHandlerUI chui = ChunkHandlerUI(cH);
		chui.printActiveChunks(sel.selectedpos, square4hex6);
		//print the selection
		cout << endl;
		cout << "Selection Ring:" << endl;
		//now need to print in the selection circle
		chrono::steady_clock::time_point begin = chrono::steady_clock::now();
		vector<coord> circle = vector<coord>();
		vector<moveability> moves;
		int defrad = 2;
		selCircle::getSelCircleM(sel.selectedpos.x, sel.selectedpos.y, circle, moves, lenx, leny, defrad, square4hex6);
		int size = circle.size();
		chrono::steady_clock::time_point end = chrono::steady_clock::now();
		cout << "Time difference = " << chrono::duration_cast<chrono::microseconds>(end - begin).count() << "[micros]" << endl;
		ui::print_matrixSelM(circle, moves, size, lenx, leny, sel.selectedpos.x, sel.selectedpos.y, ui::Red);
		circle.clear();
		//DISCOVERED
		ui::setConsoleColour(ui::Green);
		cout << "Discovered" << endl;
		ui::print_matrix(sightMap->discovered, lenx, leny);
		//VISIBLE
		ui::setConsoleColour(ui::Purple);
		cout << "Visible" << endl;
		ui::print_matrix(sightMap->visible, lenx, leny);

		//wrap up with def colour
		ui::defColour();
	}
}

void levelmapUI::printFootPathCaveOverlap()
{
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			if (blockTypes[x][y] == blockTypes::cave)
			{
				ui::setConsoleColour(ui::Purple);
			}
			else if(blockTypes[x][y] == blockTypes::caveInterior)
			{
				ui::setConsoleColour(ui::Green);
			}
			else if (blockTypes[x][y] == blockTypes::footpath)
			{
				ui::setConsoleColour(ui::Orange);
			}
			else
			{
				ui::defColour();
			}
			if (blockTypes[x][y] == blockTypes::footpath && Caves[x][y])
			{
				ui::setConsoleColour(ui::Red);
			}
			cout << Caves[x][y];
		}
		cout << endl;
	}
}

void levelmapUI::printWaterfalls(int size)
{
	if (printinfo)
	{

		//now print
		int** blankArr = dynInitaliser::makeInt(lenx, leny, 0);
		int x = 0;
		int y = 0;
		for (int i = 0; i < size; i++)
		{
			x = waterfalls[i].getX();
			y = waterfalls[i].getY();
			cout << x << " " << y << endl;
			blankArr[x][y] = 1;
		}
		ui::print_matrix(blankArr, lenx, leny);
		dynInitaliser::del(blankArr, lenx);

	}
}

void levelmapUI::printScaleMap()
{

	float** zscale = dynInitaliser::makeFloat(lenx, leny, 1); //initalised with a value of 1
	int type = 0;
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			type = blockTypes[x][y];
			zscale[x][y] = blockTypes::getZScale(type);//from type
		}
	}
	cout << "Z Scale factor:" << endl;
	ui::print_matrix(zscale, lenx, leny);
	//clean up by deleting
	dynInitaliser::del(zscale, lenx);

}

void levelmapUI::printChests()
{
	//chests
	ui::setConsoleColour(ui::Blue);
	cout << "Chests:" << endl;
	bool** che = dynInitaliser::makeBool(lenx, leny, false); //initalised with a value of 1
	int type = 0;
	for (int x = 0; x < lenx; x++)
	{
		for (int y = 0; y < leny; y++)
		{
			type = surfaceTypes[x][y];
			if (type == surfaceTypes::chest)
				che[x][y] = true;
		}
	}
	ui::print_matrix(che, lenx, leny);
	dynInitaliser::del(che, lenx);
}
