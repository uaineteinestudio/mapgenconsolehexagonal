#include "levelmapIO.h"

levelmapIO::~levelmapIO()
{
}

void levelmapIO::savemap()
{
	string fn = filename.getName();
	mapparser::writeFile(fn, version, lenx, leny, blockTypes, surfaceTypes, blockHeights, rot, temp, water, footpaths, Caves, sightMap->visible, sightMap->discovered, chests, square4hex6);
}

void levelmapIO::loadmap()
{
	loading = true;
	string fn = filename.getName();
	DiscoveryMap nmap = DiscoveryMap(0, MaxLen, MaxLen);
	sightMap = &nmap;
	mapparser::loadFile(fn, version, lenx, leny, blockTypes, surfaceTypes, blockHeights, rot, temp, water, footpaths, Caves, sightMap->visible, sightMap->discovered, chests, square4hex6);
	//CHUNKS
	cH = ChunkHandler(lenx, leny, activechunksradius);
	cH.UpdateActiveChunks(sel.selectedpos, sel.selectedpos);
	findWalkable();
	loading = false;
}