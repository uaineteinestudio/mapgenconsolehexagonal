#pragma once

#include "mapvisibility.h"
#include "Temp.h"
#include "heightmapGenerator.h"
#include "stonegenerator.h"
#include "footpathgen.h"
#include "waterGenerator.h"
#include "waterfallGenerator.h"
#include "foliagegen.h"
#include "caveGenerator.h"

//inherit from mapdata
class mapgenerate : public mapvisibility
{
public:
	mapgenerate() : mapvisibility(MaxLen, MaxLen, false)
	{
		//anything to add
	};
	mapgenerate(int lx, int ly, bool squareorhex) : mapvisibility(lx, ly, squareorhex) //0 is square, 1 is hex
	{
		//anything to add
	};
	mapgenerate(int lx, int ly, bool squareorhex, bool setStaticHeightBorder) : mapvisibility(lx, ly, squareorhex) //0 is square, 1 is hex
	{
		setStaticHeightBorders = setStaticHeightBorder;
	};
	~mapgenerate();
	//FIELDS
	const int DefBoundaryH = 300;		//boundary height

	//METHODS AND FUNCTIONS
	void FillBorder();	//called in generatemap but could be called again if wanted
	void finaliseTemp();//same as this
	void findWalkable();//same as this
	void GenerateMap();	//main method to generate the whole map-calls all
protected:
	bool generating = false;	//is generating

	bool setStaticHeightBorders = false;
	void makeHeightMap();
	int heightmapsmoothfac = 2;
	void makeCaves();
	void makegrassPatches(int &amountLGrass, int &amountHGrass); //return the sizes here
	void makeWater();
	void makeWaterfalls();
	void makeFoliage();
	void makeStone();
	void establishFootpaths();		//use footpathgen to make some footpaths that 'walkable'
	void surfaceTileCleanup();
};

