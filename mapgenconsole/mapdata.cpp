#include "mapdata.h"

mapdata::~mapdata()
{
}

void mapdata::initalise()
{
	initialisearrs();
	InitialiseTypes();
	InitaliseRotation();
}

void mapdata::destroy()
{
	deletearrs();
}

void mapdata::InitaliseRotation()
{
	if (square4hex6)
		maxRot = rotLenHex;
	else
		maxRot = rotLen;
	/* initialize random seed: */
	srand(time(NULL));
	int range = maxRot - minRot;
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			rot[x][y] = (rand() % range) + minRot;
		}
	}
}

void mapdata::InitialiseTypes()
{
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			blockTypes[x][y] = blockTypes::grass;
			surfaceTypes[x][y] = surfaceTypes::air;
		}
	}
}

void mapdata::initialisearrs()
{
	blockTypes = dynInitaliser::makeInt(MaxLen, MaxLen, blockTypes::stone);
	surfaceTypes = dynInitaliser::makeInt(MaxLen, MaxLen, surfaceTypes::air);
	blockHeights = dynInitaliser::makeInt(MaxLen, MaxLen, 0);
	water = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	temp = dynInitaliser::makeFloat(MaxLen, MaxLen, 0);
	walkable = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	walkableTranspose = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	footpaths = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	rot = dynInitaliser::makeInt(MaxLen, MaxLen, minRot);
	Caves = dynInitaliser::makeBool(MaxLen, MaxLen, false);
	//CHUNKS
	cH = ChunkHandler(lenx, leny, activechunksradius);
	//selected pos at 0 0 to start
	sel.selectedpos = coord(0, 0);
	sel.mousepos = coord(0, 0);
	//as a result of having something selected as in above, we need an active chunk
	cH.UpdateActiveChunks(sel.selectedpos, sel.selectedpos); //old is the same as the new here
	//el fino
	initisedArrs = true;
}

void mapdata::deletearrs()
{
	dynInitaliser::del(blockTypes, MaxLen);
	dynInitaliser::del(surfaceTypes, MaxLen);
	dynInitaliser::del(blockHeights, MaxLen);
	dynInitaliser::del(water, MaxLen);
	dynInitaliser::del(temp, MaxLen);
	dynInitaliser::del(walkable, MaxLen);
	dynInitaliser::del(walkableTranspose, MaxLen);
	dynInitaliser::del(footpaths, MaxLen);
	dynInitaliser::del(rot, MaxLen);
	dynInitaliser::del(Caves, MaxLen);
	//el fino
	initisedArrs = false;//reset
}

int mapdata::type(int x, int y)
{
	return blockTypes[x][y];
}

int mapdata::stype(int x, int y)
{
	return surfaceTypes[x][y];
}

bool mapdata::isWalkable(int x, int y)
{
	return walkable[x][y];
}

void mapdata::setWalkable(int x, int y, bool walk)
{
	walkable[x][y] = walk;
}

float mapdata::getTemp(int x, int y)
{
	return temp[x][y];
}

void mapdata::setTemp(int x, int y, float tmp)
{
	temp[x][y] = tmp;
}

int mapdata::height(int x, int y)
{
	return blockHeights[x][y];
}

void mapdata::setHeight(int x, int y, int h)
{
	blockHeights[x][y] = h;
}

int mapdata::getRotation(int x, int y)
{
	return rot[x][y];
}

void mapdata::setRotation(int x, int y, int r)
{
	rot[x][y] = r;
}

coord mapdata::getRandomBlock(bool walkableb)
{
	coord p = coord(0, 0); //initalised
	if (walkableb == true)
	{
		const int attemptLim = 100;
		int i = 0;
		while (i < attemptLim)
		{
			p.x = rand() % lenx;
			p.y = rand() % leny;
			if (isWalkable(p.x, p.y))
			{
				//break
				return p;
			}
			//if over 100 iterations and nothing will return (0,0) seen in final line of this function
			i += 1;//increment index
		}
	}
	else
	{
		p.x = rand() % lenx;
		p.y = rand() % leny;
		return p;
	}
	
	return coord(0, 0); //didn't find anything
}

coord mapdata::getRandomCornerBlock(int corner, bool walkableb)
{
	int cornmax = lenx - 1;
	if (corner >= 4) //return the normal one
		return getRandomBlock(walkableb);
	else
	{
		coord p = coord(0, 0); //initalised
		if (walkableb == true)
		{
			const int attemptLim = 100;
			int i = 0;
			while (i < attemptLim)
			{
				switch(corner)
				{
				case 0: //(0,0)
					p.x = rand() % 7;
					p.y = rand() % 7;
					break;
				case 1: //(maxlen, 0)
					p.x = cornmax - rand() % 7;
					p.y = rand() % 7;
					break;
				case 2://(0, maxlen)
					p.x = rand() % 7;
					p.y = cornmax - rand() % 7;
					break;
				case 3://(maxLen, maxlen)
					p.x = cornmax - rand() % 7;
					p.y = cornmax - rand() % 7;
					break;
				}
				if (isWalkable(p.x, p.y))
				{
					//break
					return p;
				}
				//if over 100 iterations and nothing will return (0,0) seen in final line of this function
				i += 1;//increment index
			}
		}
		else
		{
			p.x = rand() % lenx;
			p.y = rand() % leny;
			return p;
		}

		return coord(0, 0); //didn't find anything
	}
}

bool mapdata::gethexorsquare()
{
	return square4hex6;
}
