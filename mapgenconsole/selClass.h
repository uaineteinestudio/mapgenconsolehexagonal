#pragma once

#include "coord.h"

class selClass
{
	friend class mapdata;
	friend class mapgenerator;
	friend class levelmapIO;
	friend class levelmap;
	friend class levelmapUI;
public:
	selClass();
	~selClass();
	//selected block
	void updateSelectedBlock(int x, int y);
	void getSelectedBlock(int &x, int &y);
	void updatemousepos(int x, int y);
	void getmousepos(int &x, int &y);
	void getmouseOldpos(int &x, int &y);
protected:
	//selected position
	coord selectedpos;	//selected position here
	coord oldpos;		//old selected position
	coord mousepos;		//mouse position here
	coord mouseposOld;	//mouse position old, updated everytime the mousepos is
};

