#include "cellautoma.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

CA::CA()
{
	//leave as is
}

CA::CA(float ch, int dl, int bl, int smoofac)
{
	chanceToStartAlive = ch;
	deathlimit = dl;
	birthlimit = bl;
	noSteps = smoofac;
}

CA::~CA()
{
}

bool** CA::cellautomata(int width, int height)
{
	srand(time(NULL));
	bool** xBools = false;					//give 0 value for now
	xBools = new bool*[MaxLen];				//x vals
	for (int x = 0; x < MaxLen; x++)
	{
		xBools[x] = new bool[MaxLen];		//y vals
		for (int y = 0; y < MaxLen; y++)
		{
			if (x < width && y < height)
			{
				float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
				if (r < chanceToStartAlive)
				{
					xBools[x][y] = true;
				}
				else
				{
					xBools[x][y] = false;
				}
			}
			else
			{
				xBools[x][y] = false;
			}
		}
	}
	//then run the sim steps
	for (int i = 0; i < noSteps; i++)
	{
		xBools = simulationStep(xBools, width, height);
	}
	return xBools;
}

int CA::countAliveNeighbours(bool** map, int x, int y, int width, int height)
{
	int count = 0;
	for (int i = -1; i < 2; i++)
	{
		for (int j = -1; j < 2; j++)
		{
			int neighx = x + i;
			int neighy = y + j;
			//middle point
			if (i == 0 && j == 0)
			{
				//do nothing, excluded
			}
			else if (neighx < 0 || neighy < 0 || neighx >= width || neighy >= height)
			{
				count += 1;
			}
			else if (map[neighx][neighy] == false)
			{
				count += 1;
			}
		}
	}
	return count;
}

bool** CA::simulationStep(bool** oldmap, int width, int height)
{
	//copy over old map
	bool** newmap = false;					//give 0 value for now
	newmap = new bool*[MaxLen];				//x vals
	for (int x = 0; x < MaxLen; x++)
	{
		newmap[x] = new bool[MaxLen];		//y vals
		for (int y = 0; y < MaxLen; y++)
		{
			newmap[x][y] = false;
		}
	}

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			int nbs = countAliveNeighbours(oldmap, x, y, width, height);
			//if a cell is alive but has too few neighbours kill it
			if (oldmap[x][y] == true)
			{
				if (nbs < deathlimit)
					newmap[x][y] = false;
				else
					newmap[x][y] = true;
			}
			else
			{
				if (nbs > birthlimit)
					newmap[x][y] = true;
				else
					newmap[x][y] = false;
			}
		}
	}
	return newmap;
}
