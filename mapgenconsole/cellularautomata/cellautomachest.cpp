#include "cellautomachest.h"
#include "../floodfiller/floodfillersize.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

CAChest::CAChest()
{
	//keeping as is for the other deets
	//adding this below
	treasureLim = 1;
	numChests = 3;
}

CAChest::~CAChest()
{
}

int CAChest::getNumChests()
{
	return numChests;
}

vector<coord> CAChest::GetTreasureSpots(bool ** map, int ** blocktypes, int width, int height, int &finalsize)
{
	//initalise
	vector<coord> treasureSpots;
	int n = 0;
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			int numrefs = 0;//number of neighbours
			if (validTreasurePlacement(map, blocktypes, x, y, width, height, numrefs))//if valid
			{
				coord spot = coord(x, y);
				treasureSpots.push_back(spot);
				n += 1;
			}
		}
	}
	//now filter by random choice
	finalsize = numChests;
	if (n <= numChests)
		finalsize = n;
	if (n <= numChests)
	{
		return treasureSpots;
	}
	else
	{
		/* initialize random seed: */
		srand(time(NULL));
		//need random ones
		int k = 0;
		int delcount = n - finalsize;
		for (int i = 0; i < delcount; i++)
		{
			k = rand() % (n - 1);//get random index
			//now delete from vector thanks
			treasureSpots.erase (treasureSpots.begin() + k, treasureSpots.begin() + k + 1);
			n -= 1;
		}
	}
	//now return
	return treasureSpots;
}

int CAChest::getChamberSize(bool ** map, int xi, int yi, int width, int height)
{
	floodfillersize fs = floodfillersize(width, height);
	return fs.getSize(map, xi, yi);
}

bool CAChest::validTreasurePlacement(bool ** map, int ** blocktypes, int xi, int yi, int width, int height, int & numrefs)
{
	if (map[xi][yi]) //is a blocked out cell
	{
		int chSize = getChamberSize(map, xi, yi, width, height);
		if (chSize >= minChamberSize)
		{
			numrefs = countAliveNeighbours(map, xi, yi, width, height);
			if (numrefs >= treasureLim)
			{
				if (blocktypes[xi][yi] == blockTypes::caveInterior)
				{
					return true;
				}
			}
		}
	}
	return false;
}
