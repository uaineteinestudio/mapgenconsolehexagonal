#include "chunk.h"

using namespace std;

chunk::chunk()
{
	chunkPos = coord(0, 0);//default
}

chunk::chunk(int cx, int cy)
{
	chunkPos = coord(cx, cy);
}

chunk::chunk(coord cpos)
{
	chunkPos = cpos;
}

chunk::~chunk()
{
}

bool chunk::isAppropirateMapLength(int len)
{
	if (len % chunkLen == 0)
		return true;
	return false; //else
}

coord chunk::getChunkPos(coord realpos)
{
	int x = realpos.x / chunkLen;
	int y = realpos.y / chunkLen;
	return coord(x, y);
}

coord chunk::returnChunkPos()
{
	return chunkPos;
}

coord chunk::getRealCoord(int x, int y)
{
	coord pos = coord(x, y);
	//from x y relative in the chunk
	pos.x += chunkPos.x * chunkLen;
	pos.y += chunkPos.y * chunkLen;
	return pos;
}

vector<coord> chunk::getChunkBlockCoords()
{
	vector<coord> output;
	for (int x = 0; x < chunkLen; x++)
	{
		for (int y = 0; y < chunkLen; y++)
		{
			output.push_back(getRealCoord(x, y));
		}
	}
	return output;
}
