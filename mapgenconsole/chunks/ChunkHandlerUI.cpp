#include "ChunkHandlerUI.h"

void ChunkHandlerUI::printActiveChunks(coord selectedpos, bool squareorhex) //0 is square
{
	cout << "All Active chunk coords:" << endl;
	bool** activeChunkCoords = dynInitaliser::makeBool(lenx, leny, false); //initalise
	for (int i = 0; i < nChunks; i++)
	{
		vector<coord> chunkarr = activeChunks[i].getChunkBlockCoords();
		for (int j = 0; j < chunkarrlen; j++)
		{
			activeChunkCoords[chunkarr[j].x][chunkarr[j].y] = true;
		}
		chunkarr.clear();
		//cleanup
	}
	ui::print_matrixSel(activeChunkCoords, lenx, leny, selectedpos.x, selectedpos.y, ui::Red);
	//cleanup
	dynInitaliser::del(activeChunkCoords, lenx);

	cout << "selected pos: ";
	ui::printCoord(selectedpos);
	cout << endl;
}