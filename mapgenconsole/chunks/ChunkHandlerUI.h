#pragma once

#include "ChunkHandler.h"
#include "../ui/ui.h"
#include <iostream>

class ChunkHandlerUI : public ChunkHandler
{
public:
	ChunkHandlerUI(int width, int height) : ChunkHandler(width, height)
	{
		//ADD HERE
	}
	ChunkHandlerUI(int width, int height, int activelen) : ChunkHandler(width, height, activelen)
	{
		//ADD HERE
	}
	//COPY IN FROM BASE CLASS
	ChunkHandlerUI(ChunkHandler ch) : ChunkHandler(ch.lenx, ch.leny, ch.activechunkwidth)
	{
		nChunks = ch.nChunks;
		activeChunks = ch.activeChunks;
	}
	void printActiveChunks(coord selectedpos, bool squareorhex);
};

