#include "ChunkHandler.h"

using namespace std;

ChunkHandler::ChunkHandler()
{
	activechunkwidth = 2;
	lenx = MaxLen;
	leny = MaxLen;
}

ChunkHandler::ChunkHandler(int width, int height)
{
	activechunkwidth = 2;
	lenx = width;
	leny = height;
}

ChunkHandler::ChunkHandler(int width, int height, int activeclen)
{
	activechunkwidth = activeclen;
	lenx = width;
	leny = height;
}

ChunkHandler::~ChunkHandler()
{
}

void ChunkHandler::addActiveChunk(coord cpos)
{
	//not been used so add
	activeChunks.push_back(chunk(cpos));
	nChunks += 1;
}

bool ChunkHandler::ChunkNotActive(coord newcoord)
{
	for (int i = 0; i < nChunks; i++)
	{
		if (activeChunks[i].returnChunkPos() == newcoord)
			return false;
	}
	//else
	return true;
}

void ChunkHandler::UpdateActiveChunks(coord selpos, coord selectedposOld)
{
	//now I have the oldpos
	coord cpos = chunk::getChunkPos(selpos);
	coord cposold = chunk::getChunkPos(selectedposOld);
	coord del = cpos - cposold; //delta x and delta y
	coord cpos2 = cpos; //make copy

	int minx = 10 * MaxLen;
	int miny = minx;
	int maxx = -minx;
	int maxy = -miny;
	for (int x = -activechunkwidth; x < activechunkwidth; x++)
	{
		for (int y = -activechunkwidth; y < activechunkwidth; y++)
		{
			cpos2.x = cpos.x + x;
			cpos2.y = cpos.y + y;
			if (cpos2.x >= 0 && cpos2.y >= 0)
			{
				if (cpos2.x < lenx / chunkLen)
				{
					if (cpos2.y < leny / chunkLen)
					{
						if (cpos2.x < minx)
							minx = cpos2.x;
						if (cpos2.x > maxx)
							maxx = cpos2.x;
						if (cpos2.y < miny)
							miny = cpos2.y;
						if (cpos2.y > maxy)
							maxy = cpos2.y;
						if (ChunkNotActive(cpos2))
						{
							addActiveChunk(cpos2);
						}
					}
				}
			}
		}
	}
	//now delete if del is greater than 0
	if (del.x != 0)
	{
		//got to delete
		//delx first
		int k = 0;
		if (del.x > 0) //positive
		{
			for (int i = 1; i < del.x + 1; i++)//loop over all vals
			{
				k = minx - i;
				for (int j = -activechunkwidth; j < activechunkwidth; j++)
				{
					int m = j + cposold.y;
					if (m >= 0) //just one quick check but is still error handled after
					//remove (if it is in the list)
						removeChunk(coord(k, m));
				}
			}
		}
		//else
		else //is negative
		{
			for (int i = 1; i < -del.x + 1; i++)
			{
				k = maxx + i;
				for (int j = -activechunkwidth; j < activechunkwidth; j++)
				{
					int m = j + cposold.y;
					if (m <= leny / chunkLen) //just one quick check but is still error handled after
					{
						//remove if it is in the list
						removeChunk(coord(k, m));
					}
				}
			}
		}
	}
	if (del.y != 0)
	{
		//got to delete
		//dely next
		int k = 0;
		if (del.y > 0) //positive
		{
			for (int i = 1; i < del.y + 1; i++)//loop over all vals
			{
				k = miny - i;
				for (int j = -activechunkwidth; j < activechunkwidth; j++)
				{
					int m = j + cposold.x;
					if (m >= 0) //just one quick check but is still error handled after
					//remove (if it is in the list)
						removeChunk(coord(m, k));
				}
			}
		}
		//else
		else //is negative
		{
			for (int i = 1; i < -del.y + 1; i++)
			{
				k = maxy + i;
				for (int j = -activechunkwidth; j < activechunkwidth; j++)
				{
					int m = j + cposold.x;
					if (m <= lenx / chunkLen) //just one quick check but is still error handled after
					{
						//remove if it is in the list
						removeChunk(coord(m, k));
					}
				}
			}
		}
	}
}

void ChunkHandler::removeChunk(int i)
{
	activeChunks.erase(activeChunks.begin() + i);
	nChunks -= 1; //removed right
}

void ChunkHandler::removeChunk(coord cpos)
{
	for (int i = 0; i < nChunks; i++)
	{
		if (cpos == activeChunks[i].returnChunkPos())
		{
			//then deleeeeete
			removeChunk(i);
			break;
		}
	}
}
