#pragma once

#include "chunk.h"
#include "../maplen.h"
#include "../arrays/dynInitaliser.h"

class ChunkHandler
{
	friend class ChunkHandlerUI;
public:
	ChunkHandler(); //default at 2 width and max lens
	ChunkHandler(int width, int height);//default 2 width
	ChunkHandler(int width, int height, int activeclen);//by how many chunks left and right of a position is 'active'
	~ChunkHandler();
	vector<chunk> activeChunks; //active chunks for me
	void UpdateActiveChunks(coord selpos, coord selectedposOld);
protected:
	int nChunks = 0;
	int activechunkwidth;
	int lenx;
	int leny;
private:
	bool ChunkNotActive(coord newcoord);
	void addActiveChunk(coord cpos);
	void removeChunk(int i);
	void removeChunk(coord cpos);
};

