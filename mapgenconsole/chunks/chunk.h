#pragma once

#include "../coord.h" //hopefully can make a static list of this
#include "../maplen.h"
#include <vector>

using namespace std;

static const int chunkLen = 4; //4 by 4 for fun
static const int chunkarrlen = chunkLen * chunkLen;

class chunk 
{
public:
	//default constructor of pos 0,0
	chunk();
	chunk(int cx, int cy); //chunk cx and chunk cy;
	chunk(coord cpos); //chunk cx and chunk cy;
	~chunk();

	static bool isAppropirateMapLength(int len); //is multiple of chunk
	static coord getChunkPos(coord realpos);
	coord returnChunkPos();
	coord getRealCoord(int x, int y);
	vector<coord> getChunkBlockCoords(); //return pointer/array of the chunk coordinates
private:
	coord chunkPos;
};

