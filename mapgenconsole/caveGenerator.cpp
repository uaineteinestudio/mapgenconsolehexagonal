#include "caveGenerator.h"

caveGenerator::caveGenerator(int lenx, int leny)
{
	w = lenx;
	h = leny;
	Interior = 0;
}

caveGenerator::~caveGenerator()
{
}

bool ** caveGenerator::makeCaves()
{
	bool ** caves = cellautomata(w, h);
	//ui::print_matrix(caves, 20, 20); //this is for debug
	//purgeCaves(caves, width, height); //now to be called at a higher level being public
	return caves;
}

void caveGenerator::setInteriorCells(bool ** cavemap, int ** blocktypes)
{
	int size = 0;
	int xi = 0;
	int yi = 0;
	vector<coord> cells = interiorCells(cavemap, size);
	for (int i = 0; i < size; i++)
	{
		xi = cells[i].x;
		yi = cells[i].y;
		blocktypes[xi][yi] = Interior;
	}
}

void caveGenerator::setInteriorCellsHex(bool ** cavemap, int ** blocktypes, int** blockheights)
{
	int size = 0;
	int xi = 0;
	int yi = 0;
	int heightsum = 0;
	vector<coord> cells = interiorCellsHex(cavemap, size);
	if (size > 0) //if there were any
	{
		for (int i = 0; i < size; i++)
		{
			xi = cells[i].x;
			yi = cells[i].y;
			blocktypes[xi][yi] = Interior;
			heightsum += blockheights[xi][yi];
		}
		int average = heightsum / size;
		for (int i = 0; i < size; i++)
		{
			xi = cells[i].x;
			yi = cells[i].y;
			blockheights[xi][yi] = average; //average height set
		}
	}
	else
	{
		//none so do nothing
	}
}

vector<coord> caveGenerator::interiorCells(bool ** caves, int &size)
{
	size = 0;
	vector<coord> cells = vector<coord>();
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			if (isInterior(caves, x, y))
			{
				size += 1;
				cells.push_back(coord(x, y));
			}
		}
	}
	return cells;
}

vector<coord> caveGenerator::interiorCellsHex(bool ** caves, int &size)
{
	size = 0;
	vector<coord> cells = vector<coord>();
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			if (isInteriorHex(caves, x, y))
			{
				size += 1;
				cells.push_back(coord(x, y));
			}
		}
	}
	return cells;
}

void caveGenerator::PlaceChests(bool ** cavemap, int ** blocktypes, int ** surfacetypes, vector<coord> &chestLocations, int &numChests)
{
	numChests = 0;
	int xi = 0;
	int yi = 0;
	chestLocations = GetTreasureSpots(cavemap, blocktypes, w, h, numChests);
	for (int i = 0; i < numChests; i++)
	{
		xi = chestLocations[i].x;
		yi = chestLocations[i].y;
		surfacetypes[xi][yi] = surfaceTypes::chest;
	}
}

bool caveGenerator::isInterior(bool ** map, int xi, int yi)
{
	int col[4] = { -1, 0, 0, 1 };
	int row[4] = { 0, -1, 1, 0 };

	int x = 0;
	int y = 0;

	if (map[xi][yi] == false)//if a cave required
	{
		return false;
	}
	else
	{
		for (int i = 0; i < 4; i++)
		{
			x = xi + col[i];
			y = yi + row[i];
			if (boundchecker::inBounds(x, y, w, h))
				if (map[x][y] == false)
					return false;
		}
		return true; //else
	}
}

bool caveGenerator::isInteriorHex(bool ** map, int xi, int yi)
{
	int x = 0;
	int y = 0;
	int sum = 0;

	int col[HexAdjLen];
	int row[HexAdjLen];
	hexAdjaceny::WriteColRow(col, row, xi, yi);

	if (map[xi][yi] == false)//if a cave required
	{
		return false;
	}
	else //is a cave
	{
		for (int i = 0; i < HexAdjLen; i++)
		{
			x = xi + col[i];
			y = yi + row[i];
			if (boundchecker::inBounds(x, y, w, h))
			{
				if (map[x][y] == false) //is not a cave
					return false; //we are already having to stop as this is not interior
				else
					sum += 1;
			}
		}
		if (sum == HexAdjLen)
			return true; //else when all 6 are caves
	}
	return false; //all else
}

void caveGenerator::purgeCaves(bool ** caves, bool sqHex)
{
	int** sizeArr = dynInitaliser::makeInt(w, h, 0);
	if (sqHex)	//hex-true
	{
		floodfillerHexsize fs = floodfillerHexsize(w, h);
		fs.getSizeArray(caves, sizeArr, w, h);
	}
	else
	{
		floodfillersize fs = floodfillersize(w, h);
		fs.getSizeArray(caves, sizeArr, w, h);
	}
	//ui::print_matrix(sizeArr, w, h);
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			if (sizeArr[x][y] < purgeSmallerThan)
				caves[x][y] = false;
			else if (sizeArr[x][y] > purgeLargerThan)
				caves[x][y] = false;
		}
	}
	//ui::print_matrix(caves, w, h); //this is for debug
	if (!HasCave(caves)) //if false damn
	{
		makeArtificalCave(caves);
	}
}

void caveGenerator::setCaves(bool ** caves, int** bt)
{
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			if (caves[x][y])//if a cave
				bt[x][y] = blockTypes::cave;
		}
	}
}

void caveGenerator::DebugPrintStats(float p)
{
	cout << p << "% of cave gen complete" << endl;
}

void caveGenerator::makeArtificalCave(bool ** cavesmap)
{
	int select = rand() % numArtCaves;
	int x0 = rand() % (w - artCaveDimLen);
	int y0 = rand() % (h - artCaveDimLen);
	for (int x = 0; x < artCaveDimLen; x++)
	{
		for (int y = 0; y < artCaveDimLen; y++)
		{
			cavesmap[x0 + x][y0 + y] = artCavs[select][x][y];
		}
	}
}

void caveGenerator::caveCleanup(bool ** caves, int** bt, int keepthistype, bool sqhex)
{
	int col[HexAdjLen];
	int row[HexAdjLen];

	int sqcol[4] = { -1, 0, 0, 1 };
	int sqrow[4] = { 0, -1, 1, 0 };

	//initalised for speed
	int xi = 0;
	int yi = 0;
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			//find a cave and then get boundaries
			//once these are found, make a count for cellinterior
			//if none, then remove this point
			if (caves[x][y])
			{
				//is a cave
				if (sqhex) //hex
				{
					hexAdjaceny::WriteColRow(col, row, x, y);
					bool any = false;
					for (int i = 0; i < HexAdjLen; i++)
					{
						xi = col[i] + x;
						yi = row[i] + y;
						if (boundchecker::inBounds(xi, yi, w, h))
						{
							//in bounds
							if (bt[xi][yi] == keepthistype)
							{
								any = true;
							}
						}
					}
					if (any == false)
					{
						//none found so remove
						caves[x][y] = false;
						bt[x][y] = blockTypes::heavystonegrass; //heavy stone grass
					}
				}
				else //square
				{
					bool any = false;
					for (int i = 0; i < 4; i++)
					{
						xi = sqcol[i] + x;
						yi = sqrow[i] + y;
						if (boundchecker::inBounds(xi, yi, w, h))
						{
							//in bounds
							if (bt[xi][yi] == keepthistype)
							{
								any = true;
							}
						}
					}
					if (any == false)
					{
						//none found so remove
						caves[x][y] = false;
						bt[x][y] = blockTypes::heavystonegrass; //heavy stone grass
					}
				}
			}
		}
	}
}

bool caveGenerator::HasCave(bool ** cavemapAfterPurge)
{
	for (int x = 0; x < w; x++)
	{
		for (int y = 0; y < h; y++)
		{
			if (cavemapAfterPurge[x][y]) //is true
				return true;
		}
	}
	//else not one found
	return false;
}

bool caveGenerator::oppval(bool ** caves, int x, int y)
{
	if (caves[x][y])
		return false;
	return true;
}

float caveGenerator::calcPercComplete(int num, int max)
{
	return (float)num / float(max) * 100;
}
