# mapgenconsoleHex

A c++ map generator for square and hexagonal tiles with types (both base and surface types), heightmap, caves (cellular automata), chest placement, inventory management, water level, water edge detection, floodfiller, walkable properties, seletion capabilites, active chunks to restrict update overhead, footpath spawning and general pathfinding and IO.

Additionally contains a seperate template for player pieces with visibility and discovery calculations atop of unique IDs for turn based movement. Can be reprogrammed to suit target needs.

This repo is an extension of [mapgenconsole](https://bitbucket.org/uaineteinestudio/mapgenconsole) by [UaineTeine](https://bitbucket.org/uaineteinestudio/).

## Getting Started

See the mapgenconsole.cpp file for execution of various problems and the levelmap.h and levelmap.cpp for the building of the maps.

## Version

##### Beta 1.5.3

See the [changelog](changelog.txt) for details

## Authors

* **Daniel Stamer-Squair** - *UaineTeine*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.