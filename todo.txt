--BETA 1.6--
caves have 'entrance' tile to allow wizard to open gate. can have multiple
visibility ring on 'edge' of what can be seen-possibly to handle in engine, but function can be written for manual execution

--BETA 1.7--
gravestone generation
featuregeneration - relic site etc
chests out of caves

--BETA 1.8--
item lists and chest items
--chests contains items

--BETA 1.9--	est date 29/11/2021
need small non interiro caves to look diff to those with inner
More mountains
keep walkability transpose copy in map, don't need to save though, just load it back

--BETA 1.10--	est date: 15/12/2021
CSV READER
WATERFALL GENERATION

--BETA 1.11--	est date: 15/12/2021
Minimap bitmap generator

--ONWARDS--
SEPERATE PIECE PROJECT TO BUILD UPON THIS
..
playerNPC class
buffs and such
non-transferable items and cards
link items and cards to inventory
item level chests variance